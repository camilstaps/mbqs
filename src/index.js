/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

const path=require ('path');
const process=require ('process');
const {crashReporter} = require('electron');
crashReporter.start({ submitURL: 'https://example.com' });

if (__dirname.substr (__dirname.length-14).match (/[\/\\]resources[\/\\]app/))
	process.chdir (path.join (__dirname,'..','..'));
else
	process.chdir (__dirname);

const {run}=require ('./MBQSServer-www/js/itasks-electron.js');

const opts={
	app: 'MBQSServer',
	/* We need to always use the same iTasks port so that LocalStorage works.
	 * LocalStorage is used by the HandsonTable PersistentState plugin, which
	 * makes sure that column widths are stored over sessions. */
	itasks_port: 13217
};

run (opts);
