/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

const fs=require ('fs/promises');
const path=require ('path');
const process=require ('process');

(async function(){
	let page=`<!DOCTYPE html>
<!--
 - This file is part of MBQS.
 -
 - MBQS is free software: you can redistribute it and/or modify it under the
 - terms of the GNU Affero General Public License as published by the Free
 - Software Foundation, version 3 of the License.
 -
 - MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 - WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 - FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 - more details.
 -
 - You should have received a copy of the GNU Affero General Public License
 - along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 -->
<html style="width: 100%; height: 100%;">
<head>
	<meta charset="UTF-8">
	<title>License details</title>

	<link rel="stylesheet" href="/css/itasks-modules.css" type="text/css" >
	<link rel="stylesheet" href="/css/mbqs.css" type="text/css" >
</head>
<body style="width: 100%; height: 100%; overflow-y: auto !important;">
	<h1>Licenses</h1>

	<h2>Məḇaqqēš</h2>
	<details>
		<summary>Click for details of the Məḇaqqēš license.</summary>
		<pre>`;

	await fs.readFile ('../LICENSE', {encoding: 'utf8'}).then (file => {
		page+=file;
		page+=`		</pre>
	</details>

	<h2>Licenses for open source Clean software used in Məḇaqqēš</h2>
	<p>
		The following open source Clean projects are used in Məḇaqqēš.
		Click for details of their licenses.
	</p>`;
	});

	const pkg_path=path.join ('..','nitrile-packages',process.platform == 'win32' ? 'windows-x64' : 'linux-x86');
	await fs.readdir (pkg_path).then (async pkgs => {
		for (const pkg of pkgs){
			if (pkg == '.home')
				continue;
			await fs.readFile (path.join (pkg_path,pkg,'LICENSE'), {encoding: 'utf8'}).then (file => {
				page+=`<details><summary>${pkg}</summary><pre>`;
				page+=file;
				page+='</pre></details>';
			}).catch (e => {
				console.warn (`Warning: ${pkg} has no file named LICENSE.`);
			});
		}
	});

	page+=`
	<h2>Open source JavaScript software used in Məḇaqqēš</h2>

	<p>
		The following open source JavaScript projects are used in Məḇaqqēš.
		The licenses can be found in the corresponding source files.
	</p>

	<p>
		@fortawesome/fontawesome-free,
		css-doodle,
		electron,
		handsontable,
		moment,
		request,
		split.js,
		and vanilla-picker.
	</p>
</body>
</html>`;

	await fs.writeFile ('WebPublic/licenses.html',page);
})();
