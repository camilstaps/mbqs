implementation module iTasks.Extensions.CssDoodle

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Func
import qualified Data.Map
import Text

import iTasks
import iTasks.UI.JavaScript

import ABC.Interpreter.JavaScript

(hasProperties) :: !String ![CssDoodleProperty] -> CssDoodleBlock
(hasProperties) selector props = CssDoodleBlock selector props

(is) :: !String !String -> CssDoodleProperty
(is) name val = CssDoodleProperty name val

instance toString CssDoodle
where
	toString (CssDoodle blocks props) = concat (map toString blocks ++ map toString props)

instance toString CssDoodleBlock
where
	toString (CssDoodleBlock selector props) = concat
		[ selector
		, "{"
		: map toString props ++
		[ "}"
		]]

instance toString CssDoodleProperty
where
	toString (CssDoodleProperty name val) = concat4 name ":" val ";"

derive class iTask \ gEditor CssDoodle, CssDoodleBlock, CssDoodleProperty

gEditor{|CssDoodle|} _ = JavaScriptInit initUI @>> leafEditorToEditor
	{ LeafEditor
	| onReset    = onReset
	, onEdit     = onEdit
	, onRefresh  = onRefresh
	, writeValue = \_ -> Ok EmptyEditor
	}

onReset :: !UIAttributes !(?CssDoodle) !*VSt -> *(!*MaybeErrorString *(UI, (), * ?(EditorReport CssDoodle)), !*VSt)
onReset attrs ?None vst = (Error "cssDoodle can only be used in View mode", vst)
onReset attrs (?Just doodle) vst =
	( Ok
		( uia UIHtmlView ('Data.Map'.union (valueAttr (JSONString (toString doodle))) attrs)
		, ()
		, ?None
		)
	, vst
	)

initUI :: !FrontendEngineOptions !JSVal !*JSWorld -> *JSWorld
initUI options me w
	#  (cb,w) = jsWrapFun (initDOMEl options me) me w
	= (me .# "initDOMEl" .= cb) w
initDOMEl {FrontendEngineOptions|serverDirectory} me _ w
	#  (cb,w) = jsWrapFun (initDOMEl` me) me w
	# w = jsTrace serverDirectory w
	= addJSFromUrl True (serverDirectory +++ "js/css-doodle.min.js") (?Just cb) me w
initDOMEl` me _ w
	# w = (me .# "domEl.style.padding" .= 0) w
	# (doodle,w) = (jsDocument .# "createElement" .$ "css-doodle") w
	# w = (me .# "domEl.appendChild" .$! doodle) w
	# w = (doodle .# "update" .$! me .# "attributes.value") w
	= w

onEdit :: () !() !*VSt -> *(!*MaybeErrorString *(UIChange, (), ?(EditorReport CssDoodle)), !*VSt)
onEdit _ _ vst = (Error "no edit event expected from cssDoodle", vst)

onRefresh :: !(?CssDoodle) () !*VSt -> *(!*MaybeErrorString *(UIChange, (), ?(EditorReport CssDoodle)), !*VSt)
onRefresh mbDoodle _ vst =
	( Ok
		( ReplaceUI (uia UIHtmlView (valueAttr (JSONString (maybe "" toString mbDoodle))))
		, ()
		, ?None
		)
	, vst
	)
