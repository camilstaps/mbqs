implementation module iTasks.Extensions.SearchField

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Error
import Data.Func
import qualified Data.Map

import iTasks
import iTasks.UI.JavaScript

import ABC.Interpreter.JavaScript

derive class iTask Query, SearchFieldAction

instance toString SearchFieldAction
where
	toString FocusSearchBar = "FocusSearchBar"

searchField :: !(String -> MaybeError String a) -> Editor Query (EditorReport Query)
searchField check =
	tune (syncOnEnterAttr True) $
	JavaScriptInit (initUI check) @>> leafEditorToEditor
	{ LeafEditor
	| onReset    = onReset
	, onEdit     = onEdit
	, onRefresh  = onRefresh
	, writeValue = \q -> Ok $ ValidEditor {query=q, action= ?None}
	}

onReset :: !UIAttributes !(?Query) !*VSt -> *(!*MaybeErrorString *(UI, String, * ?(EditorReport Query)), !*VSt)
onReset attrs mbQuery vst =
	( Ok
		( uia UITextField ('Data.Map'.put "value" (JSONString value) attrs)
		, value
		, ?None
		)
	, vst
	)
where
	value = maybe "" (\q -> q.query) mbQuery

onEdit :: !(?String) String !*VSt -> *(!*MaybeErrorString *(UIChange, String, ?(EditorReport Query)), !*VSt)
onEdit ?None _ vst = (Ok (NoChange, "", ?Just EmptyEditor), vst)
onEdit (?Just newVal) _ vst = (Ok (NoChange, newVal, ?Just (ValidEditor {query=newVal, action= ?None})), vst)

onRefresh :: !(?Query) !String !*VSt -> *(!*MaybeErrorString *(UIChange, String, ?(EditorReport Query)), !*VSt)
onRefresh (?Just {query,action= ?Just action}) old vst = (Ok (change, query, ?Just (ValidEditor {query=query, action= ?None})), vst)
where
	change = ChangeUI
		[ SetAttribute "action" (JSONString (toString action))
		: if (query == old) [] [SetAttribute "value" (JSONString query)]
		]
		[]
onRefresh (?Just {query,action= ?None}) old vst =
	// NB: always send the query; because we use syncOnEnterAttr the value on
	// the frontend may differ from the `old` state we have here.
	(Ok (ChangeUI [SetAttribute "value" (JSONString query)] [], query, ?None), vst)
onRefresh ?None old vst =
	(Error "searchField: onRefresh expects a value", vst)

initUI :: !(String -> MaybeError String a) FrontendEngineOptions !JSVal !*JSWorld -> *JSWorld
initUI check _ me w
	# (orgOnAttributeChange,w) = me .# "onAttributeChange" .? w
	# (onAttributeChange,w) = jsWrapFun (onAttributeChange orgOnAttributeChange me) me w
	# w = (me .# "onAttributeChange" .= onAttributeChange) w
	# (orgInitDOMEl,w) = me .# "initDOMEl" .? w
	# (initDOMEl,w) = jsWrapFun (initDOMEl orgInitDOMEl me) me w
	= (me .# "initDOMEl" .= initDOMEl) w
where
	initDOMEl orgInitDOMEl me _ w
		# w = (jsCall (orgInitDOMEl .# "bind") me .$! ()) w
		# (onInput,w) = jsWrapFun (onInput check me) me w
		# w = (me .# "domEl.addEventListener" .$! ("input", onInput)) w
		= w

onAttributeChange :: !JSFun !JSVal !{!JSVal} !*JSWorld -> *JSWorld
onAttributeChange orgOnAttributeChange me {[0]=name,[1]=val} w
	# w = (jsCall (orgOnAttributeChange .# "bind") me .$! (name,val)) w
	# name = fromJS "" name
	  val = fromJS "" val
	| name == "value"
		= (me .# "domEl.value" .= val) w
	| name == "action"
		| val == "FocusSearchBar"
			= (me .# "domEl.focus" .$! ()) w
			= jsTrace ("iTasks.Extensions.SearchField: unknown action: "+++val) w
	| otherwise
		= w

onInput :: !(String -> MaybeError String a) !JSVal !{!JSVal} !*JSWorld -> *JSWorld
onInput check me _ w
	# (val,w) = me .# "domEl.value" .?? ("", w)
	# mbErr = check val
	| isError mbErr
		# w = (me .# "domEl.style.background" .= "#fcd1d1") w
		# w = (me .# "domEl.title" .= fromError mbErr) w
		= w
	| otherwise
		# w = (me .# "domEl.style.background" .= "white") w
		# w = (me .# "domEl.title" .= "") w
		= w
