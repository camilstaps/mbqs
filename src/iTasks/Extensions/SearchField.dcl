definition module iTasks.Extensions.SearchField

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError
from Data.GenEq import generic gEq
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import class iTask

:: Query =
	{ query  :: !String
		//* The actual value.
	, action :: !?SearchFieldAction
		//* This field can be set externally to perform an action on the field.
		//* Its value is then reset to `?None` by the editor.
	}

:: SearchFieldAction
	= FocusSearchBar

derive class iTask Query

searchField :: !(String -> MaybeError String a) -> Editor Query (EditorReport Query)
