implementation module iTasks.Extensions.ColorPicker

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Func

import iTasks
import iTasks.UI.JavaScript

import ABC.Interpreter.JavaScript

colorPicker :: Editor (?String) (EditorReport (?String))
colorPicker =
	// NB: we need the extra container level so that the picker has a parentNode to attach to
	mapEditorInitialValue (?Just o fromMaybe ?None) $
	mapEditorRead (fromMaybe "none") $
	mapValidEditorWriteError check $
	styleAttr "align-items: stretch;" @>>
	container1 (JavaScriptInit initUI @>> textField)
where
	check color
		| size color == 7 && color.[0] == '#' && all isHexDigit [color.[i] \\ i <- [1..6]]
			= Ok (?Just color)
		| color == "none"
			= Ok ?None
			= Error "not a valid color in #rrggbb format"

initUI :: !FrontendEngineOptions !JSVal !*JSWorld -> *JSWorld
initUI {FrontendEngineOptions|serverDirectory} me w
	# (oldOnAttributeChange,w) = me .# "onAttributeChange" .? w
	# (onAttributeChange,w) = jsWrapFun (onAttributeChange oldOnAttributeChange me) me w
	# w = (me .# "onAttributeChange" .= onAttributeChange) w
	# (orgInitDOMEl,w) = me .# "initDOMEl" .? w
	# (initDOMEl,w) = jsWrapFun (initDOMEl serverDirectory orgInitDOMEl me) me w
	= (me .# "initDOMEl" .= initDOMEl) w
initDOMEl serverDirectory orgInitDOMEl me _ w
	# w = (jsCall (orgInitDOMEl .# "bind") me .$! ()) w
	# (cb,w) = jsWrapFun (initDOMEl` me) me w
	= addJSFromUrl True (serverDirectory+++"js/vanilla-picker.min.js") (?Just cb) me w
initDOMEl` me _ w
	// NB: the child is initialized before the parent, so `me.domEl.parentNode`
	// and `me.parentCmp.domEl` are still null (unless this callback is delayed
	// because the color picker still has to be loaded). We create a dummy div
	// to use as the `parent` of the Picker; this works because iTasks will
	// only clear that element but not overwrite it.
	# (parentNode,w) = me .# "parentCmp.domEl" .? w
	# w = if (jsIsNull parentNode) ((me .# "parentCmp.domEl" .= jsCall (jsDocument .# "createElement") "div") w) w
	# w = (me .# "domEl.classList.add" .$! "color-picker") w
	# (value,w) = me .# "domEl.value" .?? ("none", w)
	  value = if (value == "none") "#000000ff" value
	# (onDone,w) = jsWrapFun (onDone me) me w
	# (picker,w) = jsNew "Picker" (jsRecord
		[ "parent" :> me .# "parentCmp.domEl"
		, "color" :> value
		, "alpha" :> False
		, "editorFormat" :> "hex"
		, "cancelButton" :> True
		, "popup" :> "bottom"
		, "onDone" :> onDone
		]) w
	# w = (me .# "domEl.picker" .= picker) w
	# (onFocus,w) = jsWrapFun (\_ -> picker .# "openHandler" .$! ()) me w
	# w = (me .# "domEl.addEventListener" .$! ("focus", onFocus)) w
	# w = setColor me value w
	= w

onAttributeChange :: !JSFun !JSVal !{!JSVal} !*JSWorld -> *JSWorld
onAttributeChange oldOnAttributeChange me {[0]=name,[1]=val} w
	# w = (jsCall (oldOnAttributeChange .# "bind") me .$! (name,val)) w
	# name = fromJS "" name
	  val = fromJS "" val
	| name == "value"
		= jsTrace val setColor me val w
		= jsTrace ("unknown attribute change: "+++name) w

onDone :: !JSVal !{!JSVal} !*JSWorld -> *JSWorld
onDone me {[0]=color} w
	# (color,w) = color .# "hex" .?? ("#000000ff", w)
	# w = setColor me color w
	# (ev,w) = jsNew "Event" "input" w
	# w = (me .# "domEl.dispatchEvent" .$! ev) w
	= w

setColor :: !JSVal !String !*JSWorld -> *JSWorld
setColor me color w
	# w = (me .# "domEl.style.backgroundColor" .= background) w
	# w = (me .# "domEl.value" .= value) w
	# w = (me .# "domEl.picker.setColor" .$! (value, True)) w
	= w
where
	valid = color <> "#000000ff"
	background = if valid color "white"
	value = if valid (color % (0,6)) "none"
