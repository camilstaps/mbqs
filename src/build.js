/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

const fs=require ('fs');
const path=require ('path');
const process=require ('process');

const request=require ('request');
const unzipper=require ('unzipper');

let platform=null;
switch (process.platform) {
	case 'win32': platform='windows-x64'; break;
	case 'linux': platform='linux-x86'; break;
	default:
		console.log ('Unrecognized platform ' + process.platform);
		process.exit (1);
}

const packager=require ('../nitrile-packages/'+platform+'/itasks-electron/tools/itasks-electron-packager.js');

packager ({
	out: path.join ('..','dist'),
	cleanApp: 'MBQSServer',
	electronApp: 'StartMBQS',
	ignore: [
		/* Source code */
		/^\/MBQS$/,
		/^\/Gui$/,
		/^\/iTasks$/,
		/* Miscellaneous files used during development */
		/^\/tags$/,
		/^\/NETBible_ref\.bblx$/,
		/^\/MBQS\.config\.json$/,
	],
	afterCopy: (buildPath,version,platform,arch,done) => {
		/* Config */
		const config={
			english_bible_db: 'NETBible_ref.bblx',
			english_bible_abbreviation: 'NET',
			english_bible_copyright:
				'Scripture quoted by permission. Quotations designated (NET) are ' +
				'from the NET Bible® copyright ©1996, 2019 by Biblical Studies ' +
				'Press, L.L.C. http://netbible.com All rights reserved.'
		};
		fs.writeFileSync (
			path.join (buildPath,'..','..','MBQS.config.json'),
			JSON.stringify (config)
		);

		/* NET bible */
		request ('http://bible.org/download/netbible/NETBible_esword_free.zip')
			.pipe (unzipper.Parse())
			.on ('entry',entry => {
				if (entry.path=='NETBible_ref.bblx')
					entry.pipe (fs.createWriteStream (path.join (buildPath,'..','..',entry.path)));
				else
					entry.autodrain();
			})
			.on ('finish',done);
	}
}).then (appPaths => {
	console.log (`Electron app bundles created:\n${appPaths.join('\n')}`);
});
