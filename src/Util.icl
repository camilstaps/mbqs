implementation module Util

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

toHumanReadable :: !String -> String
toHumanReadable s = {#if (i==0) (toUpper c) (if (c=='_') ' ' c) \\ c <-: s & i <- [0..]}

trimLineEndings :: !String -> String
trimLineEndings s
	# end = find_last_char (size s-1)
	| end == size s-1
		= s
		= s % (0,end)
where
	find_last_char -1 = -1
	find_last_char i
		| s.[i]=='\n' || s.[i]=='\r'
			= find_last_char (i-1)
			= i
