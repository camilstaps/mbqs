"use strict";

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

const moment=require ('moment');

require('electron').ipcRenderer.on('toggle-tag', (ev,tag) => {
	const table=itasks.components.get(document.querySelector('.mbqs-sheet-main'));
	const selection=table.table.getSelected();
	if (typeof selection==='undefined'){
		window.alert('No rows selected.');
		return;
	}

	let html_table_row=table.domEl.querySelector('table.htCore tbody tr');
	let tds=html_table_row.querySelectorAll('td');
	let tag_index=null;
	for (let i=0; i<tds.length; i++){
		if (tds[i].classList.contains('tag')){
			tag_index=i;
			break;
		}
	}
	if (tag_index===null){
		window.alert('This sheet has no tag column.');
		return;
	}

	const hiddenRows=table.table.getPlugin('HiddenRows');

	let set_not_clear=false;
	decided_to_set: for (let range of selection){
		let start=range[0] < range[2] ? range[0] : range[2];
		let end=range[0] < range[2] ? range[2] : range[0];
		for (let row=start; row<=end; row++){
			if (hiddenRows.isHidden(row))
				continue;
			let current_tags=table.table.getDataAtCell(row,tag_index);
			if (!current_tags.match(tag)){
				set_not_clear=true;
				break decided_to_set;
			}
		}
	}

	let changes=[];
	for (let range of selection){
		let start=range[0] < range[2] ? range[0] : range[2];
		let end=range[0] < range[2] ? range[2] : range[0];
		for (let row=start; row<=end; row++){
			if (hiddenRows.isHidden(row))
				continue;
			let new_tags=table.table.getDataAtCell(row,tag_index);
			if (set_not_clear)
				new_tags=Array.from(new Set(new_tags + tag)).sort((x,y) => x.localeCompare(y)).join('');
			else
				new_tags=new_tags.replace(tag,'');
			changes.push([row, tag_index, new_tags]);
		}
	}

	table.table.setDataAtCell(changes);
});

function tagRenderer (instance,td,row,col,prop,val,cellProperties)
{
	let html='';
	for (let i=1; i<=9; i++){
		if (val.match(i))
			html+='<span class="tag-' + i + '">&#x25cf;</span>';
		else
			html+='<span class="no-tag">&#x25cf;</span>';
	}
	td.innerHTML=html;
	td.classList.add ('tag');
	return td;
}

function relativeTimeRenderer (instance,td,row,col,prop,val,cellProperties)
{
	const time=moment.utc (val,'X');
	td.innerHTML=time.isValid() ? time.fromNow() : '';
	td.classList.add ('date');
	return td;
}

/* NB: we cannot do a proper extends because Handsontable is not a proper class */
class MBQSTable {
	static instantiate (elem,settings)
	{
		const table=new Handsontable (elem,settings);

		table.column_classes=settings.column_classes || [];
		table.readonly_columns=settings.readonly_columns || [];
		table.saved_location=null;

		if (typeof settings.columns=='object' && settings.columns.constructor.name=='Array')
			for (var i=0; i<settings.columns.length; i++)
				if (settings.columns[i].type=='autocomplete' && typeof settings.columns[i].source=='undefined')
					settings.columns[i].source=MBQSTable.autocomplete (table,i);

		/* See tabMoves and enterMoves below */
		let last_cell_ignoring_tabs=null;
		let last_cell_with_tabs=[-1, -1];

		table.updateSettings ({
			cells: MBQSTable.cell_markup (table),
			rowHeaders: visualIndex => table.rowIndexMapper.getPhysicalFromVisualIndex (visualIndex) + 1,
			columns: settings.columns,
			hiddenRows: {
				copyPasteEnabled: false,
				indicators: false
			},
			/* tabMoves keeps track of two variables:
			 * - last_cell_ignoring_tabs is the last cell that was selected, ignoring
			 *   any moves that were made with the tab key;
			 * - last_cell_with_tabs is the last cell that was selected using the tab
			 *   key. */
			tabMoves: function () {
				let cell=table.getSelectedLast();
				if (last_cell_with_tabs[0] != cell[0] || last_cell_with_tabs[1] != cell[1])
					last_cell_ignoring_tabs=[cell[0], cell[1]];
				last_cell_with_tabs=[cell[0], cell[1]+1];
				return {row: 0, col: 1};
			},
			/* enterMoves uses the variables stored by tabMoves to get behavior
			 * similar to LibreOffice Calc: enter moves to the cell below the one
			 * that was last selected, *ignoring intermittent tabs*. This makes it
			 * more convenient to enter large amounts of data, because you can use
			 * tab to move in the row and enter returns to the first column that you
			 * have to enter in the next row. */
			enterMoves: function () {
				let cell=table.getSelectedLast();
				if (last_cell_with_tabs[0] != cell[0] || last_cell_with_tabs[1] != cell[1]) {
					/* a cell was selected without using tab, just move to cell below */
					last_cell_ignoring_tabs=last_cell_with_tabs = [cell[0]+1, cell[1]];
					return {row: 1, col: 0};
				} else {
					/* last cell was selected with tab, move to the cell below the one last selected without tab */
					last_cell_ignoring_tabs=last_cell_with_tabs = [cell[0]+1, last_cell_ignoring_tabs[1]];
					return {row: 1, col: last_cell_ignoring_tabs[1]-cell[1]};
				}
			}
		});

		/* Saves the data index of the currently selected cell, to be restored with
		 * restore_location. Should be called before hiding/showing rows. */
		table.save_location=() =>
		{
			const sel=table.getSelected();
			if (typeof sel=='undefined' || sel.length==0)
				return;
			const row=table.rowIndexMapper.getPhysicalFromVisualIndex (sel[0][0]);
			if (row!=null)
				table.saved_location={row: row, col: sel[0][1]};
		};

		/* Restores the location saved by save_location. Should be called after
		 * hiding/showing rows. */
		table.restore_location=() =>
		{
			if (table.saved_location==null)
				return;
			const row=table.rowIndexMapper.getVisualFromPhysicalIndex (table.saved_location.row)
			if (row!=null)
				table.selectCell (row,table.saved_location.col);
		};

		table.showOnlyRows=rowset => {
			table.save_location();

			const hiddenRows=table.getPlugin ('hiddenRows');
			hiddenRows.showRows (hiddenRows.getHiddenRows());

			if (rowset != null){
				const hidden=[];
				let j=0;
				let total=table.getData().length;
				for (let i=0; i<total; i++){
					if (rowset[j]==i){
						j++;
						continue;
					}
					hidden.push (i);
				}

				hiddenRows.hideRows (hidden);
			}

			table.render();

			table.restore_location();
		};

		/* TODO: Handsontable contextmenu only opens for right click, not for
		 * context menu button (#6729), so add a listener ourselves... */
		window.addEventListener ('contextmenu',ev => {
			if (table.isDestroyed)
				return;
			const selection=table.getSelected();
			if (selection==null || selection.length<1)
				return;
			const cell=table.getCell (selection[0][0],selection[0][1]);
			if (cell==null)
				return;
			const rect=cell.getBoundingClientRect();
			const click_ev=new MouseEvent ('click',{
				target: cell,
				screenX: window.screenX+rect.x,
				screenY: window.screenY+rect.y+rect.height,
				clientX: rect.x,
				clientY: rect.y+rect.height,
				button: 2
			});
			table.getPlugin ('contextMenu').open (click_ev);
		});

		for (var i in settings.columns){
			if (settings.columns[i].renderer==relativeTimeRenderer){
				window.setInterval(() => {table.render();}, 30000);
				break;
			}
		}

		return table;
	}

	/* Determine whether a cell is read-only and what CSS class it should have.
	 * This function is in JS for performance reasons.
	 * We use `instance` as an argument instead of this+bind for performance. */
	static cell_markup (instance)
	{
		return (row,col,prop) => {
			const props={};
			props.className=instance.column_classes[col];
			if (instance.readonly_columns.includes (col))
				props.readOnly=true;
			return props;
		};
	}

	/* Autocompletion function which allows values that appear in the current
	 * column as well as gives a back-up for the current value.
	 * This function is in JS for performance reasons. */
	static autocomplete (instance,col)
	{
		return (query,process) => {
			var vals=[];

			/* First add values for current selection */
			const data=instance.getData();

			for (var i=0; i<data.length; i++){
				if (instance.rowIndexMapper.isHidden (i))
					continue;

				const val=data[i][col];
				if (!val)
					continue;
				if (val.indexOf (query)==0 && !vals.includes (val))
					vals.push (val);
			}

			vals.sort();

			/* Then add values from hidden rows */
			const added_vals=[];
			instance.getPlugin ('hiddenRows').getHiddenRows().forEach (i => {
				const val=data[i][col];
				if (!val)
					return;
				if (val.indexOf (query)==0 && !vals.includes (val) && !added_vals.includes (val))
					added_vals.push (val);
			});

			added_vals.sort();
			vals=vals.concat (added_vals);
			
			/* Lastly add an option to include the current value verbatim */
			if (!vals.includes (query))
				vals.push (query);

			process (vals);
		};
	}
}
