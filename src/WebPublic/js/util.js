/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

function open_links_externally ()
{
	const links=document.getElementsByTagName ('a');
	Array.from (links).forEach (link => {
		link.onclick=ev => {
			require ('electron').shell.openExternal (link.href);
			ev.preventDefault();
			return false;
		};
	});
}

(() => {
	let config={
		attributes: false,
		childList: true,
		subtree: true
	};
	let observer=new MutationObserver (open_links_externally);
	observer.observe (document.body,config);

	open_links_externally();
})();
