definition module MBQS.Model

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from StdOverloaded import class toString

from Data.Error import :: MaybeError
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from Regex import :: Regex

from TextFabric import :: NodeRef, :: DataSet, :: DataSet`, :: EdgeSet
from TextFabric.Import import :: FeatureType

:: NodeName :== String

:: SheetSettings =
	{ nodes      :: ![NodeSettings]
	, columns    :: ![ColumnSettings]
	, text_panel :: !TextPanelSettings
	}

defaultSheetSettings :: SheetSettings

:: NodeSettings =
	{ name              :: !NodeName
	, type              :: !String
	, optional          :: !?Bool
	, filterDescription :: !NodeFilterDescription
	}

:: NodeFilterDescription
	= FeatureOp !String !FeatureOp

	| AND !NodeFilterDescription !NodeFilterDescription
	| OR !NodeFilterDescription !NodeFilterDescription
	| NOT !NodeFilterDescription
	| FALSE
		//* Used internally to create sheets without a query.

	| ParentOf !NodeName
	| ChildOf !NodeName

	| DistanceInterval !NodeName !Int !Int
		//* `x` < the distance to `n` < `y`.

instance toString NodeFilterDescription

:: FeatureOp
	= Eq !String
	| Ne !String
	| EqAny ![String]
	| Matches !Regex

:: ColumnSettings =
	{ title      :: !String
	, css_class  :: !?String
	, expression :: !?ColumnExpression
	}

:: ColumnExpression
	= Feature !NodeName !String
	| Subst !String !String !ColumnExpression
	| Concat ![ColumnExpression]
	| ConcatFor ![ColumnExpression] ![(String, NodeName, NodeFilterDescription)]

	| LastModificationDate

instance toString ColumnExpression

:: TextPanelSettings =
	{ text_node     :: !?NodeName
	, colored_nodes :: ![(NodeName, String)]
	}

:: SheetValues =
	{ node_names :: .{#.NodeName}
	, entries    :: .{#.SheetEntry}
	}

derive JSONEncode ColumnExpression, ColumnSettings, FeatureOp,
	NodeFilterDescription, NodeSettings, SheetEntry, SheetSettings,
	SheetValues, TextPanelSettings,
	{#SheetEntry}
derive JSONDecode ColumnExpression, ColumnSettings, FeatureOp,
	NodeFilterDescription, NodeSettings, SheetEntry, SheetSettings,
	SheetValues, TextPanelSettings,
	{#SheetEntry}

emptySheetValues :: SheetValues

:: SheetEntry =
	{ nodes   :: !.{#NodeRef}
	, columns :: !.{#String}
	}

checkSettings :: ![(String,FeatureType)] !SheetSettings -> ?String

requiredFeatures :: !SheetSettings -> [String]

getValues :: !SheetSettings !DataSet -> MaybeError String SheetValues

getColumnValue :: ![NodeName] !{#NodeRef} !ColumnExpression !DataSet -> ?String
getColumnValues :: ![NodeName] !{#NodeRef} ![ColumnSettings] !DataSet -> .{#String}

order_nodes :: ![NodeSettings] -> MaybeError String [NodeSettings]

nodeFilterDescriptionToSheetEntryCheck ::
	!{#NodeName} !NodeFilterDescription
	-> MaybeError String (SheetEntry -> Bool)
