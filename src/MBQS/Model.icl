implementation module MBQS.Model

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdOverloadedList

import Control.Monad
import Data.Error
import qualified Data.Foldable
import Data.Func
import Data.Functor
import Data.List
import Data.Maybe
import Data.Tuple
from Text import class Text(concat,replaceSubString), instance Text String, concat4, concat5
import Text.GenJSON

import qualified Regex
from Regex import :: Regex, :: Greediness, :: GroupId, instance toString Regex

import TextFabric
import TextFabric.Filters
import TextFabric.Import

derive JSONEncode ColumnExpression, ColumnSettings, FeatureOp,
	NodeFilterDescription, NodeSettings, SheetEntry, SheetSettings,
	SheetValues, TextPanelSettings
derive JSONDecode ColumnExpression, ColumnSettings, FeatureOp,
	NodeFilterDescription, NodeSettings, SheetEntry, SheetSettings,
	SheetValues, TextPanelSettings

derive JSONEncode Regex, Greediness, GroupId
derive JSONDecode Regex, Greediness, GroupId
JSONEncode{|{#Bool}|} b xs = JSONEncode{|*|} b [x \\ x <-: xs]
JSONDecode{|{#Bool}|} b json = appFst (fmap (\xs -> {x \\ x <- xs})) (JSONDecode{|*|} b json)

JSONEncode{|{#NodeRef}|} b xs = JSONEncode_arr b xs
JSONDecode{|{#NodeRef}|} b json = JSONDecode_arr b json
JSONEncode{|{#SheetEntry}|} b xs = JSONEncode_arr b xs
JSONDecode{|{#SheetEntry}|} b json = JSONDecode_arr b json
JSONEncode{|{#String}|} b xs = JSONEncode_arr b xs
JSONDecode{|{#String}|} b json = JSONDecode_arr b json

JSONEncode_arr b xs :== JSONEncode{|*|} b [x \\ x <-: xs]
JSONDecode_arr b json :== appFst (fmap (\xs -> {x \\ x <- xs})) (JSONDecode{|*|} b json)

defaultSheetSettings :: SheetSettings
defaultSheetSettings =
	{ nodes      = []
	, columns    = []
	, text_panel =
		{ text_node     = ?None
		, colored_nodes = []
		}
	}

isOptional :: !NodeSettings -> Bool
isOptional {optional} = fromMaybe False optional

instance toString NodeFilterDescription
where
	toString desc = pretty False desc
	where
		pretty parens desc = case desc of
			FeatureOp feat op -> case op of
				Eq val ->
					concat [feat,"=",escape val]
				Ne val ->
					concat [feat,"!=",escape val]
				EqAny vals ->
					concat [feat," in (":intersperse "," (map escape vals) ++ [")"]]
				Matches rgx ->
					concat [feat,"~",escape (toString rgx)]

			AND a b
				| parens    -> concat ["(",pretty True a," and ",pretty True b,")"]
				| otherwise -> concat [pretty True a," and ",pretty True b]
			OR a b
				| parens    -> concat ["(",pretty True a," or ",pretty True b,")"]
				| otherwise -> concat [pretty True a," or ",pretty True b]
			NOT e -> "not " +++ pretty False e
			FALSE -> "false"

			ParentOf node -> "parent of " +++ node
			ChildOf node -> "child of " +++ node

			DistanceInterval node before after
				| before+2 == after -> concat4 "distance " node "=" (toString (after-1))
				| otherwise         -> concat ["distance ",node," in (",toString before,",",toString after,")"]

instance toString ColumnExpression
where
	toString (Feature node feat) = concat [node,".",feat]
	toString (Subst search replace e) = concat ["subst(",escape search,",",escape replace,",",toString e]
	toString (Concat exprs) = concat ["concat(":intersperse "," (map toString exprs) ++ [")"]]
	toString (ConcatFor exprs vars) = concat
		[ "concat("
		: intersperse "," (map toString exprs) ++
		[ " for "
		: intersperse "," (map generatorToString vars) ++
		[ ")"
		]]]
	where
		generatorToString (type,var,filter) = concat5 type " " var " " (toString filter)
	toString LastModificationDate = ""

escape :: !String -> String
escape s = {c \\ c <- ['"':escape` [c \\ c <-: s]]}
where
	escape` [] = ['"']
	escape` [c:cs] = case c of
		'\n' -> ['\\n':escape` cs]
		'\t' -> ['\\t':escape` cs]
		'"'  -> ['\\"':escape` cs]
		'\\' -> ['\\\\':escape` cs]
		c    -> [c:escape` cs]

emptySheetValues :: SheetValues
emptySheetValues = {node_names={}, entries={}}

checkSettings :: ![(String,FeatureType)] !SheetSettings -> ?String
checkSettings all_features settings=:{nodes,columns,text_panel=tp=:{text_node,colored_nodes}}
	| isEmpty nodes
		= ?Just "You must add at least one node."
	| isEmpty columns
		= ?Just "You must add at least one column."
	| any (\n -> size n.name == 0) nodes
		= ?Just "Node names cannot be empty."
	| any (\c -> size c.title == 0) columns
		= ?Just "Column titles cannot be empty."
	| all isOptional nodes
		= ?Just "There must be at least one non-optional node."

	# mbError = order_nodes nodes
	| isError mbError
		= ?Just (fromError mbError)

	# duplicate_names = [n \\ n <- names_and_titles | length (filter ((==)n) names_and_titles) > 1]
	| not (isEmpty duplicate_names)
		= ?Just (concat ["Duplicate definition of ",hd duplicate_names,"."])

	# node_usages = concatMap used_nodes_in_node nodes
	# missing = [n \\ n=:(_,used) <- node_usages | not (isMember used node_names)]
	| not (isEmpty missing)
		# (node,depends) = hd missing
		= ?Just (concat ["Node ",node," depends on non-existing node ",depends,"."])

	# required_node_usages = filter (\(n,_) -> not (isOptional (node_by_name n))) node_usages
	# optional_deps = [u \\ u=:(_,deps) <- required_node_usages | isOptional (node_by_name deps)]
	| not (isEmpty optional_deps)
		# (node,depends) = hd optional_deps
		= ?Just (concat ["Non-optional node ",node," depends on optional node ",depends,"."])

	# node_usages = concatMap used_nodes_in_column columns
	# missing = [n \\ n=:(_,used) <- node_usages | not (isMember used node_names)]
	| not (isEmpty missing)
		# (col,depends) = hd missing
		= ?Just (concat ["Column ",col," depends on non-existing node ",depends,"."])

	| isJust text_node && not (isMember (fromJust text_node) node_names)
		= ?Just (concat ["Text node ",fromJust text_node," does not exist."])

	# missing = [n \\ (n,_) <- colored_nodes | not (isMember n node_names)]
	| not (isEmpty missing)
		= ?Just (concat ["Colored node ",hd missing," does not exist."])

	# date_cols = [c.expression \\ c <- columns | c.css_class == ?Just "date"]
	| length date_cols > 1
		= ?Just "There can be at most one date column."
	| length date_cols == 1 && not (hd date_cols)=:(?Just LastModificationDate)
		= ?Just "A date column cannot have an expression."

	# tag_cols = [c.expression \\ c <- columns | c.css_class == ?Just "tag"]
	| length tag_cols > 1
		= ?Just "There can be at most one tag column."
	| length tag_cols == 1 && isJust (hd tag_cols)
		= ?Just "A tag column cannot have an expression."

	| otherwise
		= checkRequiredFeatures (requiredFeatures settings)
where
	node_names = [n.name \\ n <- nodes]
	column_titles = [c.title \\ c <- columns]
	names_and_titles = node_names ++ column_titles

	node_by_name name = hd (filter (\n -> n.name == name) nodes)

	used_nodes_in_node {name,filterDescription} =
		[(name,node) \\ node <- used_nodes_in_filter filterDescription]

	used_nodes_in_filter f = case f of
		FeatureOp _ _ ->
			[]
		AND a b ->
			used_nodes_in_filter a ++ used_nodes_in_filter b
		OR a b ->
			used_nodes_in_filter a ++ used_nodes_in_filter b
		NOT e ->
			used_nodes_in_filter e
		FALSE ->
			[]
		ParentOf node ->
			[node]
		ChildOf node ->
			[node]
		DistanceInterval node _ _ ->
			[node]

	used_nodes_in_column {title,expression= ?Just e} =
		[(title,node) \\ node <- used_nodes_in_column_expression e]
	used_nodes_in_column _ =
		[]

	used_nodes_in_column_expression (Feature node _) = [node]
	used_nodes_in_column_expression (Subst _ _ e) = used_nodes_in_column_expression e
	used_nodes_in_column_expression (Concat es) = removeDup (concatMap used_nodes_in_column_expression es)
	used_nodes_in_column_expression (ConcatFor es vars) = removeDup $ filter (\v -> not (isMember v (map snd3 vars)))
		(concatMap used_nodes_in_column_expression es ++ concatMap (used_nodes_in_filter o thd3) vars)
	used_nodes_in_column_expression _ = []

	checkRequiredFeatures :: ![String] -> ?String
	checkRequiredFeatures [] = ?None
	checkRequiredFeatures [f:fs] = case lookup f all_features of
		?Just NodeFeature ->
			checkRequiredFeatures fs
		?Just EdgeFeature ->
			?Just (concat ["Edge feature '",f,"' is not supported yet"])
		_ ->
			?Just (concat ["Unknown feature '",f,"'"])

requiredFeatures :: !SheetSettings -> [String]
requiredFeatures s = removeDup $
	required_features s.SheetSettings.nodes ++
	required_features s.SheetSettings.columns

class required_features a :: !a -> [String]

instance required_features [a] | required_features a
where
	required_features xs = concatMap required_features xs

instance required_features NodeSettings
where
	required_features {filterDescription} = required_features filterDescription

instance required_features NodeFilterDescription
where
	required_features f = case f of
		FeatureOp f _
			-> [f]
		AND a b
			-> required_features [a,b]
		OR a b
			-> required_features [a,b]
		NOT e
			-> required_features e
		_
			-> []

instance required_features ColumnSettings
where
	required_features {expression= ?Just e} = required_features e
	required_features _ = []

instance required_features ColumnExpression
where
	required_features ce = case ce of
		Feature _ f -> [f]
		Subst _ _ e -> required_features e
		Concat es -> required_features es
		ConcatFor es vars -> required_features es ++ concatMap (required_features o thd3) vars
		LastModificationDate -> []

getValues :: !SheetSettings !DataSet -> MaybeError String SheetValues
getValues {SheetSettings | nodes,columns} data =
	order_nodes nodes >>= \nodes ->
	let node_names = [n.name \\ n <- nodes] in
	foldM (find_node_refs data node_names) ?None nodes >>= \vals ->
	case vals of
		?None ->
			Error "this sheet has no nodes"
		?Just node_ref_lists ->
			Ok
				{ node_names = {name \\ name <- node_names}
				, entries =
					{
						{ SheetEntry
						| nodes   = e
						, columns = getColumnValues node_names e columns data
						}
					\\ e <- node_ref_lists
					}
				}

order_nodes :: ![NodeSettings] -> MaybeError String [NodeSettings]
order_nodes nodes = reverse <$> order nodes []
where
	order [] added
		= Ok added
	order to_add added
		# tries = elems_and_rest to_add
		/* Avoid cycles: find a node that only depends on nodes that have already been added */
		# tries = filter (\(x,rest) -> not (any (\r -> x dependsOn r) rest)) tries
		| isEmpty tries
			= Error ("Cycle involving " +++ (hd to_add).name)
		/* If this is the first node to add; just pick any (TODO: this can be optimized) */
		| isEmpty added
			= let (add,rest) = hd tries in order rest [add]
		/* If there are already nodes added, pick one that depends on already added nodes;
		 * this avoids exponential blow-up. */
		# new_tries = filter (\(x,_) -> any (\a -> x dependsOn a) added) tries
		| isEmpty new_tries
			= Error ("Nodes " +++ (fst (hd tries)).name +++ " and " +++ (hd added).name +++ " are entirely independent")
			= let (add,rest) = hd new_tries in order rest [add:added]
	where
		elems_and_rest xs = [(x, take i xs++drop (i+1) xs) \\ i <- [0..] & x <- xs]

	(dependsOn) {filterDescription} {name} = check name filterDescription
	where
		check name filterDescription = case filterDescription of
			ParentOf c
				-> name == c
			ChildOf c
				-> name == c
			DistanceInterval c _ _
				-> name == c
			AND a b
				-> check name a || check name b
			OR a b
				-> check name a || check name b
			NOT e
				-> check name e
			_
				-> False

// NOTE: the {#Int}s are arrays of NodeRefs, one per entry. The type is Int and
// not NodeRef, because the value can be -1 as well (for no node, in case of an
// optional node), or -2 (used internally, when the node has not been found
// yet).
find_node_refs :: !DataSet ![NodeName] !(?[{#Int}]) !NodeSettings
	-> MaybeError String (?[{#Int}])
find_node_refs data node_names node_refs node_settings=:{name,type,filterDescription} =
	find_node_id name node_names >>= \node_id ->
	lift_relationships filterDescription >>=
	run_filter node_refs node_id >>=
	add_lost_entries_if_optional node_id node_refs
where
	// 1. Move {Parent,Child}Of and DistanceInterval to the start of AND nodes; prevents exponential blow-up
	// 2. Disallow these filters under OR and NOT because it breaks that optimisation (is there a use case for those expressions?)
	lift_relationships :: !NodeFilterDescription -> MaybeError String NodeFilterDescription
	lift_relationships filterDescription =
		get_relationships filterDescription >>= \(relationships,filterDescription) ->
		pure case filterDescription of
			?Just fd -> foldr (AND) fd relationships
			?None    -> 'Data.Foldable'.foldr1 (AND) relationships
	where
		get_relationships :: !NodeFilterDescription -> MaybeError String ([NodeFilterDescription], ?NodeFilterDescription)
		get_relationships p=:(ParentOf _) =
			Ok ([p],?None)
		get_relationships p=:(ChildOf _) =
			Ok ([p],?None)
		get_relationships p=:(DistanceInterval _ _ _) =
			Ok ([p],?None)
		get_relationships (AND a b) =
			get_relationships a >>= \(psa,a) ->
			get_relationships b >>= \(psb,b) -> Ok (psa++psb, case (a,b) of
				(?None,   ?None)   -> ?None
				(?None,   b)       -> b
				(a,       ?None)   -> a
				(?Just a, ?Just b) -> ?Just (AND a b))
		get_relationships (OR a b) =
			get_relationships a >>= \(psa,a) ->
			get_relationships b >>= \(psb,b) -> case (a,b) of
				(?Just a, ?Just b) | isEmpty psa && isEmpty psb
					-> Ok ([], ?Just (OR a b))
					-> Error "cannot use 'parent of' in 'or'"
		get_relationships (NOT a) =
			get_relationships a >>= \(ps,a) ->
			if (isEmpty ps && a=:(?Just _))
				(Ok ([],?Just (NOT (fromJust a))))
				(Error "cannot use 'parent of' in 'not'")
		get_relationships FALSE =
			Ok ([],?Just FALSE)
		get_relationships fop=:(FeatureOp _ _) =
			Ok ([],?Just fop)

	run_filter :: !(?[{#Int}]) !Int !NodeFilterDescription -> MaybeError String (?[{#Int}])
	run_filter node_refs node_id filterDescription = case filterDescription of
		AND a b ->
			run_filter node_refs node_id a >>= \node_refs ->
			run_filter node_refs node_id b

		ParentOf var -> case node_refs of
			?None ->
				Error "internal error in find_node_refs: ParentOf used before parent node was defined"
			?Just node_refs ->
				find_node_id var node_names >>= \var ->
				add_or_filter node_id node_refs
					(\e -> case e.[var] of
						-1    -> [| -1] // optional node without value
						child -> get_ancestor_node_refs_with (isOfType type) child data)
					(\e -> case e.[var] of
						-1    -> False // optional node without value
						child ->
							data.DataSet`.nodes.[e.[node_id]].features.[0] == type &&
							is_ancestor_of child e.[node_id] data)

		ChildOf var -> case node_refs of
			?None ->
				Error "internal error in find_node_refs: ChildOf used before child node was defined"
			?Just node_refs ->
				find_node_id var node_names >>= \var ->
				add_or_filter node_id node_refs
					(\e -> case e.[var] of
						-1    -> [| -1] // optional node without value
						child -> get_descendant_node_refs_with (isOfType type) child data)
					(\e -> case e.[var] of
						-1    -> False // optional node without value
						child ->
							data.DataSet`.nodes.[e.[node_id]].features.[0] == type &&
							is_descendant_of child e.[node_id] data)

		DistanceInterval var fr to -> case node_refs of
			?None ->
				Error "internal error in find_node_refs: DistanceInterval used before child node was defined"
			?Just node_refs ->
				find_node_id var node_names >>= \var ->
					add_or_filter node_id node_refs
						(\e -> case e.[var] of
							-1    -> [| -1] // optional node without value
							child -> get_node_refs_with_distance_between_with (isOfType type) fr to child data)
						(abort "todo\n") // TODO

		_ ->
			make_filter filterDescription >>= \nodeFilter` ->
			let
				nodeFilter = \i n d -> isOfType type i n d && nodeFilter` i n d
				matched_node_refs = filter_node_refs nodeFilter data
			in
			case node_refs of
				?None ->
					let len = length node_names in
					Ok $ ?Just [{createArray len -2 & [node_id]=ref} \\ ref <|- matched_node_refs]
				?Just node_refs ->
					add_or_filter node_id node_refs
						(const matched_node_refs)
						(\e -> nodeFilter e.[node_id] data.DataSet`.nodes.[e.[node_id]] data)

	make_filter :: NodeFilterDescription -> MaybeError String NodeFilter
	make_filter filterDescription = case filterDescription of
		FeatureOp feature op ->
			let feature_id = get_node_feature_id feature data in
			case feature_id of
				?None ->
					Error ("feature "+++feature+++" not found")
				?Just feature_id ->
					Ok \_ n _ -> check_feature_op op (get_node_feature feature_id n)

		AND a b ->
			make_filter a >>= \a ->
			make_filter b >>= \b ->
			Ok \i n d -> a i n d && b i n d
		OR a b ->
			make_filter a >>= \a ->
			make_filter b >>= \b ->
			Ok \i n d -> a i n d || b i n d
		NOT e ->
			make_filter e >>= \e ->
			Ok \i n d -> not (e i n d)
		FALSE ->
			Ok \_ _ _ -> False

		_ ->
			Error "unknown filter description in make_filter"

	add_or_filter :: !Int ![{#Int}] ({#Int} -> NodeRefList) ({#Int} -> Bool) -> m (?[{#Int}]) | pure m
	add_or_filter _ [] _ _ = pure $ ?Just []
	add_or_filter node_id entries=:[e:_] new_node_refs filter_existing
		| e.[node_id] == -2 = pure $ ?Just
			// We don't have entries yet
			[ copy_arr_with_update entry node_id new_ref
			\\ entry <- entries
			,  new_ref <|- new_node_refs entry
			]
		| otherwise = pure $ ?Just $
			// We already have entries
			filter filter_existing entries

	// run_filter may have removed entries from the results if it could not find a match for the current node.
	// If the current node is optional, we add these results again, with value -1 for the current node.
	add_lost_entries_if_optional :: !Int !(?[{#Int}]) !(?[{#Int}]) -> MaybeError String (?[{#Int}])
	add_lost_entries_if_optional _ _ mbEntries | not (isOptional node_settings) = Ok mbEntries
	add_lost_entries_if_optional _ ?None mbEntries = Ok mbEntries
	add_lost_entries_if_optional _ _ ?None = Error "add_lost_entries_if_optional: no entries"
	add_lost_entries_if_optional node_id (?Just org) (?Just new) = Ok $ ?Just $ walk org new
	where
		walk [] new = new
		walk org [] = [copy_arr_with_update o node_id -1 \\ o <- org]
		walk [o:org] allnew=:[n:new]
			| matches o n
				# (yes,no) = span (matches o) new
				= [n:yes ++ walk org no]
				= [copy_arr_with_update o node_id -1 : walk org allnew]
		where
			matches org new = and [o==n || i==node_id \\ o <-: org & n <-: new & i <- [0..]]

	copy_arr_with_update arr k val = {if (i==k) val v \\ v <-: arr & i <- [0..]}

// NB: An entry can be seen as a node where the columns are features. We can
// thus reuse `NodeFilterDescription` and later filter out the bad stuff (e.g.
// 'parent of'). This is not very neat: ideally we would have a single language
// for node filters, column expressions, and search queries.
nodeFilterDescriptionToSheetEntryCheck ::
	!{#NodeName} !NodeFilterDescription
	-> MaybeError String (SheetEntry -> Bool)
nodeFilterDescriptionToSheetEntryCheck columns filterDescription =
	case filterDescription of
		FeatureOp column op ->
			case [i \\ i <- [0..] & col <-: columns | col==column] of
				[] ->
					Error (concat ["Column "+++column+++" does not exist."])
				[i:_] ->
					Ok \e -> check_feature_op op e.SheetEntry.columns.[i]

		AND a b ->
			nodeFilterDescriptionToSheetEntryCheck columns a >>= \a ->
			nodeFilterDescriptionToSheetEntryCheck columns b >>= \b ->
			Ok \e -> a e && b e
		OR a b ->
			nodeFilterDescriptionToSheetEntryCheck columns a >>= \a ->
			nodeFilterDescriptionToSheetEntryCheck columns b >>= \b ->
			Ok \e -> a e || b e
		NOT a ->
			nodeFilterDescriptionToSheetEntryCheck columns a >>= \a ->
			Ok \e -> not (a e)

		ParentOf _ ->
			Error "The 'parent of' operator cannot be used here."

check_feature_op :: !FeatureOp !String -> Bool
check_feature_op op val = case op of
	Eq wanted ->
		wanted == val
	Ne not_wanted ->
		not_wanted <> val
	EqAny wanted ->
		any ((==) val) wanted
	Matches rgx ->
		not (isEmpty ('Regex'.match rgx val))

getColumnValue :: ![NodeName] !{#NodeRef} !ColumnExpression !DataSet -> ?String
getColumnValue node_names entry expr data = case expr of
	Feature node feature -> case find_node_id node node_names of
		Error _ -> ?None
		Ok node -> case entry.[node] of
			-1 -> ?None // optional node without value
			_  -> case get_node_feature_id feature data of
				?Just feature
					-> ?Just data.DataSet`.nodes.[entry.[node]].features.[feature]
					-> ?None
	Subst search replace e ->
		replaceSubString search replace <$> getColumnValue node_names entry e data
	Concat exprs -> ?Just $ concat [fromMaybe "" (getColumnValue node_names entry e data) \\ e <- exprs]
	ConcatFor exprs vars ->
		case foldM
				(find_node_refs data extended_node_names)
				(?Just [{createArray (size entry + length vars) -2 & [i]=j \\ i <- [0..] & j <-: entry}])
				(map toNodeSettings vars) of
			Error e ->
				?Just e
			Ok ?None ->
				?Just "check assumptions"
			Ok (?Just nodes) ->
				?Just $ concat $
				map (\e -> fromMaybe "?" $ getColumnValue extended_node_names e (Concat exprs) data) nodes
		//concat <$>
		//map (\e -> fromMaybe "?" $ getColumnValue extended_node_names e (Concat exprs) data) <$>
		//join (error2mb $ foldM
		//	(find_node_refs data node_names)
		//	(?Just [{createArray (size entry + length vars) -2 & [i]=j \\ i <- [0..] & j <-: entry}])
		//	(map toNodeSettings vars)))
	where
		extended_node_names = node_names ++ map snd3 vars

		toNodeSettings (type, var, filter) =
			{ name = var
			, type = type
			, optional = ?Just False
			, filterDescription = filter
			}
	LastModificationDate -> ?None

getColumnValues :: ![NodeName] !{#NodeRef} ![ColumnSettings] !DataSet -> .{#String}
getColumnValues node_names entry columns data =
	{ maybe "" (\e -> fromMaybe "" (getColumnValue node_names entry e data)) col.expression
	\\ col <- columns
	}

find_node_id :: !NodeName ![NodeName] -> MaybeError String Int
find_node_id name ns = case [i \\ i <- [0..] & n <- ns | n == name] of
	[i:_] -> Ok i
	_     -> Error "internal error in MBQS.Model: node not found"
