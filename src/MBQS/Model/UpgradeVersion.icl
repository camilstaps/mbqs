implementation module MBQS.Model.UpgradeVersion

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Error
import Data.Func
import Data.Functor
import qualified Data.Map
from Data.Map import instance Functor (Map k)

import TextFabric
import TextFabric.NodeMapping

import MBQS.Model

upgradeSheetValues :: !NodeMapping !MappingChoices !SheetValues -> MaybeError [(Int, Int, NodeRef)] SheetValues
upgradeSheetValues mapping mappingChoices values=:{entries}
	| any isError upgraded =
		Error [n \\ Error ns <- upgraded, n <- ns]
	| otherwise =
		Ok {values & entries = {e \\ Ok e <- upgraded}}
where
	upgraded = [upgradeEntry mapping mappingChoices i e \\ i <- [0..] & e <-: entries]

upgradeEntry :: !NodeMapping !MappingChoices !Int !SheetEntry -> MaybeError [(Int, Int, NodeRef)] SheetEntry
upgradeEntry mapping mappingChoices i entry=:{SheetEntry|nodes}
	| any isError upgraded =
		Error [e \\ Error e <- upgraded]
	| otherwise =
		Ok {SheetEntry | entry & nodes = {e \\ Ok e <- upgraded}}
where
	upgraded =
		[ case 'Data.Map'.get (i, j, n) mappingChoices of
			?Just target ->
				Ok target
			?None ->
				case upgradeNode mapping n of
					?Just target ->
						Ok target
					?None ->
						Error (i, j, n)
		\\ j <- [0..] & n <-: nodes
		]

upgradeNode :: !NodeMapping !NodeRef -> ?NodeRef
upgradeNode mapping ref = case 'Data.Map'.get ref mapping of
	?Just [|{target,dissimilarity= ?None}] ->
		?Just target
	_ ->
		?None
