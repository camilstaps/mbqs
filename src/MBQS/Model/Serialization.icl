implementation module MBQS.Model.Serialization

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import qualified StdArray
import StdMaybe

import Data.Error
import Data.List
import Data.Tuple
from Text import class Text(concat,join,rpad,split), instance Text String
import Text.GenJSON

import Util

import MBQS.Model

VERSION :== 1

writeSheet :: !SheetSettings !SheetValues !*File -> *File
writeSheet settings values f
	# f = f <<< "tsheet\r\n"
	# f = f <<< toString VERSION <<< "\r\n"
	# f = f <<< "2\r\n"
	# (pos,f) = fposition f
	# f = prepare_header 2 f
	# (size_settings,f) = write_settings settings f
	# (size_values,f) = write_values values f
	# (_,f) = fseek f pos FSeekSet
	# f = write_header
		[ ("settings",size_settings)
		, ("values",size_values)
		] 0 f
	# (_,f) = fseek f 0 FSeekEnd
	= f
where
	prepare_header :: !Int !*File -> *File
	prepare_header n_parts f = f <<< {#'\0' \\ _ <- [1..n_parts*32]}

	write_header :: ![(String,Int)] !Int !*File -> *File
	write_header [(name,size):rest] acc f
		# f = f <<< rpad (concat [name," ",toString acc]) 30 ' ' <<< "\r\n"
		= write_header rest (acc+size) f
	write_header [] _ f
		= f

	write_settings :: !SheetSettings !*File -> (!Int, !*File)
	write_settings settings f
		# json = toString (toJSON settings)
		= (size json+2, f <<< json <<< "\r\n")

	write_values :: !SheetValues !*File -> (!Int, !*File)
	write_values {node_names,entries} f
		# (sz1,f) = write_node_names node_names f
		# (sz2,f) = write_node_count entries f
		# (sz3,f) = write_entries entries 0 0 f
		= (sz1+sz2+sz3, f)
	where
		write_node_names :: !{#NodeName} !*File -> (!Int, !*File)
		write_node_names names f
			# s = join ";" [n \\ n <-: names]
			= (size s+2, f <<< s <<< "\r\n")

		write_node_count :: !{#SheetEntry} !*File -> (!Int, !*File)
		write_node_count entries f
			# s = toString (size entries)
			= (size s+2, f <<< s <<< "\r\n")

		write_entries :: !{#SheetEntry} !Int !Int !*File -> (!Int, !*File)
		write_entries entries i acc f
			| i >= size entries
				= (acc, f)
			# {SheetEntry | nodes,columns} = entries.[i]
			# s = join "\r\n" [join "," [toString r \\ r <-: nodes]:[jsonEscape c \\ c <-: columns]]
			= write_entries entries (i+1) (acc+size s+2) (f <<< s <<< "\r\n")

readSheet :: !*File -> (!MaybeError String (SheetSettings, ?SheetValues), !*File)
readSheet f
	# (line,f) = freadline f
	# line = trimLineEndings line
	| line <> "tsheet"
		= (Error ("incorrect header: "+++line), f)

	# (line,f) = freadline f
	# line = trimLineEndings line
	| toInt line <> VERSION
		= (Error ("unknown file version: "+++line), f)

	# (line,f) = freadline f
	# nparts = toInt (trimLineEndings line)
	# (header,f) = read_header nparts f
	| isNone header
		= (Error "failed to read header", f)
	# header = fromJust header

	# (start,f) = fposition f

	# (settings,f) = read_part "settings" read_settings start header f
	| isError settings
		= (liftError settings, f)
	# settings = fromOk settings
	| isNone settings
		= (Error "failed to read settings", f)
	# settings = fromJust settings

	# (values,f) = read_part "values" (read_values (length settings.SheetSettings.columns)) start header f
	| isError values
		= (liftError values, f)
		= (Ok (settings, fromOk values), f)
where
	read_header :: !Int !*File -> (!?[(String,Int)], !*File)
	read_header 0 f
		= (?Just [], f)
	read_header n f
		# (line,f) = freadline f
		= case split " " line of
			[name,offset:_] -> case read_header (n-1) f of
				(?Just rest,f) -> (?Just [(name,toInt offset):rest], f)
				(?None,f)      -> (?None, f)
			_ -> (?None, f)

	read_part :: !String !(*File -> (?a, *File)) !Int ![(String, Int)] !*File -> (!MaybeError String (?a), !*File)
	read_part name read start header f = case lookup name header of
		?None
			-> (Ok ?None, f)
		?Just offset
			# (ok,f) = fseek f (start+offset) FSeekSet
			| not ok
				-> (Error ("unexpected end of file in "+++name+++" section"), f)
				-> appFst Ok (read f)

	read_settings :: !*File -> (!?SheetSettings, !*File)
	read_settings f
		# (line,f) = freadline f
		= (fromJSON (fromString line), f)

	read_values :: !Int !*File -> (!?SheetValues, !*File)
	read_values ncols f
		# (line,f) = freadline f
		# node_names = split ";" (trimLineEndings line)
		# (line,f) = freadline f
		# line = trimLineEndings line
		# node_count = toInt line
		# (end,f) = fend f
		| end && node_count <> 0
			= (?None, f)
		# (values,f) = read_entries 0 node_count ncols ('StdArray'._createArray node_count) f
		= case values of
			?None    -> (?None, f)
			?Just es -> (?Just {node_names={n \\ n <- node_names}, entries=es}, f)
	where
		read_entries :: !Int !Int !Int !*{#*SheetEntry} !*File -> (!? .{#.SheetEntry}, !*File)
		read_entries i total ncols es f
			| i == total
				= (?Just es, f)
			# (end,f) = fend f
			| end
				= (?None, f)
			# (nodes_line,f) = freadline f
			# (values,f) = read_lines 0 ncols (createArray ncols "") f
			| isNone values
				= (?None, f)
			# es & [i] =
				{ SheetEntry
				| nodes   = {toInt n \\ n <- split "," (trimLineEndings nodes_line)}
				, columns = fromJust values
				}
			= read_entries (i+1) total ncols es f

		read_lines :: !Int !Int !*{#String} !*File -> (!? .{#String}, !*File)
		read_lines i total arr f
			| i == total
				= (?Just arr, f)
			# (end,f) = fend f
			| end
				= (?None, f)
			# (line,f) = freadline f
			= read_lines (i+1) total {arr & [i]=jsonUnescape (trimLineEndings line)} f
