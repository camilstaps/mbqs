definition module MBQS.Model.UpgradeVersion

/**
 * This module is used to update a sheet from e.g. version `c` to version
 * `2021`.
 *
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError
from Data.Map import :: Map

from TextFabric import :: NodeRef
from TextFabric.NodeMapping import :: NodeMapping, :: Target

from MBQS.Model import :: SheetValues

/**
 * For nodes that have multiple target options in the `NodeMapping`, the user
 * has to make a choice. These choices are collected in this type. See the
 * documentation on `upgradeSheetValues` for more explanation.
 */
:: MappingChoices :== Map (Int, Int, NodeRef) NodeRef

/**
 * Upgrades sheet values to a new version of the data.
 *
 * **The node filters are not used to do a new search.** Because node features
 * and slots may have changed the same sheet settings may give a different set
 * of sheet entries in the new dataset. But this function will only update the
 * old node references to new ones, and not change any data: no entries will be
 * added or removed.
 *
 * **This also means that column values are not updated.** There may be cases
 * in which not only node references have changed but also feature values (this
 * is probably the more common case). But in this case, the sheet settings
 * (i.e., the column expressions) are not used to find the new column values.
 *
 * In the end, this means that upgrading a sheet will have two consequences:
 *
 * 1. If a node is used as `text_node` or in `colored_nodes`, the displaying of
 *    the entry will use the new reference.
 * 2. If later a column with expression is added to the sheet, the new
 *    reference will be used to get the feature value.
 *
 * @param The mapping from the old to the new version.
 * @param Choices made for cases where more than one target node is available.
 * @param The values to upgrade.
 * @result
 *   When for all nodes in the values there is either only one candidate in the
 *   mapping or a choice is provided, the result is an `Ok` in which the nodes
 *   have been updated.
 *
 *   When there are nodes for which there are more than one candidate *and* no
 *   choice is made yet, the result will be an `Error`. The error contains a
 *   list of all cases: the two `Int`s form a unique identifier of the entry
 *   (the index in the sheet values) and the node (the index of the node in the
 *   `nodes` of the `SheetSettings`); the `NodeRef` is the node for which more
 *   than one candidate is available. This tuple must be used in the
 *   `MappingChoices` when the function is called again to get an `Ok` result.
 */
upgradeSheetValues :: !NodeMapping !MappingChoices !SheetValues -> MaybeError [(Int, Int, NodeRef)] SheetValues
