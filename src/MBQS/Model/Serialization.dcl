definition module MBQS.Model.Serialization

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError

from MBQS.Model import :: SheetSettings, :: SheetValues

writeSheet :: !SheetSettings !SheetValues !*File -> *File
readSheet :: !*File -> (!MaybeError String (SheetSettings, ?SheetValues), !*File)
