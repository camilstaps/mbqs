definition module MBQS.Languages

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from StdOverloaded import class ==

from Control.Applicative import class pure, class <*>, class Applicative, class Alternative
from Control.Monad import class Monad
from Data.Error import :: MaybeError
from Data.Functor import class Functor

from Regex import :: Regex

from MBQS.Model import :: NodeName, :: NodeFilterDescription, :: ColumnExpression

:: Token
	= TIdent !String
	// Literals
	| TInt !Int
	| TString !String
	// Punctuation
	| TParenOpen | TParenClose
	| TDot
	| TComma
	// Operators
	| TAnd
	| TOr
	| TNot
	| TEq
	| TNe
	| TTilde
	// Other keywords
	| TIn
	| TOf
	| TParent
	| TChild
	| TDistance
	| TSubst
	| TConcat
	| TFor

instance == Token

class LanguageView p
	| nonTerminal, comment
	, token
	, nonobligatory, sepBy1
	, ident, string, int, regex
	, Alternative, Monad p

class nonTerminal p :: !String !(p a) -> p a
class comment p :: !String -> p ()

class nonobligatory p :: !(p a) -> p (?a)
class sepBy1 p :: (p sep) !(p a) -> p [a]

class token p :: !Token -> p Token

class ident p :: p String
class string p :: p String
class int p :: p String
class regex p :: p Regex

identOrString :: p String | nonTerminal, ident, string, Alternative p
value :: p String | nonTerminal, ident, string, int, Alternative p
valueSet :: p [String] | nonTerminal, token, sepBy1, ident, string, int, Alternative, Monad p

columnExpression :: p ColumnExpression | LanguageView p
nodeFilter :: p NodeFilterDescription | LanguageView p
