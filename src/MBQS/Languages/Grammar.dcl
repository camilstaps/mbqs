definition module MBQS.Languages.Grammar

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Control.Applicative import class pure, class <*>, class Applicative, class Alternative
from Control.Monad import class Monad
from Data.Functor import class Functor
from Text.HTML import :: HtmlTag

import MBQS.Languages

:: Grammar t

grammarToHTML :: !(Grammar t) -> HtmlTag

instance Functor Grammar
where
	fmap :: (a -> b) !(Grammar a) -> Grammar b

instance pure Grammar
instance <*> Grammar

instance Alternative Grammar
where
	empty :: Grammar a
	(<|>) :: !(Grammar a) !(Grammar a) -> Grammar a

instance Monad Grammar

instance nonTerminal Grammar
instance comment Grammar

instance nonobligatory Grammar
instance sepBy1 Grammar

instance token Grammar

instance ident Grammar
instance string Grammar
instance int Grammar
instance regex Grammar
