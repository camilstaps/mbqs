implementation module MBQS.Languages.Parsers

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Applicative
import Control.Monad
import Data.Either
import Data.Error
import Data.Functor
from Text import class Text(concat), instance Text String
import Text.GenJSON
import Text.Parsers.Simple.Core

import qualified Regex

import MBQS.Languages

tokenize :: !Int ![Token] !String -> MaybeError String [Token]
tokenize i tks s
	| i >= size s
		= Ok (reverse tks)
	# c = s.[i]
	= case c of
		'(' ->
			tokenize (i+1) [TParenOpen:tks] s
		')' ->
			tokenize (i+1) [TParenClose:tks] s
		'.' ->
			tokenize (i+1) [TDot:tks] s
		',' ->
			tokenize (i+1) [TComma:tks] s
		'=' ->
			tokenize (i+1) [TEq:tks] s
		'~' ->
			tokenize (i+1) [TTilde:tks] s
		'"' ->
			case scan_utf8_string (i+1) of
				Error e ->
					Error (concat [e," at position ",toString i])
				Ok j ->
					tokenize j [TString (jsonUnescape (s % (i+1,j-2))):tks] s
		'!' | i+1 < size s && s.[i+1]=='=' ->
			tokenize (i+2) [TNe:tks] s
		'a' | i+2 < size s && s.[i+1]=='n' && s.[i+2]=='d' && noIdentChar (i+3) s ->
			tokenize (i+3) [TAnd:tks] s
		'n' | i+2 < size s && s.[i+1]=='o' && s.[i+2]=='t' && noIdentChar (i+3) s ->
			tokenize (i+3) [TNot:tks] s
		'o' | i+1 < size s && s.[i+1]=='r' && noIdentChar (i+2) s ->
			tokenize (i+2) [TOr:tks] s
		'i' | i+1 < size s && s.[i+1]=='n' && noIdentChar (i+2) s ->
			tokenize (i+2) [TIn:tks] s
		'o' | i+1 < size s && s.[i+1]=='f' && noIdentChar (i+2) s ->
			tokenize (i+2) [TOf:tks] s
		'p' | i+5 < size s && s.[i+1]=='a' && s.[i+2]=='r' && s.[i+3]=='e' &&
				s.[i+4]=='n' && s.[i+5]=='t' && noIdentChar (i+6) s ->
			tokenize (i+6) [TParent:tks] s
		'c' | i+4 < size s && s.[i+1]=='h' && s.[i+2]=='i' && s.[i+3]=='l' &&
				s.[i+4]=='d' && noIdentChar (i+5) s ->
			tokenize (i+5) [TChild:tks] s
		'd' | i+7 < size s && s.[i+1]=='i' && s.[i+2]=='s' && s.[i+3]=='t' &&
				s.[i+4]=='a' && s.[i+5]=='n' && s.[i+6]=='c' && s.[i+7]=='e' &&
				noIdentChar (i+8) s ->
			tokenize (i+8) [TDistance:tks] s
		's' | i+4 < size s && s.[i+1]=='u' && s.[i+2]=='b' && s.[i+3]=='s' &&
				s.[i+4]=='t' && noIdentChar (i+5) s ->
			tokenize (i+5) [TSubst:tks] s
		'c' | i+5 < size s && s.[i+1]=='o' && s.[i+2]=='n' && s.[i+3]=='c' &&
				s.[i+4]=='a' && s.[i+5]=='t' && noIdentChar (i+6) s ->
			tokenize (i+6) [TConcat:tks] s
		'f' | i+2 < size s && s.[i+1]=='o' && s.[i+2]=='r' && noIdentChar (i+3) s ->
			tokenize (i+3) [TFor:tks] s
		c | isDigit c || (c=='-' && size s > i+1 && isDigit s.[i+1])
			# j = scan_int (i+1)
			-> tokenize j [TInt (toInt (s % (i,j-1))):tks] s
		c | isBeginIdentChar c
			# j = scan_ident i
			-> tokenize j [TIdent (s % (i,j-1)):tks] s
		c | isSpace c
			-> tokenize (i+1) tks s
		_
			-> Error (concat ["Unexpected character '",one_utf8_char i,"' at position ",toString i])
where
	isBeginIdentChar c = isAlpha c
	isIdentChar c = isAlphanum c || c=='_'
	noIdentChar i s = i >= size s || not (isIdentChar s.[i])

	scan_utf8_string :: !Int -> MaybeError String Int
	scan_utf8_string i
		| i >= size s
			= Error "Unterminated string"
		# c = s.[i]
		| c == '"'
			= Ok (i+1)
		| c == '\\' && i+1 < size s
			| isMember s.[i+1] ['nt\\"']
				= scan_utf8_string (i+2)
				= Error (concat ["Unknown escape sequence \\",{s.[i+1]}," in string"])
		# ci = toInt c
		| ci < 128 // 1-byte code point
			= scan_utf8_string (i+1)
		| ci bitand 0xe0 == 0xc0 && i+1 < size s && // 2-byte code point
				toInt s.[i+1] bitand 0xc0 == 0x80
			= scan_utf8_string (i+2)
		| ci bitand 0xf0 == 0xe0 && i+2 < size s && // 3-byte code point
				toInt s.[i+1] bitand 0xc0 == 0x80 &&
				toInt s.[i+2] bitand 0xc0 == 0x80
			= scan_utf8_string (i+3)
		| ci bitand 0xf8 == 0xf0 && i+3 < size s && // 3-byte code point
				toInt s.[i+1] bitand 0xc0 == 0x80 &&
				toInt s.[i+2] bitand 0xc0 == 0x80 &&
				toInt s.[i+3] bitand 0xc0 == 0x80
			= scan_utf8_string (i+4)
			= Error "Invalid UTF-8 sequence in string"

	one_utf8_char :: !Int -> String
	one_utf8_char i // Illegal UTF-8 sequences are caught by scan_utf8_string
		# c = toInt s.[i]
		| c < 128 // one-byte code point
			= {#s.[i]}
		# end = scan_to_end (i+1)
		= s % (i,end)
	where
		scan_to_end i
			| i >= size s
				= i-1
			| toInt s.[i] bitand 0xc0 == 0x80
				= scan_to_end (i+1)
				= i-1

	scan_int :: !Int -> Int
	scan_int i
		| i >= size s || not (isDigit s.[i])
			= i
			= scan_int (i+1)

	scan_ident :: !Int -> Int
	scan_ident i
		| i >= size s || not (isIdentChar s.[i])
			= i
			= scan_ident (i+1)

isValidNodeName :: !NodeName -> Bool
isValidNodeName s = case tokenize 0 [] s of
	Ok [TIdent _]
		-> True
		-> False

parseNodeFilterDescription :: !String -> MaybeError String NodeFilterDescription
parseNodeFilterDescription s = tokenize_and_parse nodeFilter s

parseColumnExpression :: !String -> MaybeError String ColumnExpression
parseColumnExpression s = tokenize_and_parse columnExpression s

tokenize_and_parse :: !(Parser Token a) !String -> MaybeError String a
tokenize_and_parse parser s =
	tokenize 0 [] s >>= \tks ->
	case parse parser tks of
		Right r    -> Ok r
		Left [e:_] -> Error e
		Left []    -> Error "unknown error"

instance nonTerminal (Parser Token) where nonTerminal _ p = p

instance comment (Parser Token) where comment _ = pure ()

instance nonobligatory (Parser Token) where nonobligatory p = optional p

instance sepBy1 (Parser Token) where sepBy1 psep px = pSepBy1 px psep

instance token (Parser Token) where token t = pToken t

instance ident (Parser Token)
where
	ident = (\(TIdent id) -> id) <$> pSatisfy (\t -> t=:(TIdent _))

instance string (Parser Token)
where
	string = (\(TString s) -> s) <$> pSatisfy (\t -> t=:(TString _))

instance int (Parser Token)
where
	int = (\(TInt i) -> toString i) <$> pSatisfy (\t -> t=:(TInt _))

instance regex (Parser Token)
where
	regex = identOrString >>= \s -> case 'Regex'.parseRegex [c \\ c <-: s] of
		Error e -> pError e
		Ok rgx  -> pYield rgx
