implementation module MBQS.Languages.Grammar

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Applicative
import Control.Monad
import Data.Func
import Data.Functor
import Data.List
import qualified Text
from Text import class Text, instance Text String
import Text.GenJSON
import Text.HTML

import MBQS.Languages

:: Grammar t = Grammar !([GrammarRule] -> ([GrammarAlternative], [GrammarRule]))

:: GrammarRule =
	{ nonterminal  :: !String
	, alternatives :: ![GrammarAlternative]
	}

:: GrammarAlternative
	= LiteralToken !Token
	| NonTerminal !String
	| Comment !String
	| AnyPart ![GrammarAlternative]
	| Concat ![GrammarAlternative]
	| Optional !GrammarAlternative
	| Href !String !String

grammarToHTML :: !(Grammar t) -> HtmlTag
grammarToHTML (Grammar g)
	# (_,rules) = g []
	= DivTag [ClassAttr "grammar"] (map ruleToHTML (reverse rules))

ruleToHTML :: !GrammarRule -> HtmlTag
ruleToHTML {nonterminal,alternatives=[alt:alts]} = DivTag [ClassAttr "grammar-rule"]
	[ SpanTag [ClassAttr "grammar-nonterminal"] [Text nonterminal]
	, DivTag [ClassAttr "grammar-alternative"]
		[ SpanTag [ClassAttr "grammar-syntax"] [Text "= "]
		: alternativeToHTML alt
		]
	:
		[ DivTag [ClassAttr "grammar-alternative"]
			[ SpanTag [ClassAttr "grammar-syntax"] [Text "| "]
			: alternativeToHTML a
			]
		\\ a <- alts
		]
	]

alternativeToHTML :: !GrammarAlternative -> [HtmlTag]
alternativeToHTML (LiteralToken t) = [SpanTag [ClassAttr "grammar-token"] [Text (toString t)]]
alternativeToHTML (NonTerminal nt) = [SpanTag [ClassAttr "grammar-nonterminal"] [Text nt]]
alternativeToHTML (Comment c) = [SpanTag [ClassAttr "grammar-comment"] [Text c]]
alternativeToHTML (AnyPart []) = []
alternativeToHTML (AnyPart [p]) = alternativeToHTML p
alternativeToHTML (AnyPart ps) = [SpanTag [ClassAttr "grammar-any"]
	[ Text "("
	: intersperse (Text "|") [SpanTag [ClassAttr "grammar-any-part"] (alternativeToHTML p) \\ p <- ps]
		++ [Text ")"]
	]]
alternativeToHTML (Concat alts) = concatMap alternativeToHTML alts
alternativeToHTML (Optional alt) = [SpanTag
	[ClassAttr "grammar-nonobligatory"]
	[Text "(":alternativeToHTML alt ++ [Text ")?"]]]
alternativeToHTML (Href url s) = [ATag [HrefAttr url] [Text s]]

instance toString Token
where
	toString t = case t of
		TIdent id   -> id
		TInt i      -> toString i
		TString s   -> 'Text'.concat ["\"", jsonEscape s, "\""]
		TParenOpen  -> "("
		TParenClose -> ")"
		TDot        -> "."
		TComma      -> ","
		TAnd        -> "and"
		TOr         -> "or"
		TNot        -> "not"
		TEq         -> "="
		TNe         -> "!="
		TTilde      -> "~"
		TIn         -> "in"
		TOf         -> "of"
		TParent     -> "parent"
		TChild      -> "child"
		TDistance   -> "distance"
		TSubst      -> "subst"
		TConcat     -> "concat"
		TFor        -> "for"

nop = Grammar \rules -> ([], rules)
add part = Grammar \rules -> ([part], rules)

instance Functor Grammar
where
	fmap :: (a -> b) !(Grammar a) -> Grammar b
	fmap _ (Grammar f) = Grammar f

instance pure Grammar
where
	pure _ = nop

instance <*> Grammar
where
	(<*>) l r = ap l r

instance Alternative Grammar
where
	empty :: Grammar a
	empty = nop

	(<|>) :: !(Grammar a) !(Grammar a) -> Grammar a
	(<|>) (Grammar l) (Grammar r) = Grammar \rules
		# (lalts,rules) = l rules
		# (ralts,rules) = r rules
		-> (lalts ++ ralts, rules)

instance Monad Grammar
where
	bind (Grammar l) fr = Grammar \rules
		# (lalts,rules) = l rules
		# (Grammar r) = fr undef
		# (ralts,rules) = r rules
		-> ([Concat [AnyPart lalts, AnyPart ralts]], rules)

instance nonTerminal Grammar
where
	nonTerminal name (Grammar g) = Grammar \rules
		# matches = filter (\r -> r.nonterminal == name) rules
		-> case matches of
			[_:_]
				-> ([NonTerminal name], rules)
			[]
				# (alts,rules) = g [{nonterminal=name, alternatives=[]}:rules]
				-> ([NonTerminal name], [if (r.nonterminal==name) {r & alternatives=alts} r \\ r <- rules])

instance comment Grammar
where
	comment c = add (Comment ("-- "+++c))

instance nonobligatory Grammar
where
	nonobligatory (Grammar f) = Grammar \rules
		# (alts,rules) = f rules
		-> case alts of
			[]  -> ([], rules)
			[a] -> ([Optional a], rules)
			as  -> ([Optional (AnyPart as)], rules)

instance sepBy1 Grammar
where
	sepBy1 sep x = x >>| add (Comment " separated by ") >>| sep $> []

instance token Grammar
where
	token t = add (LiteralToken t)

instance ident Grammar
where
	ident = nonTerminal "identifier" $ add $ Comment
		"an alphabetic character followed by a sequence of alphanumeric characters and underscores"

instance string Grammar
where
	string = nonTerminal "string" $ add $ Comment
		"a sequence of characters delimited by double quotation marks (\")"

instance int Grammar
where
	int = nonTerminal "integer" $ add $ Comment
		"a non-empty sequence of digits"

instance regex Grammar
where
	regex = nonTerminal "regular-expression" $ add $ Concat
		[ Comment "an "
		, NonTerminal "identifier"
		, Comment " or "
		, NonTerminal "string"
		, Comment ", which will be parsed as a "
		, Href "https://www.regular-expressions.info/" "regular expression"
		]
