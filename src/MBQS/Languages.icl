implementation module MBQS.Languages

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Applicative
import Control.Monad
import Data.Either
import Data.Error
from Data.Func import $
import Data.Functor
import Data.Maybe
import Data.Tuple

import MBQS.Model

(|<<) infixl 1 :: !(m a) !(m b) -> m a | Monad m
(|<<) ma mb = ma >>= \a -> mb >>= \_ -> pure a

instance == Token
where
	(==) a b = case a of
		TIdent a    -> case b of TIdent b -> a == b; _ -> False
		TInt a      -> case b of TInt b -> a == b; _ -> False
		TString a   -> case b of TString b -> a == b; _ -> False
		TParenOpen  -> b=:TParenOpen
		TParenClose -> b=:TParenClose
		TDot        -> b=:TDot
		TComma      -> b=:TComma
		TAnd        -> b=:TAnd
		TOr         -> b=:TOr
		TNot        -> b=:TNot
		TEq         -> b=:TEq
		TNe         -> b=:TNe
		TTilde      -> b=:TTilde
		TIn         -> b=:TIn
		TOf         -> b=:TOf
		TParent     -> b=:TParent
		TChild      -> b=:TChild
		TDistance   -> b=:TDistance
		TSubst      -> b=:TSubst
		TConcat     -> b=:TConcat
		TFor        -> b=:TFor

identOrString :: p String | nonTerminal, ident, string, Alternative p
identOrString = ident <|> string

value :: p String | nonTerminal, ident, string, int, Alternative p
value = nonTerminal "value" $
	identOrString <|> int

valueSet :: p [String] | nonTerminal, token, sepBy1, ident, string, int, Alternative, Monad p
valueSet = nonTerminal "value-set" $
	token TParenOpen >>| sepBy1 (token TComma) value |<< token TParenClose

columnExpression :: p ColumnExpression | LanguageView p
columnExpression = nonTerminal "column-expression" (
	liftM2 Feature
		identOrString
		(token TDot >>| ident |<< comment "n.f takes feature f of node n")
	<|> liftM3 Subst
		(token TSubst >>| token TParenOpen >>| value)
		(token TComma >>| value)
		(token TComma >>| columnExpression |<< token TParenClose |<< comment "subst(s,r,e) replaces s by r in e")
	<|> liftM Concat
		(token TConcat >>| token TParenOpen >>| sepBy1 (token TComma) columnExpression |<< token TParenClose
			|<< comment "concat(e1,e2,...) concatenates the expressions e1,e2,...")
	<|> liftM2 ConcatFor
		(token TConcat >>| token TParenOpen >>| sepBy1 (token TComma) columnExpression)
		(token TFor >>| sepBy1 (token TComma) (liftM3 tuple3 ident ident nodeFilter) |<< token TParenClose
			|<< comment "concat(e1,e2,... for node_type x filter, node_type y filter, ...) concatenates the expressions e1,e2,... for all x,y,...")
	)

nodeFilter :: p NodeFilterDescription | LanguageView p
nodeFilter = nonTerminal "node-filter" $
	rightAssoc "or" (op TOr OR) $
	rightAssoc "and" (op TAnd AND) $
	noInfix
where
	rightAssoc ::
		!String
		(p (NodeFilterDescription NodeFilterDescription -> NodeFilterDescription))
		(p NodeFilterDescription)
		-> p NodeFilterDescription
		| LanguageView p
	rightAssoc name opp descp = nonTerminal ("node-filter-"+++name) (
		descp >>= \d1 ->
		nonobligatory (opp >>= \op -> rightAssoc name opp descp >>= \d -> pure (op,d)) >>=
		pure o maybe d1 (\(op,d2) -> op d1 d2)
		)

	noInfix :: p NodeFilterDescription | LanguageView p
	noInfix = nonTerminal "no-infix" (
		NOT <$> (token TNot >>| noInfix)
		<|> liftM2 FeatureOp identOrString (featureFilter |<< comment "filter based on a feature")
		<|> ParentOf <$> (token TParent >>| token TOf >>| identOrString)
		<|> ChildOf <$> (token TChild >>| token TOf >>| identOrString)
		<|> liftM3 DistanceInterval
			(token TDistance >>| identOrString)
			(token TIn >>| token TParenOpen >>| toInt <$> int)
			(token TComma >>| toInt <$> int |<< token TParenClose |<< comment
				"distance n in (x,y) finds nodes where the distance d to node n satisfies x < d < y")
		<|> liftM2 (\node distance -> DistanceInterval node (distance-1) (distance+1))
			(token TDistance >>| identOrString)
			(token TEq >>| toInt <$> int |<< comment
				"distance n=x is a shorthand for distance n in (x-1,x+1)")
		<|> (token TParenOpen >>| nodeFilter |<< token TParenClose)
		)

	featureFilter :: p FeatureOp | LanguageView p
	featureFilter = nonTerminal "feature-filter" (
		Eq <$> (token TEq >>| value |<< comment "equality")
		<|> Ne <$> (token TNe >>| value |<< comment "inequality")
		<|> EqAny <$> (token TIn >>| valueSet |<< comment "equality to any of the set members")
		<|> Matches <$> (token TTilde >>| regex |<< comment "matching a regular expression")
		)

	op :: !Token !(a a -> a) -> p (a a -> a) | LanguageView p
	op t f = token t $> f
