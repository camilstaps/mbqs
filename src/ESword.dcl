definition module ESword

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from StdMaybe import :: Maybe

from Data.Error import :: MaybeError
from System.FilePath import :: FilePath
from System._Pointer import :: Pointer

from Bible import :: Book, :: Reference

/**
 * NB: This has the same representation as an `SQLiteCursor`, so that we may
 * cast them to each other. Don't use these fields directly.
 */
:: ESwordModule =
	{ sqlite_conn_ptr :: !Pointer
	, sqlite_stmt_ptr :: !Pointer
	, sqlite_step_res :: !Int
	, sqlite_num_cols :: !Int
	}

openModule :: !FilePath !*World -> (!MaybeError String ESwordModule, !*World)

getMaxVerse :: !Book !Int !ESwordModule -> (!MaybeError String Int, !ESwordModule)

getVerse :: !Reference !ESwordModule -> (!MaybeError String String, !ESwordModule)

/**
 * @param Whether to allow footnotes in the string.
 */
eSwordMarkupToHTML :: !Bool !String -> String
