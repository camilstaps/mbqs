implementation module Gui.NewSheet

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Monad => qualified return, forever, sequence, join
import Data.Func
import Data.Functor
import Data.List => qualified group
from Text import class Text(join), instance Text String, concat4
import Text.HTML

import iTasks
import iTasks.Extensions.DateTime
import iTasks.UI.Editor.Common

import iTasks.Extensions.ColorPicker

import Regex.Print

import MBQS.Languages
import MBQS.Languages.Grammar
import MBQS.Languages.Parsers
import MBQS.Model

import Gui.Common
import Gui.Derives
import Gui.DistinctColumnValues
import Gui.NewSheet.Examples
import Gui.Shares
import Gui.Sheet
import Gui.Util

derive class iTask \ gEditor SheetSettingsInput, NodeSettingsInput

createNewSheet :: Task SheetSettings
createNewSheet =
	getAllFeatures >>- \all_features ->
	withShared {SheetSettingsInput | nodes=[], columns=[], text_node=""} \input ->
	edit all_features input
where
	edit all_features input =
		(ScrollContent @>> AddCSSClass "small" @>> AddCSSClass "new-sheet-editor" @>>
			(updateSharedInformation [] input >&> \mbInput ->
			viewSharedInformation [ViewAs (errorsToHtml all_features)] mbInput ||-
			watch mbInput)) >>*
		[ OnAction (Action "Load example") $ always $
			loadExample input >-|
			edit all_features input
		, OnAction (Action "Preview") $ ifValue validSheetInput
			\(?Just settings) -> preview settings >>*
			[ OnAction (Action "Back") $ always $ edit all_features input
			]
		, OnAction ActionContinue $ ifValue validSheetInput $
			return o fromOk o toRealSettings o fromJust
		]
	where
		validSheetInput ?None = False
		validSheetInput (?Just input) = isOk (checkSettingsInput all_features input)

	errorsToHtml all_features input = case mbError all_features input of
		?None -> Html ""
		?Just e -> DivTag []
			[ H3Tag [] [Text "Errors"]
			, PTag [StyleAttr "color:red;"] [Text e]
			]

	mbError _ ?None = ?Just "Please fill in all fields."
	mbError all_features (?Just input) = case checkSettingsInput all_features input of
		Ok _ -> ?None
		Error e -> ?Just e

loadExample :: !(SimpleSDSLens SheetSettingsInput) -> Task ()
loadExample input =
	(AddCSSClass "small" @>> Title "Choose example" @>> Hint example_warning @>>
		enterChoiceAs [ChooseFromGrid id] examples (\e -> e.example)) >>*
	[ OnAction ActionCancel   $ always $ return ()
	, OnAction ActionContinue $ hasValue \example -> set example input @! ()
	]
where
	example_warning = join " "
		[ "Loading an example will clear your current settings."
		, "Click Cancel to go back."
		]

preview :: !SheetSettingsInput -> Task ()
preview input =
	let settings = fromOk (toRealSettings input) in
	enterInformation [EnterUsing id (mapEditorWrite ValidEditor loader)] ||-
	// NB: ugly hack: waitForTimer is needed so that the UILoader is shown
	(waitForTimer False 1 >-| getDataSet (requiredFeatures settings)) >>- \data ->
	case getValues settings data of
		Error e ->
			throw ("Failed to import data: "+++e)
		Ok values ->
			updateInformation
				[UpdateUsing id (\_ x -> x) sheetEditor]
				(toSheet settings values) @!
			()
where
	toSheet settings values =
		{ html_id                   = ?None
		, settings                  = settings
		, values                    = values
		, shown_rows                = ?None
		, distinct_values           = DistinctColumnValues [!!]
		, distinct_values_selection = DistinctColumnValues [!!]
		, unsaved_changes           = ?None
		, current_cell              = ?None
		, current_selection         = ?None
		}

checkSettingsInput :: ![(String, FeatureType)] !SheetSettingsInput -> MaybeError String SheetSettings
checkSettingsInput all_features input =
	toRealSettings input >>= \settings ->
	case checkSettings all_features settings of
		?Just e -> Error e
		?None   -> Ok settings

toRealSettings :: !SheetSettingsInput -> MaybeError String SheetSettings
toRealSettings {SheetSettingsInput | nodes=input_nodes,columns=input_columns,text_node} =
	mapM toRealColumnSettings input_columns >>= \columns ->
	pure
	{ SheetSettings
	| nodes      = nodes
	, columns    = columns
	, text_panel =
		{ text_node     = ?Just text_node
		, colored_nodes = colored_nodes
		}
	}
where
	nodes = map toRealNodeSettings input_nodes
	colored_nodes = [(name,c) \\ {name,color= ?Just c} <- input_nodes]

	toRealNodeSettings :: !NodeSettingsInput -> NodeSettings
	toRealNodeSettings {NodeSettingsInput | name,type,optional,filterDescription} =
		{ NodeSettings
		| name              = name
		, type              = type
		, optional          = if optional (?Just True) ?None
		, filterDescription = filterDescription
		}

	toRealColumnSettings :: !ColumnSettings -> MaybeError String ColumnSettings
	toRealColumnSettings settings=:{css_class,expression}
		| css_class == ?Just "date"
			| isJust expression
				= Error "Date columns cannot have an expression."
				= Ok {settings & expression= ?Just LastModificationDate}
		| css_class == ?Just "tag"
			| isJust expression
				= Error "Tag columns cannot have an expression."
				= Ok {settings & expression= ?None}
		| otherwise
			= Ok settings

gEditor{|NodeSettingsInput|} ViewValue = abort "gEditor{|NodeSettingsInput|}: ViewValue\n"
gEditor{|NodeSettingsInput|} EditValue =
	mapEditorRead read $
	mapEditorWrite write $
	container5
		(Label "Name" @>>     nodeNameEditor)
		(Label "Type" @>>     nodeTypeEditor)
		(Label "Optional" @>> checkBox)
		(Label "Filter" @>>   nodeFilterDescriptionEditor)
		(Label "Colour" @>>   withDynamicHintAttributes "colour" colorPicker)
where
	read {name,type,optional,filterDescription,color} =
		( name
		, type
		, optional
		, filterDescription
		, color
		)
	write (mbName,mbType,optional,mbFilterDescription,mbColor) =
		fmap
			(\(n,t,fd,c) -> {name=n, type=t, optional=optional, filterDescription=fd, color=c})
			(editorReportTuple4 (mbName, mbType, mbFilterDescription, mbColor))

gEditor{|SheetSettingsInput|} ViewValue = abort "gEditor{|SheetSettingsInput|}: ViewValue\n"
gEditor{|SheetSettingsInput|} EditValue =
	mapEditorInitialValue (?Just o fromMaybe default) $
	mapEditorRead read $
	mapEditorWrite write $
	container4
		htmlView
		(container2 htmlView (styleAttr "margin:0 2em;" @>> gEditor{|*|} EditValue))
		(container2 htmlView (styleAttr "margin:0 2em;" @>> gEditor{|*|} EditValue))
		(container2 htmlView (styleAttr "margin:0 2em;" @>> nodeNameEditor))
where
	default =
		{ SheetSettingsInput
		| nodes     = []
		, columns   = []
		, text_node = ""
		}
	read input=:{SheetSettingsInput | nodes,columns,text_node} =
		( H1Tag [StyleAttr "margin-bottom:0;"] [Text "New sheet"]
		, (nodes_hint,     nodes)
		, (columns_hint,   columns)
		, (text_node_hint, text_node)
		)
	write (_,(_,mbNodes),(_,mbColumns),(_,mbTextNode)) =
		fmap
			(\(ns,cs,tn) -> {SheetSettingsInput | nodes=ns, columns=cs, text_node=tn})
			(editorReportTuple3 (withDefault [] mbNodes, withDefault [] mbColumns, withDefault "" mbTextNode))
	where
		withDefault default EmptyEditor = ValidEditor default
		withDefault _ nonEmptyEditor = nonEmptyEditor

nodes_hint = DivTag []
	[ H3Tag [] [Text "Nodes"]
	, PTag [] $ map (Html o flip (+++) " ")
		[ "Nodes are points in the Text-Fabric graph."
		, "They refer for example to words, phrases, clauses, or verses."
		, "See <a href=\"https://etcbc.github.io/bhsa/features/otype/\">the documentation</a> for the various types of nodes."
		, "Nodes themselves are references to objects in the graph."
		, "They are not visible in any way unless they have a <em>column</em> linked to them."
		, "The node <em>name</em> can be chosen freely and is a logical, human-readable name to refer to the node."
		]
	, PTag []
		[ Html "When a node is <em>optional</em>, the query will still return results even if no match can be found. "
		, Html "Optionality propagates, i.e., nodes that depend on optional nodes must be optional themselves as well."
		]
	, PTag []
		[ Html "When you set the <em>color</em> of a node, the text of that node will be highlighted in the Hebrew text pane."
		]
	, PTag []
		[ Html "The node <em>filter</em> is like a search query: it determines which nodes are matched. "
		, Html "Node filters are described by the following grammar:"
		]
	, DetailsTag []
		[ SummaryTag [] [Text "(Click to expand node filter grammar)"]
		, grammarToHTML nodeFilter
		]
	]
columns_hint = DivTag []
	[ H3Tag [] [Text "Columns"]
	, PTag [] $ map (Html o flip (+++) " ")
		[ "Columns are in the end visible in your sheet."
		, "The title is used as the column header."
		, "Use the <em>Hebrew</em> style for columns which contain Hebrew text, so that they are formatted nicely."
		, "The <em>Date</em> style gives a column that is automatically updated with the current date when a row is modified."
		, "The <em>Tag</em> style gives a column that can be used to tag rows with a category from 1 to 9, which are up to the interpretation of the user but can be recognized with a colored dot."
		, "Leave the expression empty when using a date or tag column."
		]
	, PTag []
		[ Html "The column <em>expression</em> can be empty, in which case the column is editable. "
		, Html "Otherwise, the expression should follow this grammar:"
		]
	, DetailsTag []
		[ SummaryTag [] [Text "(Click to expand column expression grammar)"]
		, grammarToHTML columnExpression
		]
	]
text_node_hint = DivTag []
	[ H3Tag [] [Text "Text node"]
	, PTag []
		[ Html "The text node should be the name of one of your nodes, and should have type <em>verse</em>. "
		, Html "This node will be used to display Hebrew and English text in the right-side panel."
		]
	]
