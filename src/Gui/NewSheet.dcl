definition module Gui.NewSheet

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.GenEq import generic gEq
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import :: Task, class iTask

from MBQS.Model import :: SheetSettings, :: ColumnSettings, :: NodeFilterDescription

:: SheetSettingsInput =
	{ nodes        :: ![NodeSettingsInput]
	, columns      :: ![ColumnSettings]
	, text_node    :: !String
	}

:: NodeSettingsInput =
	{ name              :: !String
	, type              :: !String
	, optional          :: !Bool
	, filterDescription :: !NodeFilterDescription
	, color             :: !?String
	}

derive class iTask \ gEditor SheetSettingsInput
derive gEditor SheetSettingsInput

createNewSheet :: Task SheetSettings
