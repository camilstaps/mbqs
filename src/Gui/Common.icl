implementation module Gui.Common

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Func
import Data.Functor
import Data.List => qualified group

import iTasks
import iTasks.UI.Editor.Common

import Bible
import Bible.Parse

import MBQS.Languages.Parsers
import MBQS.Model

import Util

withExtraCheck :: !String !(w -> ?String) !(Editor r (EditorReport w)) -> Editor r (EditorReport w)
withExtraCheck name check editor = mapValidEditorWriteError
	(\val -> case check val of
		?None -> Ok val
		?Just e -> Error e)
	editor

nameEditor :: !String !Int -> Editor NodeName (EditorReport NodeName)
nameEditor type max_length = withDynamicHintAttributes type $
	withExtraCheck type
		(\s
			| isValidNodeName s -> ?None
			| size s == 0       -> ?Just $ "Please enter a "+++type
			| isAlpha s.[0]     -> ?Just $ type`+++"s may contain only letters, digits, and underscores"
			| otherwise         -> ?Just $ type`+++"s must begin with a letter")
		(textField <<@ maxlengthAttr max_length)
where
	type` = type:=(0, toUpper type.[0])

nodeNameEditor :: Editor NodeName (EditorReport NodeName)
nodeNameEditor = nameEditor "node name" 20

nodeTypeEditor :: Editor String (EditorReport String)
nodeTypeEditor =
	mapEditorInitialValue (?Just o fromMaybe "") $
	mapEditorRead read $
	mapEditorWrite write $
	dropdownWithGroups
where
	read type =
		( [({ChoiceText | id=i, text=toHumanReadable t}, ?Just g) \\ (g,ts) <- types, (t,i) <- ts]
		, [i \\ (_,ts) <- types, (t,i) <- ts | t == type]
		)
	write [t] = ValidEditor (fst (concatMap snd types !! t))
	write [] = InvalidEditor ["Please enter a node type"]

	types = fst $ flip (mapSt (\(g,ts) i -> let (ts`,i`) = mapSt (\t i -> ((t,i),i+1)) ts i in ((g,ts`),i`))) 0 $
		[ ("Basic",    ["word","phrase","clause","sentence","verse","chapter","book"])
		, ("Advanced", ["lex","subphrase","phrase_atom","clause_atom","sentence_atom","half_verse"])
		]

nodeFilterDescriptionEditor :: Editor NodeFilterDescription (EditorReport NodeFilterDescription)
nodeFilterDescriptionEditor =
	withDynamicHintAttributes "node filter" $
	mapEditorRead toString $
	mapValidEditorWriteError parseNodeFilterDescription $
	mapEditorInitialValue (?Just o fromMaybe "") $
	textField

columnTitleEditor :: Editor String (EditorReport String)
columnTitleEditor = nameEditor "column title" 30

columnStyleEditor :: Editor (?String) (EditorReport (?String))
columnStyleEditor =
	mapEditorInitialValue (?Just o fromMaybe ?None) $
	mapEditorRead read $
	mapEditorWrite (fmap write) $
	chooseWithDropdown (map toHumanReadable styles)
where
	styles = ["plain", "hebrew", "date", "tag"]

	read ?None = 0
	read (?Just style) = fromMaybe 0 (elemIndex style styles)

	write 0 = ?None
	write i = ?Just (styles !! i)

columnExpressionEditor :: Editor (?ColumnExpression) (EditorReport (?ColumnExpression))
columnExpressionEditor =
	withDynamicHintAttributes "column expression" $
	mapEditorInitialValue (?Just o fromMaybe ?None) $
	mapEditorRead (maybe "" toString) $
	mapValidEditorWriteError write $
	textField
where
	write "" = Ok ?None
	write s = ?Just <$> parseColumnExpression s

bibleReferenceTextField :: Editor Reference (EditorReport Reference)
bibleReferenceTextField =
	withDynamicHintAttributes "bible reference" $
	mapEditorRead toString $
	mapValidEditorWriteError parseReference
	textField
