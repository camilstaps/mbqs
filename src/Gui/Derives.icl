implementation module Gui.Derives

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import Control.Monad => qualified return, forever, sequence
import Data.Func
import Data.Functor
import Data.GenHash

import iTasks

import Regex.Print

import TextFabric
import TextFabric.Import
import TextFabric.NodeMapping

import Bible

import MBQS.Model

import Gui.Common

gEq{|DataSet`|} _ x y = equal_ptr x y
gText{|DataSet`|} _ _ _ = abort "gText DataSet`\n"
gEditor{|DataSet`|} _ _ _ _ = abort "gEditor DataSet`\n"
JSONEncode{|DataSet`|} _ _ _ = abort "JSONEncode DataSet`\n"
JSONDecode{|DataSet`|} _ _ _ = abort "JSONDecode DataSet`\n"

gEq{|EdgeSet|} _ _ = abort "gEq EdgeSet\n"
gText{|EdgeSet|} _ _ = abort "gText EdgeSet\n"
gEditor{|EdgeSet|} _ = abort "gEditor EdgeSet\n"
JSONEncode{|EdgeSet|} _ _ = abort "JSONEncode EdgeSet\n"
JSONDecode{|EdgeSet|} _ _ = abort "JSONDecode EdgeSet\n"

derive JSONEncode FeatureType, Target

derive class iTask Reference, Book
derive gHash Reference, Book

derive class iTask \ JSONEncode, JSONDecode, gEq SheetSettings,
	TextPanelSettings, NodeSettings
derive gEq NodeSettings

// These are used internally in iTasks to determine whether values have to be
// written, shares have to be notified, etc. Because of this we cannot use
// abort. It is probably safer to give false negatives than to give false
// positives, so we here only check whether the pointers equal. We cannot use
// equal_ptr x y, because this will build a new record.
gEq{|SheetValues|} x y =
	equal_ptr x.node_names y.node_names &&
	equal_ptr x.entries y.entries
gEq{|SheetSettings|} x y =
	equal_ptr x.SheetSettings.nodes y.SheetSettings.nodes &&
	equal_ptr x.SheetSettings.columns y.SheetSettings.columns &&
	equal_ptr x.text_panel y.text_panel

equal_ptr :: !.a !.a -> Bool
equal_ptr _ _ = code {
	push_a_b 0
	push_a_b 1
	pop_a 2
	eqI
}

gText{|SheetValues|} _ _ = []

derive class iTask \ JSONEncode, JSONDecode, gEditor NodeFilterDescription,
	FeatureOp

gEq{|Regex|} x y = toString x == toString y
gText{|Regex|} _ mbRgx = maybeToList (toString <$> mbRgx)

gEditor{|NodeFilterDescription|} ViewValue = abort "gEditor{|NodeFilterDescription|}: ViewValue\n"
gEditor{|NodeFilterDescription|} EditValue = nodeFilterDescriptionEditor

derive class iTask \ JSONEncode, JSONDecode, gEditor ColumnSettings

gEditor{|ColumnSettings|} ViewValue = abort "gEditor{|ColumnSettings|}: ViewValue\n"
gEditor{|ColumnSettings|} EditValue =
	mapEditorRead read $
	mapEditorWrite write $
	container3
		(Label "Title" @>>             columnTitleEditor)
		(Label "Style" @>>             columnStyleEditor)
		(Label "Column expression" @>> columnExpressionEditor)
where
	read {title,css_class,expression} =
		( title
		, css_class
		, expression
		)
	write (mbTitle,mbCssClass,mbExpression) =
		fmap
			(\(t,c,e) -> {title=t, css_class=c, expression=e})
			(editorReportTuple3 (mbTitle, mbCssClass, mbExpression))

derive class iTask \ JSONEncode, JSONDecode ColumnExpression
