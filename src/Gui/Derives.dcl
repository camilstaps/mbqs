definition module Gui.Derives

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.GenEq import generic gEq
from Data.GenHash import generic gHash
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import :: Task, class iTask

from TextFabric import :: DataSet`, :: EdgeSet
from TextFabric.Import import :: FeatureType
from TextFabric.NodeMapping import :: Target

from Bible import :: Book, :: Reference

from MBQS.Model import
	:: SheetSettings,
	:: NodeFilterDescription,
	:: ColumnSettings, :: ColumnExpression,
	:: SheetValues

derive gEq DataSet`, EdgeSet
derive gText DataSet`, EdgeSet
derive gEditor DataSet`, EdgeSet
derive JSONEncode DataSet`, EdgeSet
derive JSONDecode DataSet`, EdgeSet

derive JSONEncode FeatureType, Target

derive class iTask Reference
derive gHash Reference, Book

derive class iTask \ JSONEncode, JSONDecode, gEq SheetSettings
derive gEq SheetSettings

derive gEq SheetValues
derive gText SheetValues

derive class iTask \ JSONEncode, JSONDecode, gEditor NodeFilterDescription
derive gEditor NodeFilterDescription

derive class iTask \ JSONEncode, JSONDecode, gEditor ColumnSettings
derive gEditor ColumnSettings

derive class iTask \ JSONEncode, JSONDecode ColumnExpression
