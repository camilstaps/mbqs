implementation module Gui.Sheet

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdOverloadedList

import graph_copy

import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

import Control.Applicative
import Control.Monad => qualified return
import Data.Func
import Data.Functor
import Data.List
from Data.Map import :: Map
import qualified Data.Map
import System._Unsafe
from Text import concat4
import Text.HTML
import Text.Language

import iTasks => qualified forever, group, sequence
import iTasks.UI.JavaScript

import TextFabric

import MBQS.Model
import MBQS.Model.Serialization

import Gui.Derives
import Gui.DistinctColumnValues
import Gui.Shares

tableIdAttr :: !String -> UIAttribute
tableIdAttr id = ("tableId", JSONString id)

openSheet :: !FilePath -> Task (SheetSettings, ?SheetValues)
openSheet path = accWorldError
	(\w
		# (ok,f,w) = fopen path FReadData w
		| not ok -> (Error "file does not exist or we are not allowed to read it", w)
		# (mbSettingsAndValues,f) = readSheet f
		# (_,w) = fclose f w
		-> (mbSettingsAndValues, w))
	((+++) "Failed to open sheet: ")

saveSheet :: !FilePath !SheetSettings !SheetValues -> Task ()
saveSheet path settings values = accWorldError
	(\w
		# (ok,f,w) = fopen path FWriteData w
		| not ok -> (Error "failed to open file for writing", w)
		# f = writeSheet settings values f
		# (f,w) = fclose f w
		-> (Ok (), w))
	((+++) "Failed to save sheet: ")

unsavedChangesView :: Editor Sheet ()
unsavedChangesView =
	mapEditorWrite (const ()) $
	mapEditorRead (\s -> toString s.Sheet.unsaved_changes) $
	textView
where
	toString ?None = "No changes."
	toString (?Just n) = pluralisen English n "edit" +++ " since last (auto)save."

nrSearchResultsView :: Editor Sheet ()
nrSearchResultsView =
	mapEditorWrite (const ()) $
	mapEditorRead (\s -> toString s.Sheet.shown_rows) $
	textView
where
	toString ?None = ""
	toString (?Just rs) = pluralisen English (size rs) "search result" +++ "."

selectionView :: Editor Sheet ()
selectionView =
	mapEditorWrite (const ()) $
	mapEditorRead (\s -> toString s.Sheet.current_selection) $
	textView
where
	toString ?None = ""
	toString (?Just ((r1,c1), (r2,c2)))
		| nrows == 1 && ncols == 1 = ""
		| otherwise = concat4
			(pluralisen English nrows "row")
			" and "
			(pluralisen English ncols "column")
			" selected."
	where
		// r2 < 0 happens when selecting a column in a sheet with no entries
		nrows = if (r2 < 0) 0 (abs (r1-r2) + 1)
		ncols = abs (c1-c2) + 1

:: EditorState =
	{ html_id                   :: !?String
	, settings                  :: !SheetSettings
	, values                    :: !.SheetValues
	, shown_rows                :: !?{#Int}
	, distinct_values           :: !DistinctColumnValues
	, distinct_values_selection :: !DistinctColumnValues
	, unsaved_changes           :: !?Int
	, current_cell              :: !?(Int, Int)
	, current_selection         :: !?((Int, Int), (Int, Int))
	}

:: ClientToServer
	= Change !Int !Int !String
	| SelectCell !Int !Int
	| SelectRange !(? ((!Int, !Int), !(!Int, !Int)))

derive JSONEncode ClientToServer, EditorState
derive JSONDecode ClientToServer, EditorState

gEq{|{#Int}|} xs ys = size xs == size ys && and [x == y \\ x <-: xs & y <-: ys]
gText{|{#Int}|} _ _ = abort "gText{|{#Int}|}\n"

JSONEncode{|{#Int}|} _ xs = [JSONArray [JSONInt x \\ x <-: xs]]
JSONDecode{|{#Int}|} _ json=:[JSONArray ints:rest]
	| all (\i -> i=:(JSONInt _)) ints
		= (?Just {i \\ JSONInt i <- ints}, rest)
		= (?None, json)
JSONDecode{|{#Int}|} _ json = (?None, json)

derive class iTask \ gEditor Sheet
gEditor{|Sheet|} _ = sheetEditor

sheetEditor :: Editor Sheet (EditorReport Sheet)
sheetEditor = panel1 $ JavaScriptInit initUI @>> leafEditorToEditor_
	(\st -> ?Just (dynamic st))
	(\dyn -> case dyn of ?Just (st :: EditorState) -> ?Just (unsafeCoerce st); _ -> ?None)
	{ LeafEditor
	| onReset    = onReset
	, onEdit     = onEdit
	, onRefresh  = onRefresh
	, writeValue = writeValue
	}
where
	onReset attributes val vst=:{abcInterpreterEnv}
		# contents=:{Sheet | settings,values,shown_rows} = fromJust val
		# client_contents = jsSerializeGraph (hyperstrict (settings,values)) abcInterpreterEnv
			// NB: Text-Fabric uses {32#} internally, which cannot be
			// serialized for the moment; therefore, we first do `hyperstrict`
		# attributes = 'Data.Map'.unions
			[ attributes
			, maybe 'Data.Map'.newMap (\id -> classAttr ["mbqs-sheet-" +++ id]) contents.Sheet.html_id
			, 'Data.Map'.fromList
				[ ("mbqs_contents", JSONString client_contents)
				, ("shown_rows", encodeShownRows shown_rows)
				]
			]
		=
			( Ok
				( uia UIHtmlView attributes
				, unsafeCoerce
					{ EditorState
					| html_id                   = contents.Sheet.html_id
					, settings                  = settings
					, values                    = values
					, shown_rows                = shown_rows
					, distinct_values           = computeAllDistinctValues values.entries
					, distinct_values_selection = contents.Sheet.distinct_values_selection
					, unsaved_changes           = contents.Sheet.unsaved_changes
					, current_cell              = contents.Sheet.current_cell
					, current_selection         = contents.Sheet.current_selection
					}
				, ?Just (ValidEditor contents)
				)
			, vst
			)
	
	onEdit (Change row col val) st vst
		#! st = unsafeCoerce st
		#! (old,st) = st!EditorState.values.entries.[row].SheetEntry.columns.[col]
		#! st
			& EditorState.distinct_values = updateDistinctValues col old val st.EditorState.distinct_values
			, EditorState.values.entries.[row].SheetEntry.columns.[col] = val
			, EditorState.unsaved_changes = ?Just (maybe 1 inc st.EditorState.unsaved_changes)
		#! (v,st) = stateToValue st
		= (Ok (NoChange, st, ?Just (ValidEditor v)), vst)
	onEdit (SelectCell row col) st vst
		#! st & EditorState.current_cell = ?Just (row, col)
		#! (v,st) = stateToValue st
		= (Ok (NoChange, st, ?Just (ValidEditor v)), vst)
	onEdit (SelectRange mbRange) st=:{EditorState|shown_rows} vst
		#! st
			& EditorState.current_selection = mbRange
			, EditorState.distinct_values_selection = case mbRange of
				?Just ((from_row,from_col),(to_row,to_col)) | from_col == to_col ->
					computeAllDistinctValues
						{ st.EditorState.values.entries.[i]
						\\ i <- selected_rows
						| 0 <= i && i < size st.EditorState.values.entries
						}
					with
						fr = min from_row to_row
						to = max from_row to_row
						selected_rows = case shown_rows of
							?Just selection -> [selection.[r] \\ r <- [fr..to]]
							?None -> [fr..to]
				_ ->
					DistinctColumnValues [!!]
		#! (v,st) = stateToValue st
		= (Ok (NoChange, st, ?Just (ValidEditor v)), vst)

	onRefresh ?None st vst =
		( Error "sheetEditor: onRefresh is expected to have a value"
		, vst
		)
	onRefresh (?Just newVal=:{Sheet | shown_rows}) st vst =
		( Ok
			( change
			,
				{ EditorState | st
				& shown_rows      = shown_rows
				, unsaved_changes = newVal.Sheet.unsaved_changes
				}
			, ?None
			)
		, vst
		)
	where
		change
			| st.EditorState.shown_rows === shown_rows
				= NoChange
				= ChangeUI [SetAttribute "shown_rows" (encodeShownRows shown_rows)] []

	writeValue st
		# (v,st) = stateToValue st
		= Ok (ValidEditor v)

	stateToValue st
		# (values,st) = st!EditorState.values
		# val =
			{ Sheet
			| html_id                   = st.EditorState.html_id
			, settings                  = st.EditorState.settings
			, values                    = values
			, shown_rows                = st.EditorState.shown_rows
			, distinct_values           = st.EditorState.distinct_values
			, distinct_values_selection = st.EditorState.distinct_values_selection
			, unsaved_changes           = st.EditorState.unsaved_changes
			, current_cell              = st.EditorState.current_cell
			, current_selection         = st.EditorState.current_selection
			}
		= (val, unsafeCoerce st)

	encodeShownRows ?None = JSONNull
	encodeShownRows (?Just rows) = JSONArray [JSONInt r \\ r <-: rows]

	initUI _ me w
		# (init,w) = jsWrapFun (\_ w -> snd (runJS () me (initFrontend me) w)) me w
		= (me .# "onShow" .= init) w

initFrontend :: !JSVal -> JS () JSVal
initFrontend me =
	appJS (domEl .# "style.padding" .= "0px") >>|
	appJS (domEl .# "style.flexGrow" .= "1") >>|
	appJS (domEl .# "style.width" .= "100%") >>|
	accJS (jsDocument .# "createElement" .$ "div") >>= \contents_div ->
	appJS (contents_div .# "class" .= "table-contents") >>|
	/* a unique identifier is needed for the persistentState plugin */
	appJS (contents_div .# "id" .= jsCall (jsGlobal "btoa") (me .# "attributes.tableId")) >>|
	appJS (domEl .# "appendChild" .$! contents_div) >>|

	accJS (jsDeserializeJSVal (me .# "attributes.mbqs_contents")) >>= \(settings, data) ->
	let
		columns = settings.SheetSettings.columns
		titles = [c.ColumnSettings.title \\ c <- columns]
		column_classes = [fromMaybe "" c.css_class \\ c <- columns]
		readonly_columns = [i \\ i <- [0..] & c <- columns | isJust c.expression]
		date_column = case [i \\ i <- [0..] & c <- columns | c.expression=:(?Just LastModificationDate)] of
			[c:_] -> ?Just c
			_     -> ?None
	in

	jsWrapMonad onAttributeChange >>= \onAttributeChange ->
	appJS (me .# "onAttributeChange" .= onAttributeChange) >>|

	jsWrapMonad (afterChange date_column) >>= \afterChange ->
	jsWrapMonad afterSelection >>= \afterSelection ->
	jsWrapMonad (onContextMenuSearchWith titles) >>= \onContextMenuSearchWith ->

	accJS (jsGlobal "MBQSTable.instantiate" .$ (contents_div, jsRecord
		[ "licenseKey" :> "non-commercial-and-evaluation"
		, "width" :> "100%"
		, "height" :> "100%"
		, "enterBeginsEditing" :> False
		, "colHeaders" :> titles
		, "columns" :>
			[ jsRecord case c.expression of
				?Just LastModificationDate ->
					[ "renderer" :> jsGlobal "relativeTimeRenderer"
					]
				?Just _ ->
					[]
				?None | c.css_class == ?Just "tag" ->
					[ "renderer" :> jsGlobal "tagRenderer"
					]
				?None ->
					[ "type" :> "autocomplete"
					, "strict" :> True
					, "sortByRelevance" :> False
					]
			\\ c <- columns & i <- [0..]
			]
		, "column_classes" :> column_classes
		, "readonly_columns" :> readonly_columns
		, "currentRowClassName" :> "current-row"
		, "readOnlyCellClassName" :> "" /* to disable greying values out */
		, "stretchH" :> "last"
		, "manualColumnResize" :> True
		, "persistentState" :> True /* saves column sorting, positions, and sizes in LocalStorage (only the last is relevant here) */
		, "contextMenu" :> jsRecord
			[ "items" :> jsRecord
				[ "cut" :> jsRecord []
				, "copy" :> jsRecord []
				// TODO: add own paste; this is not provided
				, "---------" :> jsRecord []
				, "search" :> jsRecord
					[ "name" :> "Search with this..."
					, "submenu" :> jsRecord
						[ "items" :>
							[ jsRecord
								[ "key" :> "search:"+++toString i
								, "name" :> title
								, "callback" :> onContextMenuSearchWith
								]
							\\ title <- titles & i <- [0..]
							]
						]
					]
				]
			]
		, "data" :> [[v \\ v <-: e.SheetEntry.columns] \\ e <-: data.entries]
		, "afterChange" :> afterChange
		, "afterSelection" :> afterSelection
		])) >>= \table ->
	appJS (me .# "table" .= table) >>|

	// If the table is added before the layout is finalized, the dimensions
	// will be off; manually reset them here:
	refreshDimensions () >>|

	// When the table is in a sidebar, the width/height of the grandfather may change.
	// We watch for this using a MutationObserver, and refresh the handsontable dimensions when needed.
	appJS (me .# "resize_timeout_id" .= jsNull) >>|
	jsWrapMonad refreshDimensions >>= \refreshDimensions ->
	jsWrapMonad (onAttrMutation refreshDimensions) >>= \onAttrMutation ->
	accJS (jsNew "MutationObserver" onAttrMutation) >>= \mutation_observer ->
	let config = jsRecord ["attributeFilter" :> ["style"], "attributes" :> True] in
	appJS (mutation_observer .# "observe" .$! (sidebar_parent, config)) >>|
	appJS (jsWindow .# "addEventListener" .$! ("resize", onAttrMutation))
where
	domEl = me .# "domEl"
	dimensions_parent = domEl .# "parentNode"
	sidebar_parent = dimensions_parent .# "parentNode.parentNode"

	onAttributeChange {[0]=name,[1]=val}
		# name = fromJS "" name
		| name == "shown_rows" =
			appJS (me .# "table.showOnlyRows" .$! val)
		| otherwise =
			appJS (jsTrace ("unknown attribute change: "+++name))

	onAttrMutation f _ =
		appJS (jsWindow .# "clearTimeout" .$! (me .# "resize_timeout_id")) >>|
		accJS (jsWindow .# "setTimeout" .$ (f, 100)) >>= \id ->
		appJS (me .# "resize_timeout_id" .= id)
	refreshDimensions _ =
		appJS (domEl .# "style.display" .= "none") >>|
		accJS ((.?) (dimensions_parent .# "clientHeight")) >>= \h ->
		accJS ((.?) (dimensions_parent .# "clientWidth")) >>= \w ->
		appJS (domEl .# "style.display" .= "block") >>|
		appJS (me .# "table.updateSettings" .$! jsRecord
			[ "height" :> maybe "100%" (\h -> toString h+++"px") (jsValToInt h)
			, "width"  :> maybe "100%" (\w -> toString w+++"px") (jsValToInt w)
			])

	afterChange date_column {[0]=change} | jsIsNull change = // fired when data is loaded
		pure jsNull
	afterChange date_column {[0]=change} =
		accJS (jsValToList change parseChange) >>= \changes ->
		case changes of
			?None ->
				appJS (jsTraceVal change o jsTrace "failed to parse changes:")
			?Just cs ->
				accJS (jsNew "Date" ()) >>= \date ->
				accJS (date .# "getTime" .$ ()) >>= \time` ->
				let time = toString (fromJS 0 time` / 1000) in
				mapM_ (writeChange time) cs $> jsNull
	where
		parseChange change = case (jsValToInt (change .# 0), jsValToInt (change .# 1), jsValToString (change .# 3)) of
			(?Just r, ?Just c, ?Just v)
				-> ?Just (r,c,v)
			(?Just r, ?Just c, ?None) | jsIsNull (change .# 3) /* empty value is null */
				-> ?Just (r,c,"")
				-> ?None
		writeChange time (visrow,col,val) =
			gets (\st -> st.component) >>= \me ->
			accJS (me .# "table.rowIndexMapper.getPhysicalFromVisualIndex" .$ visrow) >>= \row ->
			case jsValToInt row of
				?None ->
					pure jsNull
				?Just row -> case date_column of
					?Just c | c <> col ->
						appJS (me .# "table.setSourceDataAtCell" .$! (row,c,time)) >>|
						doEditEvent me (Change row col val)
					_ ->
						doEditEvent me (Change row col val)

	afterSelection args=:{[0]=visual_row,[1]=col,[2]=visual_row_2,[3]=col_2} =
		gets (\st -> st.component) >>= \me ->
		accJS (me .# "table.rowIndexMapper.getPhysicalFromVisualIndex" .$ visual_row) >>= \data_row ->
		case (jsValToInt data_row, jsValToInt col) of
			(?Just data_row,?Just col) ->
				doEditEvent me (SelectCell data_row col) >>|
				accJS (me .# "table.rowIndexMapper.getRenderableFromVisualIndex" .$ visual_row) >>= \visual_row ->
				accJS (me .# "table.rowIndexMapper.getRenderableFromVisualIndex" .$ visual_row_2) >>= \visual_row_2 ->
				case (jsValToInt visual_row, jsValToInt visual_row_2, jsValToInt col_2) of
					(?Just visual_row,?Just visual_row_2,?Just col_2) | visual_row_2 <> visual_row || col_2 <> col ->
						doEditEvent me (SelectRange (?Just ((visual_row,col), (visual_row_2,col_2))))
					_ ->
						doEditEvent me (SelectRange ?None)
			_ ->
				pure jsNull

	onContextMenuSearchWith columns {[0]=key,[1]=selection,[2]=ev}
		| keys % (0,6) == "search:" =
			let column = toInt (keys % (7,size keys-1)) in
			accJS ((.?) (selection .# 0 .# "start.row")) >>= \row ->
			case jsValToInt row of
				?None ->
					pure jsNull
				?Just row ->
					gets (\st -> st.component) >>= \me ->
					accJS (me .# "table.getData" .$ ()) >>= \data ->
					accJS ((.?) (data .# row .# column)) >>= \val ->
					case jsValToString val of
						?None ->
							pure jsNull
						?Just val ->
							simulateSearch (toString (FeatureOp (columns !! column) (Eq val)))
		| otherwise =
			appJS (jsTraceVal selection o jsTraceVal key)
	where
		keys = fromJS "" key

	// kind of a hack..
	simulateSearch query =
		accJS (jsDocument .# "querySelector" .$ ".mbqs-search input") >>= \input ->
		appJS (input .# "value" .= query) >>|
		accJS (jsNew "Event" "input") >>= \ev ->
		appJS (input .# "dispatchEvent" .$! ev) >>|
		accJS (jsNew "KeyboardEvent" ("keyup", jsRecord ["keyCode" :> 13])) >>= \ev ->
		appJS (input .# "dispatchEvent" .$! ev)

	doEditEvent me event = appJS
		(me .# "doEditEvent" .$!
			( me .# "attributes.taskId"
			, me .# "attributes.editorId"
			, toJSON event
			))

addColumn :: !Int !ColumnSettings !Sheet -> Task Sheet
addColumn place col sheet=:{Sheet|settings,values,unsaved_changes}
	# (before,after) = splitAt place settings.SheetSettings.columns
	# settings = {SheetSettings | settings & columns=before++[col:after]}
	  sheet & Sheet.unsaved_changes = ?Just (maybe 1 inc unsaved_changes)
	= getAllFeatures >>- \all_features -> case checkSettings all_features settings of
		?Just error ->
			throw error
		?None -> case col.ColumnSettings.expression of
			?Just expr | not (expr=:LastModificationDate)
				-> getDataSet (requiredFeatures settings) >>- \data
					# node_names = [n.NodeSettings.name \\ n <- settings.SheetSettings.nodes]
					# values & entries =
						{ {SheetEntry | e & columns=add_column (fromMaybe "" val) columns}
						\\ e=:{SheetEntry | nodes,columns} <-: values.entries
						, let val = getColumnValue node_names nodes expr data
						}
					-> return {Sheet | sheet & settings=settings, values=values}
			_
				# values & entries =
					{ {SheetEntry | e & columns=add_column "" e.SheetEntry.columns}
					\\ e <-: values.entries
					}
				-> return {Sheet | sheet & settings=settings, values=values}
where
	add_column :: !String !.{#String} -> .{#String}
	add_column new vals =
		{ if (i<place) vals.[i] (if (i==place) new vals.[i-1])
		\\ i <- [0..size vals]
		}

addDisambiguatingColumn :: !Int !ColumnSettings ![Int] !Sheet -> Task Sheet
addDisambiguatingColumn place col cols_to_disambiguate sheet=:{Sheet|settings,values,unsaved_changes}
	| isEmpty cols_to_disambiguate
		= throw "Can only disambiguate with at least one column"
	# (before,after) = splitAt place settings.SheetSettings.columns
	# settings = {SheetSettings | settings & columns=before++[col:after]}
	  sheet & Sheet.unsaved_changes = ?Just (maybe 1 inc unsaved_changes)
	# values & entries = {e \\ e <- concatMap update_entries (groupBy same_values [e \\ e <-: values.entries])}
	= return {Sheet | sheet & settings=settings, values=values}
where
	update_entries [e] = [add_column "" e]
	update_entries es = [add_column {c} e \\ e <- es & c <- ['a'..]]

	same_values e1 e2 = values e1 == values e2
	where
		values e = [e.SheetEntry.columns.[i] \\ i <- cols_to_disambiguate]

	add_column :: !String !SheetEntry -> SheetEntry
	add_column new entry=:{SheetEntry|columns} =
		{ SheetEntry
		| entry
		& columns =
			{ if (i<place) columns.[i] (if (i==place) new columns.[i-1])
			\\ i <- [0..size columns]
			}
		}

renameColumn :: !Int !String !Sheet -> Task Sheet
renameColumn place title sheet=:{Sheet|settings,unsaved_changes}
	| place >= length settings.SheetSettings.columns
		= throw "Index out of bounds"
	# (before,[cur:after]) = splitAt place settings.SheetSettings.columns
	  new = before ++ [{ColumnSettings | cur & title=title}:after]
	# settings = {SheetSettings | settings & columns=new}
	= return
		{ Sheet
		| sheet
		& settings        = settings
		, unsaved_changes = ?Just (maybe 1 inc unsaved_changes)
		}

deleteColumn :: !Int !Sheet -> Task Sheet
deleteColumn place sheet=:{Sheet|settings,values,unsaved_changes}
	| place >= length settings.SheetSettings.columns
		= throw "Index out of bounds"
	# (before,[_:after]) = splitAt place settings.SheetSettings.columns
	# settings = {SheetSettings | settings & columns=before++after}
	# values & entries =
		{ {SheetEntry | e & columns=delete_column e.SheetEntry.columns}
		\\ e <-: values.entries
		}
	= return
		{ Sheet
		| sheet
		& settings        = settings
		, values          = values
		, unsaved_changes = ?Just (maybe 1 inc unsaved_changes)
		}
where
	delete_column :: !.{#String} -> .{#String}
	delete_column vals =
		{ if (i<place) vals.[i] vals.[i+1]
		\\ i <- [0..size vals-2]
		}

// To insert newly added entries in the right spot in `addEntry`.
instance < SheetEntry
where
	(<) x y = [n \\ n <-: x.SheetEntry.nodes] < [n \\ n <-: y.SheetEntry.nodes]

addEntry :: ![NodeRef] !Sheet -> Task Sheet
addEntry nodes sheet=:{Sheet|settings,values,unsaved_changes}
	| length nodes <> length settings.SheetSettings.nodes =
		throw "Gui.Sheet.addEntry: incorrect amount of nodes"
	| otherwise =
		getDataSet (requiredFeatures settings) >>- \data
			# nodes = {n \\ n <- nodes}
			# entry =
				{ SheetEntry
				| nodes = nodes
				, columns = getColumnValues
					[n.NodeSettings.name \\ n <- settings.SheetSettings.nodes]
					nodes
					settings.SheetSettings.columns
					data
				}
			# values & entries = {e \\ e <- merge [e \\ e <-: values.entries] [entry]}
			-> return
				{ Sheet
				| sheet
				& values = values
				, unsaved_changes = ?Just (maybe 1 inc unsaved_changes)
				}

deleteEntry :: !Int !Sheet -> Task Sheet
deleteEntry i sheet=:{Sheet|values,unsaved_changes}
	| 0 >= i || i >= size values.entries =
		throw "Gui.Sheet.deleteEntry: illegal row index"
	# (before,[_:after]) = splitAt i [e \\ e <-: values.entries]
	  sheet & Sheet.values.entries = {e \\ e <- before ++ after}
	        , unsaved_changes = ?Just (maybe 1 inc unsaved_changes)
	= return sheet

trimByShownRows :: !Sheet -> Sheet
trimByShownRows sheet=:{Sheet|shown_rows= ?None} =
	sheet
trimByShownRows sheet=:{Sheet|values={entries},shown_rows= ?Just shown_rows,unsaved_changes} =
	{ Sheet
	| sheet
	& values.entries = if (size shown_rows == 0)
		{}
		{ e \\ e <-: entries
		& show <|- shown_rows_bools 0 0 shown_rows
		| show
		}
	, unsaved_changes = ?Just (maybe 1 inc unsaved_changes)
	}
where
	shown_rows_bools i j rows
		| i > rows.[size rows-1]
			= [#]
		| i == rows.[j]
			= [#True:shown_rows_bools (i+1) (j+1) rows]
			= [#False:shown_rows_bools (i+1) j rows]
