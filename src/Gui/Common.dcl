definition module Gui.Common

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError

from iTasks.UI.Editor import :: Editor, :: EditorReport

from Bible import :: Reference

from MBQS.Model import :: NodeName, :: NodeFilterDescription, :: ColumnExpression

nodeNameEditor :: Editor NodeName (EditorReport NodeName)

nodeTypeEditor :: Editor String (EditorReport String)

nodeFilterDescriptionEditor :: Editor NodeFilterDescription (EditorReport NodeFilterDescription)

columnTitleEditor :: Editor String (EditorReport String)

columnStyleEditor :: Editor (?String) (EditorReport (?String))

columnExpressionEditor :: Editor (?ColumnExpression) (EditorReport (?ColumnExpression))

bibleReferenceTextField :: Editor Reference (EditorReport Reference)
