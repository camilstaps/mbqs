definition module Gui.Main

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from StdMaybe import :: Maybe

from Data.GenEq import generic gEq
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.SDS.Definition import :: SDSLens
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import :: Task, class iTask

from MBQS.Model import :: ColumnSettings

:: ApplicationMenu =
	{ file_save                      :: !Bool
	, file_save_as                   :: !Bool
	, file_export_csv                :: !Bool
	, edit_add_column                :: !Bool
	, edit_add_disambiguating_column :: !Bool
	, edit_rename_column             :: !Bool
	, edit_delete_column             :: !Bool
	, edit_add_row                   :: !Bool
	, edit_delete_row                :: !Bool
	, edit_find                      :: !Bool
	, edit_cancel_search             :: !Bool
	, edit_trim_by_search            :: !Bool
	, edit_toggle_tag                :: !Bool
	}

derive class iTask ApplicationMenu

fullApplicationMenu :: ApplicationMenu

mainApp :: Task ()
