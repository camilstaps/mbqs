implementation module Gui.Settings

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import Data.Func
from Text import class Text(join), instance Text String
import Text.HTML

import iTasks

derive class iTask Settings

app_settings :: SimpleSDSLens (?Settings)
app_settings =: sdsFocus "MBQS.config.json" jsonFileShare

getSettings :: Task Settings
getSettings =
	get app_settings >>- \settings ->
	case settings of
		?None ->
			set (?Just default) app_settings @ fromJust
		?Just settings ->
			return settings
where
	default =
		{ tf_cache         = ?None
		, tf_path          = ?None
		, english_bible_db = ?None
		, english_bible_abbreviation = ?None
		, english_bible_copyright = ?None
		}

saveSettings :: !Settings -> Task ()
saveSettings settings = set (?Just settings) app_settings @! ()

editSettings :: Task Settings
editSettings =
	getSettings >>-
	updateInformation
		[ UpdateUsing
			(\s -> (s.tf_path, s.english_bible_db))
			(\s (tf_path, english_bible_db) ->
				{ s
				& tf_path          = tf_path
				, english_bible_db = english_bible_db
				})
			editor
		]
where
	editor =
		mapEditorWrite (\(_,x,_,y) -> ValidEditor (noEmptyString x, noEmptyString y)) $
		mapEditorRead (\(x,y) -> (hint_tf_path, fromMaybe "" x, hint_translation_path, fromMaybe "" y)) $
		panel4
			htmlView
			textField
			textView
			textField
	where
		noEmptyString (ValidEditor "") = ?None
		noEmptyString (ValidEditor s) = ?Just s
		noEmptyString _ = ?None

	hint_tf_path = Html $ join " "
		[ "Enter the path to the Text-Fabric data."
		, "This should be a path"
		, "(e.g. <tt>C:\\Users\\Name\\text-fabric-data\\github\\etcbc\\bhsa\\tf\\2021</tt> on Windows"
		, "or <tt>/home/name/text-fabric-data/github/etcbc/bhsa/tf/2021</tt> on Linux)"
		, "pointing to a download of <a href=\"https://github.com/etcbc/bhsa\" target=\"_blank\">github.com/etcbc/bhsa</a>."
		, "If unspecified, these examples are tried as a default location."
		]
	hint_translation_path = join " "
		[ "Enter the path to a translation file."
		, "This should be a file in e-Sword format containing a Bible translation,"
		, "which will be shown alongside the Hebrew data."
		]
