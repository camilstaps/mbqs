implementation module Gui.DistinctColumnValues

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdOverloadedList

import Control.Monad => qualified return, forever, sequence
import Data.Func
import Data.Functor
from Data.Map import :: Map
import qualified Data.Map
import Text.HTML

import iTasks
import iTasks.UI.JavaScript

import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

import MBQS.Model

computeAllDistinctValues :: !{#SheetEntry} -> DistinctColumnValues
computeAllDistinctValues entries | size entries == 0 = DistinctColumnValues [|]
computeAllDistinctValues entries = DistinctColumnValues
	[| computeDistinctValues i entries
	\\ i <- [0..]
	 & _ <-: entries.[0].SheetEntry.columns
	]

computeDistinctValues :: !Int !{#SheetEntry} -> Map String Int
computeDistinctValues col entries = find 'Data.Map'.newMap (size entries-1) entries
where
	find vs -1 _ = vs
	find vs i arr
		# v = arr.[i].SheetEntry.columns.[col]
		  vs = 'Data.Map'.alter (?Just o inc o fromMaybe 0) v vs
		= find vs (i-1) arr

updateDistinctValues :: !Int !String !String !DistinctColumnValues -> DistinctColumnValues
updateDistinctValues col old new (DistinctColumnValues maps) =
	DistinctColumnValues (UpdateAt col (upd (maps !!| col)) maps)
where
	upd map =
		'Data.Map'.alter (\n -> case n of
			?Just 1 -> ?None
			?Just n -> ?Just (n-1)
			?None   -> ?None /* should not happen */) old $
		'Data.Map'.alter (?Just o maybe 1 inc) new map

derive class iTask \ gEditor DistinctColumnValues, DistinctColumnValuesView

(!?|) :: ![!a!] !Int -> ?a
(!?|) xs i
	| i < 0
		= ?None
		= get i xs
where
	get _ [|] = ?None
	get 0 [|x:_] = ?Just x
	get i [|_:xs] = get (i-1) xs

gEditor{|DistinctColumnValuesView|} EditValue = abort "gEditor{|DistinctColumnValuesView|}: EditValue\n"
gEditor{|DistinctColumnValuesView|} ViewValue = JavaScriptInit initUI @>> leafEditorToEditor
	{ LeafEditor
	| onReset    = onReset
	, onEdit     = onEdit
	, onRefresh  = onRefresh
	, writeValue = writeValue
	}
where
	onReset attributes (?Just {values=DistinctColumnValues values,column}) vst
		// !?| because column may be out of range (when selecting the row header)
		# values = column >>= (!?|) values
		=
			( Ok
				( uia UIHtmlView $ 'Data.Map'.union attributes $ 'Data.Map'.fromList
					[ ("value", JSONString $ toString $ DivTag [] [])
					, ("values", encodeValues values)
					]
				, ()
				, ?None
				)
			, vst
			)
	
	onEdit () st vst = (Ok (NoChange, st, ?None), vst)

	onRefresh ?None st vst =
		( Error "gEditor{|DistinctColumnValues|}: onRefresh is expected to have a value"
		, vst
		)
	onRefresh (?Just {values=DistinctColumnValues values,column}) st vst
		// !?| because column may be out of range (when selecting the row header)
		# values = column >>= (!?|) values
		=
			( Ok
				( ChangeUI [SetAttribute "values" (encodeValues values)] []
				, ()
				, ?None
				)
			, vst
			)

	writeValue st = Ok EmptyEditor

	encodeValues ?None = JSONArray []
	encodeValues (?Just map) = JSONArray
		[ JSONArray [JSONString (if (size k == 0) "(empty)" k), JSONInt n]
		\\ (k,n) <- 'Data.Map'.toList map
		]

	initUI _ me w
		# (init,w) = jsWrapFun (\_ w -> snd (runJS () me (initFrontend me) w)) me w
		= (me .# "onShow" .= init) w

initFrontend :: !JSVal -> JS () JSVal
initFrontend me =
	appJS (domEl .# "style.padding" .= "0px") >>|
	appJS (domEl .# "style.overflow" .= "hidden") >>|
	appJS (domEl .# "style.height" .= "100%") >>|
	appJS (domEl .# "style.width" .= "100%") >>|

	jsWrapMonad onAttributeChange >>= \onAttributeChange ->
	appJS (me .# "onAttributeChange" .= onAttributeChange) >>|

	accJS (jsNew "Handsontable" (me .# "domEl.firstChild", jsRecord
		[ "licenseKey" :> "non-commercial-and-evaluation"
		, "width" :> "100%"
		, "height" :> "100%"
		, "renderAllRows" :> True
		, "enterBeginsEditing" :> False
		, "colHeaders" :> ["Value", "Frequency"]
		, "rowHeaders" :> False
		, "readOnly" :> True
		, "readOnlyCellClassName" :> "" /* to disable greying values out */
		, "columnSorting" :> True
		, "disableVisualSelection" :> True
		, "stretchH" :> "last"
		, "manualColumnResize" :> True
		, "data" :> me .# "attributes.values"
		])) >>= \table ->
	appJS (me .# "table" .= table) >>|

	// If the table is added before the layout is finalized, the dimensions
	// will be off; manually reset them here:
	refreshDimensions () >>|

	// When the table is in a sidebar, the width/height of the grandfather may change.
	// We watch for this using a MutationObserver, and refresh the handsontable dimensions when needed.
	appJS (me .# "resize_timeout_id" .= jsNull) >>|
	jsWrapMonad refreshDimensions >>= \refreshDimensions ->
	jsWrapMonad (onAttrMutation refreshDimensions) >>= \onAttrMutation ->
	accJS (jsNew "MutationObserver" onAttrMutation) >>= \mutation_observer ->
	let config = jsRecord ["attributeFilter" :> ["style"], "attributes" :> True] in
	appJS (mutation_observer .# "observe" .$! (sidebar_parent, config))
where
	domEl = me .# "domEl"
	sidebar_parent = domEl .# "parentNode.parentNode.parentNode"

	onAttributeChange {[0]=name,[1]=val}
		# name = fromJS "" name
		| name == "values" =
			accJS (me .# "table.getPlugin" .$ "columnSorting") >>= \columnSorting ->
			accJS (columnSorting .# "getSortConfig" .$ ()) >>= \sortConfig ->
			appJS (me .# "table.loadData" .$! val) >>|
			appJS (columnSorting .# "sort" .$! sortConfig)
		| otherwise =
			appJS (jsTrace ("unknown attribute change: "+++name))

	onAttrMutation f _ =
		appJS (jsWindow .# "clearTimeout" .$! (me .# "resize_timeout_id")) >>|
		accJS (jsWindow .# "setTimeout" .$ (f, 100)) >>= \id ->
		appJS (me .# "resize_timeout_id" .= id)
	refreshDimensions _ =
		appJS (domEl .# "firstChild.style.display" .= "none") >>|
		accJS ((.?) (domEl .# "clientWidth")) >>= \w ->
		appJS (domEl .# "firstChild.style.display" .= "block") >>|
		appJS (me .# "table.updateSettings" .$! jsRecord
			[ "width"  :> maybe "100%" (\w -> toString w+++"px") (jsValToInt w)
			])
