implementation module Gui.Window

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdOverloadedList

import Control.Applicative
import Control.Monad => qualified return, sequence, forever, join
import Data.Either
import Data.Func
import Data.Functor
from Data.Map import :: Map
import qualified Data.Map
import Data.Tuple
import System.FilePath
from Text import class Text(concat,join), instance Text String, concat3, concat4, concat5
import Text.HTML

import iTasks
import iTasks.Extensions.DateTime
import iTasks.Extensions.SearchField
import iTasks.Internal.Task
import iTasks.Internal.TaskIO
import iTasks.ResizeableSplit
import iTasks.UI.Editor.Common
import iTasks.UI.JavaScript

import ABC.Interpreter.JavaScript.Monad

import Electron.App
import Electron.Dialog

import TextFabric
import TextFabric.BHSA
import TextFabric.Filters

import Bible

import MBQS.Languages.Parsers
import MBQS.Model

import Gui.Common
import Gui.Derives
import Gui.DistinctColumnValues
import Gui.HebrewTextViewer
import Gui.Main
import Gui.SearchHistory
import Gui.Settings
import Gui.Shares
import Gui.Sheet => qualified addColumn, addDisambiguatingColumn, renameColumn,
	deleteColumn, addEntry, deleteEntry
import Gui.Util

derive class iTask MBQSWindow

gDefault{|MBQSWindow|} =
	{ file_path    = ?None
	, menu         = fullApplicationMenu
	, sheet        = ?None
	, sheet_task   = TaskId -1 -1
	, search_field = {query="", action= ?None}
	}

windows :: SDSLens ElectronWindow (?MBQSWindow) (?MBQSWindow)
windows =: sdsLens
	"window"
	(const ())
	(SDSRead \win windows -> Ok ('Data.Map'.get win windows))
	(SDSWrite \win windows newVal -> Ok $ ?Just case newVal of
		?None     -> 'Data.Map'.del win windows
		?Just val -> 'Data.Map'.put win val windows)
	(SDSNotifyWithoutRead \win _ _ win2 -> win == win2)
	(?Just \win windows -> Ok ('Data.Map'.get win windows))
	(share "windows" 'Data.Map'.newMap)

openInNewWindow :: !FilePath -> Task ElectronWindow
openInNewWindow path =
	createWindow
		{ ElectronWindowOptions
		| defaultOptions
		& task = \win -> open path win >-| sheetView win
		} >>- \win ->
	set (?Just {defaultValue & file_path= ?Just path}) (sdsFocus win windows) @!
	win
where
	open path win =
		openSheet path >>- uncurry mbGetValues >>- \(settings,values) ->
		upd
			(fmap \w ->
				{ MBQSWindow | w
				& sheet = ?Just
					{ html_id                   = ?Just "main"
					, settings                  = settings
					, values                    = values
					, shown_rows                = ?None
					, distinct_values           = DistinctColumnValues [!!]
					, distinct_values_selection = DistinctColumnValues [!!]
					, unsaved_changes           = ?None
					, current_cell              = ?None
					, current_selection         = ?None
					}
				})
			(sdsFocus win windows)

	mbGetValues settings ?None =
		getDataSet (requiredFeatures settings) >>- \data ->
		case getValues settings data of
			Error e ->
				throw ("Failed to import data: "+++e)
			Ok values ->
				mbGetValues settings (?Just values)
	mbGetValues settings (?Just values) =
		return (settings, values)

withMBQSWindowWithSheet :: !ElectronWindow !(MBQSWindow -> Task ()) -> Task ()
withMBQSWindowWithSheet win task=
	get (sdsFocus win windows) >>- \window -> case window of
	?Just mbqsWindow=:{sheet= ?Just _} ->
		task mbqsWindow
	?Just _ ->
		throw "This window contains no sheet"
	?None ->
		throw "Illegal window passed to close"

addColumn :: !ElectronWindow -> Task ()
addColumn win =
	withMBQSWindowWithSheet win \{sheet= ?Just sheet,sheet_task=TaskId instanceNo _} ->
	createWindow
		{ defaultOptions
		& parent = ?Just win
		, task = \_ -> addColumnTask sheet instanceNo
			( {title="", css_class= ?None, expression= ?None}
			, length sheet.settings.SheetSettings.columns
			)
		, width = 400
		, height = 300
		} @!
	()
where
	addColumnTask sheet=:{settings} instanceNo val =
		(tune (Title "Add a column") $ tune (AddCSSClass "small") $ tune ScrollContent $
			(Hint "Enter column details:" @>> heightAttr WrapSize @>>
			updateInformation
				[UpdateUsing id (\_ x -> x) (editor settings)]
				val) -||
			(viewInformation
				[ViewAs \ns -> DetailsTag []
					[ SummaryTag [] [Text "Click to see nodes defined in this sheet"]
					, UlTag [] [LiTag [] [Text name] \\ name <- ns]
					]]
				[n.NodeSettings.name \\ n <- settings.SheetSettings.nodes])) >>*
		[ OnAction ActionCancel $ always closeWindow
		, OnAction ActionContinue $ hasValue \val=:(column_settings,place) ->
			catchAll (
				enterInformation [EnterUsing id (mapEditorWrite ValidEditor loader)] ||-
				// NB: ugly hack: waitForTimer is needed so that the UILoader is shown
				(waitForTimer False 1 >-| 'Gui.Sheet'.addColumn place column_settings sheet) >>- \sheet ->
				upd (fmap \w -> {w & sheet= ?Just sheet}) (sdsFocus win windows) >-|
				mkInstantTask (\_ iw -> (Ok (), queueEvent instanceNo ResetEvent iw)) >-|
				closeWindow
			) \e ->
				showSimpleMessageBox WarningMsg e >-|
				addColumnTask sheet instanceNo val
		]

	editor settings =
		mapEditorRead (\(s,p) -> (s.ColumnSettings.title, s.css_class, s.expression, p)) $
		mapEditorWrite (fmap write o editorReportTuple4) $
		container4
			(Label "Title" @>>      columnTitleEditor)
			(Label "Style" @>>      columnStyleEditor)
			(Label "Expression" @>> columnExpressionEditor)
			(Label "Place" @>>      columnPlaceEditor settings)
	where
		write (t,s,e,p) =
			( {title=t, css_class=s, expression=e}
			, p
			)

addDisambiguatingColumn :: !ElectronWindow -> Task ()
addDisambiguatingColumn win =
	withMBQSWindowWithSheet win \{sheet= ?Just sheet,sheet_task=TaskId instanceNo _} ->
	createWindow
		{ defaultOptions
		& parent = ?Just win
		, task = \_ -> addColumnTask sheet instanceNo
			( {title="", css_class= ?None, expression= ?None}
			, length sheet.settings.SheetSettings.columns
			)
			[]
		, width = 400
		, height = 300
		} @!
	()
where
	addColumnTask sheet=:{settings} instanceNo val sel =
		(tune (Title "Add a disambiguating column") $ tune (AddCSSClass "small") $ tune ScrollContent $
			(Hint "Enter column details:" @>> heightAttr WrapSize @>>
			updateInformation
				[UpdateUsing id (\_ x -> x) (editor settings)]
				val) -&&-
			(Hint "Select columns to disambiguate:" @>> heightAttr WrapSize @>>
			updateMultipleChoice
				[ChooseFromCheckGroup id]
				[c.ColumnSettings.title \\ c <- columns]
				sel)) >>*
		[ OnAction ActionCancel $ always closeWindow
		, OnAction ActionContinue $ ifValue (\(_, sel) -> sel=:[_:_]) \(val=:(column_settings,place), sel) ->
			catchAll (
				enterInformation [EnterUsing id (mapEditorWrite ValidEditor loader)] ||-
				// NB: ugly hack: waitForTimer is needed so that the UILoader is shown
				(waitForTimer False 1 >-| 'Gui.Sheet'.addDisambiguatingColumn place column_settings (indices sel columns) sheet) >>- \sheet ->
				upd (fmap \w -> {w & sheet= ?Just sheet}) (sdsFocus win windows) >-|
				mkInstantTask (\_ iw -> (Ok (), queueEvent instanceNo ResetEvent iw)) >-|
				closeWindow
			) \e ->
				showSimpleMessageBox WarningMsg e >-|
				addColumnTask sheet instanceNo val sel
		]
	where
		columns = settings.SheetSettings.columns

		indices sel columns = [i \\ c <- columns & i <- [0..] | isMember c.ColumnSettings.title sel]

	editor settings =
		mapEditorRead (appFst \s -> s.ColumnSettings.title) $
		mapEditorWrite (fmap (appFst \t -> {title=t, css_class= ?None, expression= ?None}) o editorReportTuple2) $
		container2
			(Label "Title" @>> columnTitleEditor)
			(Label "Place" @>> columnPlaceEditor settings)

columnPlaceEditor settings :== chooseWithDropdown
	[ "At the start"
	: [concat4 "Between " c1 " and " c2 \\ c1 <- titles & c2 <- tl titles] ++
	[ "At the end"
	]]
where
	titles = [c.ColumnSettings.title \\ c <- settings.SheetSettings.columns]

renameColumn :: !ElectronWindow -> Task ()
renameColumn win =
	withMBQSWindowWithSheet win \{sheet= ?Just sheet=:{current_cell},sheet_task=TaskId instanceNo _} ->
	createWindow
		{ defaultOptions
		& parent = ?Just win
		, task = \_ -> renameColumnTask sheet instanceNo (maybe 0 snd current_cell)
		, width = 300
		, height = 300
		} @!
	()
where
	renameColumnTask sheet=:{Sheet|settings,current_cell} instanceNo val =
		(Title "Rename column" @>> AddCSSClass "small" @>> ScrollContent @>> (
			((Hint "Choose the column to rename:" @>>
				updateChoice
					[ChooseFromDropdown ((!!) titles)]
					[i \\ i <- [0..] & _ <- titles]
					val)
			-&&- (Hint "The new name for the column:" @>>
				enterInformation [])))) >>*
		[ OnAction ActionCancel $ always closeWindow
		, OnAction ActionContinue $ hasValue \(col,title) ->
			catchAll (
				'Gui.Sheet'.renameColumn col title sheet >>- \sheet ->
				upd (fmap \w -> {w & sheet= ?Just sheet}) (sdsFocus win windows) >-|
				mkInstantTask (\_ iw -> (Ok (), queueEvent instanceNo ResetEvent iw)) >-|
				closeWindow
			) \e ->
				showSimpleMessageBox WarningMsg e >-|
				renameColumnTask sheet instanceNo val
		]
	where
		titles = [c.ColumnSettings.title \\ c <- settings.SheetSettings.columns]

deleteColumn :: !ElectronWindow -> Task ()
deleteColumn win =
	withMBQSWindowWithSheet win \{sheet= ?Just sheet=:{current_cell},sheet_task=TaskId instanceNo _} ->
	createWindow
		{ defaultOptions
		& parent = ?Just win
		, task = \_ -> deleteColumnTask sheet instanceNo (maybe 0 snd current_cell)
		, width = 300
		, height = 200
		} @!
	()
where
	deleteColumnTask sheet=:{Sheet|settings,current_cell} instanceNo val =
		(Title "Delete column" @>> AddCSSClass "small" @>> ScrollContent @>>
			Hint "Choose the column to delete:" @>>
			updateChoice
				[ChooseFromDropdown ((!!) titles)]
				[i \\ i <- [0..] & _ <- titles]
				val) >>*
		[ OnAction ActionCancel $ always closeWindow
		, OnAction ActionContinue $ hasValue \col ->
			catchAll (
				'Gui.Sheet'.deleteColumn col sheet >>- \sheet ->
				upd (fmap \w -> {w & sheet= ?Just sheet}) (sdsFocus win windows) >-|
				mkInstantTask (\_ iw -> (Ok (), queueEvent instanceNo ResetEvent iw)) >-|
				closeWindow
			) \e ->
				showSimpleMessageBox WarningMsg e >-|
				deleteColumnTask sheet instanceNo val
		]
	where
		titles = [c.ColumnSettings.title \\ c <- settings.SheetSettings.columns]

addRows :: !ElectronWindow -> Task ()
addRows win =
	withMBQSWindowWithSheet win \{sheet= ?Just sheet,sheet_task=TaskId instanceNo _} ->
	case sheet.settings.SheetSettings.nodes of
		[{NodeSettings|type="verse"}] ->
			createWindow
				{ defaultOptions
				& parent = ?Just win
				, task = \_ -> addRowsTask sheet instanceNo
				, width = 400
				, height = 300
				} @!
			()
		_ ->
			showSimpleMessageBox InfoMsg
				"Currently, rows can only be added to sheets with one node (of type verse)."
where
	addRowsTask sheet=:{settings} instanceNo =
		getDataSet ["book", "chapter", "verse"] >>- \data ->
		(
			Hint "Verse reference:" @>>
			enterInformation [EnterUsing id bibleReferenceTextField]
		>&^
			viewSharedInformation
				// flex-end is needed to align short Hebrew verses to the right
				[ViewUsing (textToHTML data) (styleAttr "align-self:flex-end;" @>> htmlView)]
		) >>*
		[ OnAction ActionCancel $ always closeWindow
		, OnAction ActionOk $ ifValue (isJust o findNode data) \ref ->
			catchAll
				(addEntry ref data >-| closeWindow)
				\e ->
					showSimpleMessageBox WarningMsg e >-|
					addRowsTask sheet instanceNo
		, OnAction ActionNext $ ifValue (isJust o findNode data) \ref ->
			catchAll
				(addEntry ref data)
				(\e -> showSimpleMessageBox WarningMsg e @! sheet) >>-
			flip addRowsTask instanceNo
		]
	where
		textToHTML data ?None = Text ""
		textToHTML data (?Just ref) = case findNode data ref of
			?None ->
				PTag [] [Text "Failed to find reference"]
			?Just node ->
				PTag [ClassAttr "hebrew large"] [Text (concat (map snd (get_text node data)))]

		addEntry ref data =
			enterInformation [EnterUsing id (mapEditorWrite ValidEditor loader)] ||-
			// NB: ugly hack: waitForTimer is needed so that the UILoader is shown
			(waitForTimer False 1 >-| 'Gui.Sheet'.addEntry [node] sheet) >>- \sheet ->
			upd (fmap \w -> {w & sheet= ?Just sheet}) (sdsFocus win windows) >-|
			mkInstantTask (\_ iw -> (Ok sheet, queueEvent instanceNo ResetEvent iw))
		where
			(?Just node) = findNode data ref

		findNode data {book,chapter,verse} =
			let
				(?Just [ft_book,ft_chapter,ft_verse:_]) =
					get_node_feature_ids ["book", "chapter", "verse"] data
				nodes = filter_node_refs
					(\_ node _ ->
						get_node_feature 0 node == "verse" &&
						get_node_feature ft_book node == toString book &&
						toInt (get_node_feature ft_chapter node) == chapter &&
						toInt (get_node_feature ft_verse node) == verse)
					data
			in
			case nodes of
				[|] ->
					?None
				[|n:_] ->
					?Just n

deleteRow :: !ElectronWindow -> Task ()
deleteRow win =
	withMBQSWindowWithSheet win \{sheet= ?Just sheet=:{current_cell},sheet_task=TaskId instanceNo _} ->
	case current_cell of
		?None ->
			showSimpleMessageBox InfoMsg "Select the row you want to delete before performing this action."
		?Just (row,_) ->
			let human_row = toString (row+1) in
			showMessageBox QuestionMsg ("Delete row " +++ human_row)
				(concat3 "Are you sure you want to delete row " human_row "?")
				["Yes", "No"] 1 >>- \answer ->
			case answer of
				?Just "Yes" ->
					catchAll (
						'Gui.Sheet'.deleteEntry row sheet >>- \sheet ->
						upd (fmap \w -> {w & sheet= ?Just sheet}) (sdsFocus win windows) >-|
						mkInstantTask (\_ iw -> (Ok (), queueEvent instanceNo ResetEvent iw))
					) \e ->
						showSimpleMessageBox WarningMsg e
				_ ->
					return ()

save :: !Bool !ElectronWindow -> Task ()
save autosave win =
	get share >>- \win -> case win of
	?None ->
		throw "Illegal window passed to save"
	?Just {file_path,sheet}
		| isNone file_path ->
			throw "No file path is set for this window"
		| isNone sheet ->
			throw "This window contains no sheet"
		| otherwise ->
			let {settings,values} = fromJust sheet in
			saveSheet (fromJust file_path+++if autosave "~" "") settings values >-|
			upd (fmap \w -> {w & sheet=unsetChanges <$> w.sheet}) share @!
			()
where
	share = sdsFocus win windows

	unsetChanges s = {s & unsaved_changes= if autosave (?Just 0) ?None}

close :: !ElectronWindow -> Task ()
close win=:(ElectronWindow id) =
	get (sdsFocus win windows) >>- \window -> case window of
	?Just {sheet= ?Just {unsaved_changes}}
		| isNone unsaved_changes ->
			runInMainProcess (doClose id)
		| otherwise ->
			showMessageBox QuestionMsg "Save file?"
				"This file has unsaved changes. Do you want to save them before closing?"
				["&Don't save", "&Cancel", "&Save"]
				1 >>- \choice ->
			case choice of
				?Just "&Don't save" ->
					runInMainProcess (doClose id)
				?Just "&Save" ->
					save False win >-|
					runInMainProcess (doClose id)
				_ ->
					return ()
	?Just _ ->
		runInMainProcess (doClose id)
	?None ->
		throw "Illegal window passed to close"
where
	doClose id me w
		# (win,w) = (me .# "windows.get" .$ id) w
		# w = (win .# "close" .$! ()) w
		= w

focusSearchBar :: !ElectronWindow -> Task ()
focusSearchBar win =
	get share >>- \window -> case window of
	?Just window ->
		set (?Just {window & search_field.action= ?Just FocusSearchBar}) share @!
		()
	?None ->
		throw "Illegal window passed to close"
where
	share = sdsFocus win windows

cancelSearch :: !ElectronWindow -> Task ()
cancelSearch win =
	get share >>- \window -> case window of
	?Just window ->
		set (?Just {window & search_field.query=""}) share @!
		()
	?None ->
		throw "Illegal window passed to close"
where
	share = sdsFocus win windows

trimBySearch :: !ElectronWindow -> Task ()
trimBySearch win =
	showMessageBox WarningMsg "Trim sheet by query" "This will delete all rows that are not currently shown. Continue?"
		["Ok", "Cancel"] 1 >>- \answer ->
	case answer of
		?Just "Ok" ->
			withMBQSWindowWithSheet win \{sheet= ?Just sheet,sheet_task=TaskId instanceNo _} ->
			upd (fmap \w -> {w & sheet= ?Just (trimByShownRows sheet)}) (sdsFocus win windows) >-|
			mkInstantTask (\_ iw -> (Ok (), queueEvent instanceNo ResetEvent iw))
		_ ->
			return ()

toggleTag :: !Int !ElectronWindow -> Task ()
toggleTag tag win=:(ElectronWindow winId) = runInMainProcess
	\me w
		# (mbWin,w) = getBrowserWindow win w
		| isNone mbWin
			-> (jsGlobal "alert" .$! ("Failed to find the BrowserWindow with id " +++ toString winId)) w
			-> (fromJust mbWin .# "webContents.send" .$! ("toggle-tag", tag)) w

sheetView :: !ElectronWindow -> Task ()
sheetView win =
	get window >>- \window_val ->
	AddCSSClass "mbqs-main" @>>
	(
		updateInTable win (fromMaybe "" window_val.file_path) search_field sheet sheet_task file_path
	-&&-
		statusBar window file_path
	) @! ()
where
	window = mapReadWrite (fromJust, \w _ -> ?Just (?Just w)) ?None (sdsFocus win windows)

	search_field = mapReadWrite (\w -> w.search_field,   \q w -> ?Just {w & search_field=q}) ?None window
	sheet        = mapReadWrite (\w -> fromJust w.sheet, \s w -> ?Just {w & sheet= ?Just s}) ?None window
	sheet_task   = mapReadWrite (\w -> w.sheet_task,     \i w -> ?Just {w & sheet_task=i})   ?None window
	file_path    = mapReadWrite (\w -> w.file_path,      \p w -> ?Just {w & file_path=p})    ?None window

updateInTable ::
	!ElectronWindow
	!String
	!(SimpleSDSLens Query)
	!(SimpleSDSLens Sheet)
	!(SimpleSDSLens TaskId)
	!(SimpleSDSLens (?FilePath))
	-> Task ()
updateInTable win tableId query sheet sheetTaskId file_path =
	// Start loading data in the background, because it is used to show Hebrew text:
	appendTopLevelTask 'Data.Map'.newMap True (getDataSet []) >-|
	appendTopLevelTask 'Data.Map'.newMap True setupESword >-|
	// The actual task:
	getSettings >>- \settings ->
	appendTopLevelTask 'Data.Map'.newMap True (onChange query search) >>- \searchTaskId ->
	appendTopLevelTask 'Data.Map'.newMap True (onChange sheet mbAutoSave) >>- \autoSaveTaskId ->
	{ResizeableSplit & storeSizesId = ?Just "mbqs-main"} @>> ArrangeHorizontal @>>
	(
		(
			(AddCSSClass "mbqs-search" @>> (
				updateSharedInformation
					[UpdateSharedUsing id (\_ x -> x) (searchField searchValidator)]
					query
			-&&-
				viewInformation [] (ITag [ClassAttr "fas fa-search"] [])
			))
		-&&-
			withStoredTaskId sheetTaskId
			(AddCSSClass "mbqs-table" @>>
			updateSharedInformation
				[UpdateSharedUsing id (\_ x -> x) (tableIdAttr tableId @>> sheetEditor)]
				sheet)
		)
	-&&-
		({ResizeableSplit & storeSizesId = ?Just "mbqs-sidebar"} @>> allTasks
			[ AddCSSClass "toolbox" @>> AddCSSClass "toolbox-text" @>>
				Title "Hebrew text" @>>
				ScrollContent @>>
				ApplyLayout (wrapUI UIPanel) @>> // should not be needed, but otherwise the title does not appear
				viewHebrewText @! ()
			, AddCSSClass "toolbox" @>> AddCSSClass "toolbox-text" @>>
				Title "English text" @>>
				SecondaryTitleIcon "info" (englishTranslationAttribution settings) @>>
				ScrollContent @>>
				AddCSSClass "with-sup" @>> AddCSSClass "english-text" @>>
				viewSharedInformation [ViewUsing id (JavaScriptInit englishTextInit @>> htmlView)] english_text @! ()
			, AddCSSClass "toolbox" @>>
				Title "Distinct column values" @>>
				SecondaryTitleIcon "info" "This toolbox shows all the different valus in the currently selected column and how often they occur" @>>
				ScrollContent @>>
				viewSharedInformation [] distinct_values @! ()
			, AddCSSClass "toolbox" @>>
				Title "Search history" @>>
				viewSearchHistory @! ()
			])
	) >-|
	removeTask searchTaskId topLevelTasks >-|
	removeTask autoSaveTaskId topLevelTasks
where
	searchValidator "" = Ok ()
	searchValidator q  = parseNodeFilterDescription q $> ()

	search {query=""} = upd (\s -> {s & shown_rows = ?None}) sheet @! ?None
	search {query} = exceptionsToErrors $
		case parseNodeFilterDescription query of
			Error e ->
				throw e
			Ok filterDescription ->
				get sheet >>- \s=:{Sheet | settings,values} ->
				let columns = {c.ColumnSettings.title \\ c <- settings.SheetSettings.columns} in
				case nodeFilterDescriptionToSheetEntryCheck columns filterDescription of
					Error e ->
						throw e
					Ok check ->
						set (AddEntry query) search_history >-|
						let rows = {#i \\ i <- [0..] & e <-: values.entries | check e} in
						set {s & shown_rows = ?Just rows} sheet @! ()

	search_history = sdsSequence "search_history"
		(const ())
		(\_ mbPath -> maybe (Left []) (\path -> Right (path, id)) mbPath)
		(SDSWriteConst \_ _ -> Ok ?None)
		(SDSSequenceWriteConst \_ mbPath query -> Ok ((\path -> (path, query)) <$> mbPath))
		file_path
		searchHistory

	viewSearchHistory = forever (
			enterChoiceWithShared [ChooseFromList id] search_history >>*
			[ OnAction (Action "Delete") $ hasValue \q -> set (RemoveEntry q) search_history @! ()
			, OnAction (Action "Search again") $ hasValue \q -> set {query=q,action= ?None} query @! ()
			]
		)

	mbAutoSave {unsaved_changes= ?Just n} | n >= 10 = exceptionsToWarnings (save True win) @! ()
	mbAutoSave _ = return ()

	viewHebrewText =
		JavaScriptInit addCopyListener @>>
		viewHebrewWithMorphologyPopups (mapRead currentTextNodeAndColoredNodes sheet)
	where
		currentTextNodeAndColoredNodes {settings,values,current_cell= ?Just (row,_)}
			| 0 > row || row >= size values.entries // prevents an index error when selecting the column header
				= ?None
			| node == -1 // node can be -1 in case of an optional node
				= ?None
				= ?Just
					( node
					,
						[ (colored_node, color)
						\\ (node_name,color) <- settings.text_panel.colored_nodes
						, let colored_node = row_nodes.[get_node_id node_name]
						| colored_node <> -1
						]
					)
		where
			row_nodes = values.entries.[row].SheetEntry.nodes
			get_node_id name = hd [i \\ n <-: values.node_names & i <- [0..] | n == name]
			node = row_nodes.[get_node_id (fromJust settings.text_panel.text_node)]
		currentTextNodeAndColoredNodes _ = ?None

	addCopyListener _ me w
		# (initDOMEl,w) = jsWrapFun (initDOMEl me) me w
		# w = (me .# "initDOMEl" .= initDOMEl) w
		= w
	where
		initDOMEl me _ w
			# (copyListener,w) = jsWrapFun copyListener me w
			# w = (me .# "domEl.addEventListener" .$! ("copy", copyListener)) w
			= w

		copyListener {[0]=event} w
			# data = jsCall (jsGlobal "String") (jsCall (jsDocument .# "getSelection") ())
			# w = (event .# "clipboardData.setData" .$! ("text/plain", data)) w
			# w = (event .# "preventDefault" .$! ()) w
			= w

	current_reference = sdsParallel
		"current_reference"
		(\_ -> ((), ())) // parameters
		(\(sheet, mbData) -> getReference sheet mbData)
		(SDSWriteConst \_ () -> Ok ?None) // write left
		(SDSWriteConst \_ () -> Ok ?None) // write right
		sheet
		data_set
	where
		getReference {current_cell= ?None} _ = ?None
		getReference _ ?None = ?None
		getReference {settings,values,current_cell= ?Just (row,_)} (?Just data)
			| 0 > row || row >= size values.entries // prevents an index error when selecting the column header
				= ?None
			| node_ref == -1 // optional node with no value
				= ?None
				= ?Just {book=book, chapter=chapter, verse=verse}
		where
			data_nodes = data.DataSet`.nodes
			row_nodes = values.entries.[row].SheetEntry.nodes
			get_node_id name = hd [i \\ n <-: values.node_names & i <- [0..] | n == name]
			node_ref = row_nodes.[get_node_id (fromJust settings.text_panel.text_node)]
			node = data_nodes.[node_ref]

			[fbook,fchapter,fverse:_] = fromJust (get_node_feature_ids ["book","chapter","verse"] data)
			book    = fromString (get_node_feature fbook node)
			chapter = toInt (get_node_feature fchapter node)
			verse   = toInt (get_node_feature fverse node)

	englishTranslationAttribution {english_bible_abbreviation,english_bible_copyright}
		| isNone english_bible_copyright
			= fromMaybe default english_bible_abbreviation
		| isNone english_bible_abbreviation
			= fromMaybe default english_bible_copyright
			= concat3 (fromJust english_bible_abbreviation) ": " (fromJust english_bible_copyright)
	where
		default = "There is no information about this translation"

	english_text =
		mapRead Html $
		sdsSequence
			"english_text"
			(\() -> ())
			(\() ref -> if (isNone ref)
				(Left "")
				(Right (fromJust ref, maybe error details o error2mb)))
			(SDSWriteConst \_ () -> Error (dynamic (), "write in english_text"))
			(SDSSequenceWriteConst \_ _ _ -> Error (dynamic (), "write in english_text"))
			current_reference
			englishChapter
	where
		details verses = join "<br/>" verses
		error = "The English translation could not be found."

	englishTextInit _ me w
		# (scroll,w) = jsWrapFun (scroll me) me w
		# w = addScroll "initDOMEl" scroll me w
		# w = addScroll "onAttributeChange" scroll me w
		= w
	where
		addScroll f scroll me w
			# (oldf,w) = me .# f .? w
			# (newf,w) = jsWrapFun (applyAndScroll oldf scroll me) me w
			# w = (me .# f .= newf) w
			= w
		where
			applyAndScroll f scroll me args w
				# w = (f .# "apply" .$! (me, [a \\ a <-: args])) w
				# w = (jsWindow .# "setTimeout" .$! (scroll, 100)) w
				= w

		scroll me _ w
			# (current_verse,w) = (me .# "domEl.querySelector" .$ ".current") w
			| jsIsNull current_verse = w
			# (offset_top,w) = current_verse .# "offsetTop" .?? (0, w)
			# (verse_height,w) = current_verse .# "offsetHeight" .?? (0, w)
			# (my_height,w) = me .# "domEl.offsetHeight" .?? (0, w)
			# scroll_pos = min offset_top (offset_top - (my_height - verse_height) / 2)
			# w = (me .# "domEl.scrollTo" .$! (0, scroll_pos)) w
			= w

	distinct_values = mapRead
		(\s ->
			{ values = if (isJust s.current_selection) s.distinct_values_selection s.distinct_values
			, column = snd <$> s.current_cell
			})
		sheet

statusBar :: !(SimpleSDSLens MBQSWindow) !(SimpleSDSLens (?FilePath)) -> Task ()
statusBar window file_path =
	ApplyLayout (wrapUI UIToolBar) @>> ArrangeHorizontal @>>
	allTasks
		[ viewSharedInformation [ViewUsing (\w -> fromJust w.sheet) unsavedChangesView] window @! ()
		, viewSharedInformation [ViewUsing (\w -> fromJust w.sheet) nrSearchResultsView] window @! ()
		, viewSharedInformation [ViewUsing (\w -> fromJust w.sheet) selectionView] window @! ()
		, viewInformation [ViewUsing id (widthAttr FlexSize @>> textView)] "" @! ()
		, viewSharedInformation [ViewAs id] file_path @! ()
		]
	@! ()
