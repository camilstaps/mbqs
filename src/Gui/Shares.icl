implementation module Gui.Shares

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdDebug

import Data.Func
import System.Environment
import System.FilePath
from Text import class Text(concat,join), instance Text String, concat3

import iTasks
import iTasks.Internal.IWorld
import iTasks.Internal.SDS
import iTasks.Internal.Task

import TextFabric
import TextFabric.Import

import Bible

import ESword

import Gui.Derives
import Gui.Settings

derive JSONEncode ESwordModule

ALWAYS_NEEDED_FEATURES =
	[ "book"
	, "chapter"
	, "verse"

	, "g_word_utf8"
	, "trailer_utf8"

	, "lex_utf8"
	, "gloss"
	, "gn"
	, "nu"
	, "ps"
	, "vs"
	, "vt"
	]

loaded_features :: SimpleSDSLens [String]
loaded_features = mapReadWrite
	( fromMaybe []
	, \val _ -> ?Just (?Just val)
	)
	?None
	(sdsFocus "loaded_features" memoryShare)

getTextFabricPath :: Task FilePath
getTextFabricPath =
	getSettings >>- \settings -> case settings.tf_path of
		?Just path ->
			return path
		?None ->
			accWorld (getEnvironmentVariable (IF_WINDOWS "USERPROFILE" "HOME")) >>- \mbHome ->
			let home = fromMaybe "" mbHome in
			return (concatPaths [home,"text-fabric-data","github","etcbc","bhsa","tf","2021"])

data_set :: SimpleSDSLens (?DataSet)
data_set =: sdsFocus "data_set" memoryShare

getDataSet :: ![String] -> Task DataSet
getDataSet required_features =
	get loaded_features >>- \loaded
	| all (\f -> isMember f loaded) features ->
		get data_set >>- \data -> case data of
			?Just data
				-> return data
				-> throw "internal error in getDataSet\n"
	| otherwise ->
		// TODO: we now simply re-import everything; it would be better to
		// recognize the case that we already imported the whole graph
		// structure and only need to add some features.
		getTextFabricPath >>- \tf_path ->
		let new_features = removeDup (features ++ loaded) in // don't discard previously loaded data
		set ?None data_set >-| // free memory
		accWorld (import_tf (\fn w -> trace_n ("Importing "+++fn+++"...") w) new_features tf_path) >>- \data ->
		case data of
			Error e ->
				throw $ concat
					[ "Failed to load Text-Fabric data from '"
					, tf_path
					, "': "
					, e
					, ". Set correct settings in File > Settings."
					]
			Ok data ->
				set new_features loaded_features >-|
				set (?Just data) data_set @
				fromJust
where
	features = removeDup (ALWAYS_NEEDED_FEATURES ++ required_features)

all_features :: SimpleSDSLens (?[(String,FeatureType)])
all_features =: sdsFocus "features" memoryShare

getAllFeatures :: Task [(String,FeatureType)]
getAllFeatures =
	get all_features >>- \features ->
	case features of
		?Just features ->
			return features
		?None ->
			getTextFabricPath >>- \tf_path ->
			accWorld (get_features tf_path) >>- \features ->
			case features of
				Error e ->
					throw $ concat
						[ "Failed to load Text-Fabric features from '"
						, tf_path
						, "': "
						, e
						, ". Set correct settings in File > Settings."
						]
				Ok features ->
					set (?Just features) all_features @
					fromJust

:: *Resource
	| ESwordResource !ESwordModule

isESwordResource :: !*Resource -> *(!Bool, !*Resource)
isESwordResource r=:(ESwordResource _) = (True, r)
isESwordResource r = (False, r)

setupESword :: Task ()
setupESword =
	getSettings >>- \{english_bible_db} ->
	case english_bible_db of
		?None ->
			return ()
		?Just path ->
			accWorld (\w
				# (mod,w) = openModule path w
				| isError mod
					-> (?None, w)
					-> (?Just (fromOk mod), w)) >>- \mod ->
			case mod of
				?None ->
					return ()
				?Just mod ->
					mkInstantTask \_  iworld ->
						( Ok ()
						, {iworld & resources=[ESwordResource mod:iworld.resources]}
						)

maxVerse :: SDSSource (Book, Int) (MaybeError String Int) ()
maxVerse =: SDSSource
	{ SDSSourceOptions
	| name  = "maxVerse"
	, read  = read
	, write = \_ _ iworld -> (Error (dynamic (), "attempt to write maxVerse"), iworld)
	}
where
	read (book, chapter) iworld
		# (mod,iworld) = iworldResource isESwordResource iworld
		| isEmpty mod
			= (Ok (Error "No translation module available"), iworld)
		# mod = (\(ESwordResource mod) -> mod) (hd mod)
		# (verse,mod) = getMaxVerse book chapter mod
		# iworld & resources = [ESwordResource mod:iworld.resources]
		| isError verse
			= (Ok (liftError verse), iworld)
			= (Ok (Ok (fromOk verse)), iworld)

verseWithCorrectReference :: SDSSource Reference (MaybeError String String) ()
verseWithCorrectReference =: SDSSource
	{ SDSSourceOptions
	| name  = "verseWithCorrectReference"
	, read  = read
	, write = \_ _ iworld -> (Error (dynamic (), "attempt to write verseWithCorrectReference"), iworld)
	}
where
	read ref iworld
		# (mod,iworld) = iworldResource isESwordResource iworld
		| isEmpty mod
			= (Ok (Error "No translation module available"), iworld)
		# mod = (\(ESwordResource mod) -> mod) (hd mod)
		# (text,mod) = getVerse ref mod
		# iworld & resources = [ESwordResource mod:iworld.resources]
		| isError text
			= (Ok (liftError text), iworld)
			= (Ok (Ok (eSwordMarkupToHTML False (fromOk text))), iworld)

englishChapter :: SDSSource Reference (MaybeError String [String]) ()
englishChapter =: SDSSource
	{ SDSSourceOptions
	| name  = "englishChapter"
	, read  = readVerses
	, write = \_ _ iworld -> (Error (dynamic (), "attempt to write englishChapter"), iworld)
	}
where
	readVerses ref iworld
		# (mbMaxVerse,iworld) = read (sdsFocus (ref.book, ref.chapter) maxVerse) EmptyContext iworld
		  maxVerseReadResult = fromOk mbMaxVerse
		  maxVerse = (\(ReadingDone (Ok v)) -> v) maxVerseReadResult
		| isError mbMaxVerse
			= (liftError mbMaxVerse, iworld)
		| not (maxVerseReadResult=:(ReadingDone (Ok _)))
			= (Error (dynamic (), "async read or failure to read maxVerse in englishChapter"), iworld)

		# verses = [{ref & verse=v} \\ v <- [1..maxVerse]]
		# (mbResults,iworld) = (mapSt (\r -> read (sdsFocus r verseWithCorrectReference) EmptyContext) verses iworld)
		  results = map fromOk mbResults

		| any isError mbResults
			= (liftError (hd (filter isError mbResults)), iworld)
		| any (\res -> res=:(Reading _)) results
			= (Error (dynamic (), "async read result in englishChapter"), iworld)

		# english_verses = hebrew_to_english ref
		# results = [markup (isMember r english_verses) r v \\ r <- verses & ReadingDone (Ok v) <- results]

		| isEmpty results
			= (Ok (Error "No English text found"), iworld)
			= (Ok (Ok results), iworld)
	where
		converted_refs = hebrew_to_english ref
		start = hd converted_refs
		end = last converted_refs

		markup current r v = concat
			[ if current "<span class='current'>" "<span>"
			, "<sup>"
			, if (r.verse == 1) (concat3 (toString r.chapter) ":" (toString r.verse)) (toString r.verse)
			, "</sup> "
			, v
			, "</span>"
			]
