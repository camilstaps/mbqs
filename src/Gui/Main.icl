implementation module Gui.Main

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Monad => qualified return
import Data.Func
import Data.Functor
import qualified Data.Map
import Data.Map.GenJSON
import System.OS
import qualified Text
from Text import class Text(endsWith), instance Text String, concat5
import Text.HTML

import ABC.Interpreter
import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

import iTasks => qualified :: Menu, forever, sequence, SelectAll
import iTasks.Extensions.DateTime
import iTasks.WF.Tasks.System

import iTasks.Extensions.CssDoodle

import Electron.App
import Electron.Dialog
import Electron.Menu => qualified applicationMenu
import Electron.Util

import TextFabric

import MBQS.Languages.Parsers
import MBQS.Model
import MBQS.Model.Serialization

import qualified Gui.CSV
import Gui.Derives
import Gui.NewSheet
import Gui.Settings
import Gui.Shares
import Gui.Sheet.UpgradeDataVersion
import Gui.Util
import Gui.Window

derive class iTask ApplicationMenu

fullApplicationMenu :: ApplicationMenu
fullApplicationMenu =
	{ file_save                      = True
	, file_save_as                   = True
	, file_export_csv                = True
	, edit_add_column                = True
	, edit_add_disambiguating_column = True
	, edit_rename_column             = True
	, edit_delete_column             = True
	, edit_add_row                   = True
	, edit_delete_row                = True
	, edit_find                      = True
	, edit_cancel_search             = True
	, edit_trim_by_search            = True
	, edit_toggle_tag                = True
	}

welcomeViewApplicationMenu :: ApplicationMenu
welcomeViewApplicationMenu =
	{ file_save                      = False
	, file_save_as                   = False
	, file_export_csv                = False
	, edit_add_column                = False
	, edit_add_disambiguating_column = False
	, edit_rename_column             = False
	, edit_delete_column             = False
	, edit_add_row                   = False
	, edit_delete_row                = False
	, edit_find                      = False
	, edit_cancel_search             = False
	, edit_trim_by_search            = False
	, edit_toggle_tag                = False
	}

applicationMenu :: SDSLens () () ApplicationMenu
applicationMenu =: mapReadWrite
	( \_ -> ()
	, \menu _ -> ?Just (?Just (toMenu menu))
	)
	?None
	'Electron.Menu'.applicationMenu
where
	toMenu menu =
		[ menuItem "File" <<@ SubMenu
			[ menuItem "New" <<@ Accelerator "CmdOrCtrl+N" <<@ OnClick \_ -> newFile
			, menuItem "Open" <<@ Accelerator "CmdOrCtrl+O" <<@ OnClick \_ -> openFile
			// TODO: save cell currently being edited when Save(As) is activated
			, menuItem "Save" <<@ Accelerator "CmdOrCtrl+S" <<@ eord menu.file_save <<@ OnClick saveFile
			, menuItem "Save as" <<@ Accelerator "Shift+CmdOrCtrl+S" <<@ eord menu.file_save_as <<@ OnClick saveFileAs
			, menuItem "Export as CSV" <<@ eord menu.file_export_csv <<@ OnClick exportFileAsCSV
			, MenuSeperator
			, menuItem "Upgrade sheet data version" <<@ OnClick \_ -> upgradeSheetDataVersion
			, MenuSeperator
			, menuItem "Settings" <<@ OnClick settingsDialog
			, MenuSeperator
			, menuItem "Close window" <<@ Accelerator "CmdOrCtrl+W" <<@ OnClick closeOrConfirm
			, menuItem "Quit" <<@ Accelerator "CmdOrCtrl+Q" <<@ OnClick \_ -> closeOrConfirmAllWindows
			]
		, menuItem "Edit" <<@ SubMenu
			[ menuItem "Add column" <<@ eord menu.edit_add_column <<@ OnClick addSheetColumn
			, menuItem "Add disambiguating column" <<@ eord menu.edit_add_disambiguating_column <<@ OnClick addDisambiguatingSheetColumn
			, menuItem "Rename column" <<@ eord menu.edit_rename_column <<@ OnClick renameSheetColumn
			, menuItem "Delete column" <<@ eord menu.edit_delete_column <<@ OnClick deleteSheetColumn
			, MenuSeperator
			, menuItem "Add row(s)" <<@ eord menu.edit_add_row <<@ OnClick addSheetRows
			, menuItem "Delete row" <<@ eord menu.edit_delete_row <<@ OnClick deleteSheetRow
			, MenuSeperator
			, menuItem "Find" <<@ Accelerator "CmdOrCtrl+F" <<@ eord menu.edit_find <<@ OnClick focusSearchBar
			, menuItem "Cancel search" <<@ Accelerator "CmdOrCtrl+D" <<@ eord menu.edit_cancel_search <<@ OnClick cancelSearch
			, menuItem "Trim sheet by search" <<@ eord menu.edit_trim_by_search <<@ OnClick trimBySearch
			, MenuSeperator
			, menuItem "Toggle tag" <<@ eord menu.edit_toggle_tag <<@ SubMenu
				[ menuItem ("Tag " +++ toString i)
					<<@ Accelerator ("CmdOrCtrl+" +++ toString i)
					<<@ eord menu.edit_toggle_tag
					<<@ OnClick (toggleTag i)
				\\ i <- [1..9]
				]
			]
		, menuItem "View" <<@ SubMenu
			[ menuItem "Reset zoom" <<@ ResetZoom
			, menuItem "Zoom in" <<@ ZoomIn
			, menuItem "Zoom out" <<@ ZoomOut
			]
		, menuItem "Help" <<@ SubMenu
			[ menuItem "Open transcription table" <<@ OnClick \_ -> openURLExternally transcription_table
			, menuItem "Open feature documentation" <<@ OnClick \_ -> openURLExternally feature_docs
			, MenuSeperator
			, menuItem "About Məḇaqqēš" <<@ OnClick aboutDialog
			, menuItem "License details" <<@ OnClick licenseDetailsDialog
			]
		, menuItem "Tools" <<@ SubMenu
			[ menuItem "Open development tools" <<@ Accelerator "Shift+CmdOrCtrl+I" <<@ ToggleDevTools
			]
		]
	
	eord b = if b Enabled Disabled

	transcription_table = "https://annotation.github.io/text-fabric/writing/hebrew.html"
	feature_docs = "https://etcbc.github.io/bhsa/features/0_home/"

welcomeWindow :: SimpleSDSLens (?ElectronWindow)
welcomeWindow =: sdsFocus "welcomeWindow" (memoryStore "mbqs" (?Just ?None))

mbCloseWelcomeWindow :: Task ()
mbCloseWelcomeWindow =
	get welcomeWindow >>- \mbWin -> case mbWin of
		?None ->
			return ()
		?Just win ->
			set ?None welcomeWindow >-|
			closeOrConfirm win

mainApp :: Task ()
mainApp =
	set welcomeViewApplicationMenu applicationMenu >-|
	appendTopLevelTask 'Data.Map'.newMap True
		(onChange currentWindow \win ->
			case win of
				?Just win ->
					get (sdsFocus win windows) >>- \window ->
					case window of
						?Just {menu} ->
							set menu applicationMenu
						?None ->
							set welcomeViewApplicationMenu applicationMenu
				?None ->
					set welcomeViewApplicationMenu applicationMenu) >-|

	getElectronCommandLine >>- \cmd -> case cmd of
		[_,_:paths] | not (isEmpty paths) ->
			allTasks [exceptionsToWarnings (openInNewWindow path) \\ path <- paths] @! ()
		_ ->
			createWindow {ElectronWindowOptions | defaultOptions & task = startup} >>- \win ->
			set (?Just win) welcomeWindow @!
			()
where
	startup _ = Title "Məḇaqqēš" @>> ArrangeHorizontal @>>
		(
			(viewInformation [] background <<@ classAttr ["welcome-background"])
		-&&-
			(viewInformation [] welcome <<@ classAttr ["welcome-view"])
		)

	background = CssDoodle
		[ ":doodle" hasProperties
			[ "@grid" is "15 / 100%"
			, "color" is "#999"
			, "font-weight" is "bold"
			]
		, ":doodle(:hover)" hasProperties
			[ "--s" is "1"
			]
		, ":host" hasProperties
			[ "overflow" is "hidden"
			, "height" is "100%"
			, "width" is "100%"
			]
		, ":after" hasProperties
			[ "content" is "@pick(א,ב,ג,ד,ה,ו,ז,ח,ט,י,כ,ל,מ,נ,ס,ע,פ,צ,ק,ר,ש)"
			, "font-family" is "'SBL Hebrew'"
			, "font-size" is "4vmax"
			, "transform" is "scale(@rand(.2, .9))"
			]
		]
		[ "transition" is ".5s cubic-bezier(.175, .885, .32, 1.275)"
		, "transition-delay" is "@rand(650ms)"
		, "transform" is "translateY(calc(var(--s) * 20%)) rotate(calc(var(--s) * 360deg))"
		]

	welcome = DivTag []
		[ H1Tag [] [Text "Welcome to ", EmTag [] [Text "Məḇaqqēš"], Text "!"]
		, PTag [] [Text "To start, do one of the following:"]
		, UlTag []
			[ LiTag [] [Html "<kbd>File</kbd> &rarr; <kbd>New</kbd> to create a new sheet"]
			, LiTag [] [Html "<kbd>File</kbd> &rarr; <kbd>Open</kbd> to open an existing sheet"]
			]
		]

settingsDialog :: !ElectronWindow -> Task ()
settingsDialog parent =
	createWindow
		{ defaultOptions
		& task           = edit
		, parent         = ?Just parent
		, width          = 600
		, height         = 400
		} @!
	()
where
	edit _ = tune (Title "Settings") $
		editSettings >>*
		[ OnAction ActionOk (hasValue \settings -> saveSettings settings >-| closeWindow)
		]

newFile :: Task ()
newFile =
	showMessageBox
		QuestionMsg "Choose sheet type" "Do you want to create an empty sheet or pre-fill it with a query?"
		["Use an empty sheet", "Prefill the sheet with a query"] 0 >>- \answer ->
	case answer of
		?Just "Prefill the sheet with a query" ->
			createWindow
				{ ElectronWindowOptions
				| defaultOptions
				& task = \_ -> createNewSheet >>- saveAndOpen
				} >-|
			mbCloseWelcomeWindow
		_ ->
			saveAndOpen emptySheet >-| mbCloseWelcomeWindow
where
	emptySheet =
		{ SheetSettings | defaultSheetSettings
		& nodes =
			[ {NodeSettings | name="verse", type="verse", optional= ?None, filterDescription=FALSE}
			]
		, columns =
			[ {title="Book", css_class= ?None, expression= ?Just (Feature "verse" "book")}
			, {title="Chapter", css_class= ?None, expression= ?Just (Feature "verse" "chapter")}
			, {title="Verse", css_class= ?None, expression= ?Just (Feature "verse" "verse")}
			, {title="Notes", css_class= ?None, expression= ?None}
			]
		, text_panel =
			{ text_node = ?Just "verse"
			, colored_nodes = []
			}
		}
	saveAndOpen settings =
		exceptionsToWarnings $
		save settings >>- \mbPath -> case mbPath of
			?Just path ->
				openInNewWindow path >-| closeWindow
			?None ->
				closeWindow
	save settings =
		getFilePathToSave >>- \mbPath -> case mbPath of
			?None ->
				showMessageBox WarningMsg "File path required"
					"You must enter a file path to store the sheet. Cancelling will lose your settings."
					["&Cancel", "&Enter file path"]
					1 >>-
				\answer -> case answer of
					?Just "&Enter file path" ->
						save settings
					_ ->
						return ?None
			?Just file_path ->
				enterInformation [EnterUsing id (mapEditorWrite ValidEditor loader)] ||-
				// NB: ugly hack: waitForTimer is needed so that the UILoader is shown
				(waitForTimer False 1 >-| getDataSet (requiredFeatures settings)) >>- \data ->
				case getValues settings data of
					Error e ->
						throw ("Failed to import data: "+++e)
					Ok values ->
						appWorld (\w
							# (_,f,w) = fopen file_path FWriteData w
							# f = writeSheet settings values f
							# (_,w) = fclose f w
							-> w) @!
						(?Just file_path)

openFile :: Task ()
openFile =
	showOpenDialog "Select a file to open"
		{OpenDialogSettings | defaultOpenDialogSettings & filters = FILE_EXTENSIONS} >>-
	\mbFilePath -> case mbFilePath of
		?None ->
			return ()
		?Just file_path ->
			exceptionsToWarnings (openInNewWindow file_path >-| mbCloseWelcomeWindow) @! ()

saveFile :: !ElectronWindow -> Task ()
saveFile win = exceptionsToWarnings (save False win) @! ()

saveFileAs :: !ElectronWindow -> Task ()
saveFileAs win =
	getFilePathToSave >>- \mbPath -> case mbPath of
		?None ->
			return ()
		?Just path ->
			upd (fmap \w -> {w & file_path= ?Just path}) (sdsFocus win windows) >-|
			saveFile win

getFilePathToSave :: Task (?FilePath)
getFilePathToSave = showSaveDialog "Save as..."
	{SaveDialogSettings | defaultSaveDialogSettings & filters = FILE_EXTENSIONS}

exportFileAsCSV :: !ElectronWindow -> Task ()
exportFileAsCSV win =
	get (sdsFocus win windows) >>- \(?Just {file_path=sheet_path}) ->
	showSaveDialog
		"Export as..."
		{ SaveDialogSettings
		| defaultSaveDialogSettings
		& filters = [("CSV", ["csv"])]
		, defaultPath = setExtension <$> sheet_path
		} >>- \mbPath ->
	case mbPath of
		?None ->
			return ()
		?Just path ->
			exceptionsToWarnings ('Gui.CSV'.export path win) @! ()
where
	setExtension path
		| endsWith ".mbqs" path
			= path % (0, size path-5) +++ "csv"
			= path +++ ".csv"

upgradeSheetDataVersion :: Task ()
upgradeSheetDataVersion =
	createWindow
		{ ElectronWindowOptions
		| defaultOptions
		& task = upgradeSheet
		} @!
	()

closeOrConfirm :: !ElectronWindow -> Task ()
closeOrConfirm win=:(ElectronWindow id) =
	get (sdsFocus win windows) >>- \mbWin -> case mbWin of
		?Just _ -> /* This is a sheet window */
			exceptionsToWarnings (close win) @! ()
		?None -> /* This is a welcome window; just close it */
			runInMainProcess \me w
				# (win,w) = (me .# "windows.get" .$ id) w
				# w = (win .# "close" .$! ()) w
				-> w

closeOrConfirmAllWindows :: Task ()
closeOrConfirmAllWindows =
	get allWindows >>- \wins ->
	allTasks (map closeOrConfirm wins) @!
	()

addSheetColumn :: !ElectronWindow -> Task ()
addSheetColumn win = exceptionsToWarnings (addColumn win) @! ()

addDisambiguatingSheetColumn :: !ElectronWindow -> Task ()
addDisambiguatingSheetColumn win = exceptionsToWarnings (addDisambiguatingColumn win) @! ()

renameSheetColumn :: !ElectronWindow -> Task ()
renameSheetColumn win = exceptionsToWarnings (renameColumn win) @! ()

deleteSheetColumn :: !ElectronWindow -> Task ()
deleteSheetColumn win = exceptionsToWarnings (deleteColumn win) @! ()

addSheetRows :: !ElectronWindow -> Task ()
addSheetRows win = exceptionsToWarnings (addRows win) @! ()

deleteSheetRow :: !ElectronWindow -> Task ()
deleteSheetRow win = exceptionsToWarnings (deleteRow win) @! ()

aboutDialog :: !ElectronWindow -> Task ()
aboutDialog parent =
	createWindow
		{ defaultOptions
		& task           = view
		, parent         = ?Just parent
		, width          = 600
		, height         = 400
		} @!
	()
where
	view _ =
		getVersion >>- \version ->
		viewInformation [] (about version) >>*
		[ OnAction ActionClose $ always closeWindow
		]
	about version = DivTag []
		[ H1Tag []
			[ EmTag [] [Text "Məḇaqqēš"]
			, Text (concat5 " " version " (" (IF_INT_64_OR_32 "64-bit" "32-bit") ")")
			]
		, PTag []
			[ EmTag [] [Text "Məḇaqqēš"]
			, Text " is a spreadsheet application for scholars of the Hebrew Bible and Biblical Hebrew."
			]
		, PTag []
			[ Text "Check "
			, ATag [HrefAttr "https://gitlab.com/camilstaps/mbqs/-/tags"] [Text "gitlab.com/camilstaps/mbqs/tags"]
			, Text " to see whether an update is available."
			]
		, PTag []
			[ Text "Use "
			, ATag [HrefAttr "https://gitlab.com/camilstaps/mbqs/-/issues"] [Text "gitlab.com/camilstaps/mbqs/issues"]
			, Text " to file bugs or ask for support, or drop the author an email."
			]
		, PTag []
			[ EmTag [] [Text "Məḇaqqēš"]
			, Text " is open source software, licensed under AGPLv3. "
			, Text "The Hebrew data and the English translation do not fall under this license. "
			, Text "See "
			, ATag [HrefAttr "https://gitlab.com/camilstaps/mbqs"] [Text "gitlab.com/camilstaps/mbqs"]
			, Text " for the source code and to contribute. "
			]
		]

licenseDetailsDialog :: !ElectronWindow -> Task ()
licenseDetailsDialog parent =
	get applicationURL >>- \serverUrl ->
	createWindow
		{ defaultOptions
		& task           = \_ -> loadURL (serverUrl +++ "licenses.html")
		, parent         = ?Just parent
		, width          = 800
		, height         = 600
		} @!
	()
