definition module Gui.Settings

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.GenEq import generic gEq
from System.FilePath import :: FilePath
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import :: Task, class iTask

:: Settings =
	{ tf_cache         :: !?FilePath
	, tf_path          :: !?FilePath
	, english_bible_db :: !?FilePath
	, english_bible_abbreviation :: !?String
	, english_bible_copyright :: !?String
	}

derive class iTask Settings

getSettings :: Task Settings
saveSettings :: !Settings -> Task ()

/**
 * Provides an editor for the current `Settings`. This task does not actually
 * save the settings; this must be done by `saveSettings`.
 */
editSettings :: Task Settings
