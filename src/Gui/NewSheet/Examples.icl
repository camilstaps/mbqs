implementation module Gui.NewSheet.Examples

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Text import concat3

import iTasks

import MBQS.Model

import Gui.NewSheet

derive class iTask \ gText Example

gText{|Example|} AsSingleLine (?Just {Example|name,description}) = [concat3 name ": " description]
gText{|Example|} AsMultiLine (?Just {Example|name,description}) = [name+++":", description]
gText{|Example|} AsHeader _ = ["Name", "Description"]
gText{|Example|} AsRow (?Just {Example|name,description}) = [name, description]

examples :: [Example]
examples =
	[ nifals
	, yhwh_as_subject
	, lamed_in_genesis
	, israel
	, pnh_with_prep
	, ntn_with_waw
	]

Hebrew :== ?Just "hebrew"

nifals =
	{ name = "Nifals"
	, description = "All nifals with morphological information: root, tense, PGN, gloss. There is also an editable column for notes."
	, example =
		{ SheetSettingsInput
		| nodes     = nodes
		, columns   = columns
		, text_node = "verse"
		}
	}
where
	nodes =
		[ {name="verb",  type="word",  optional=False, filterDescription=FeatureOp "vs" (Eq "nif"), color= ?Just "#c80000"}
		, {name="lex",   type="lex",   optional=False, filterDescription=ParentOf "verb",           color= ?Just "#eebf09"}
		, {name="verse", type="verse", optional=False, filterDescription=ParentOf "verb",           color= ?None}
		]
	columns =
		[ {title="Book",    css_class= ?None, expression= ?Just (Feature "verse" "book")}
		, {title="Chapter", css_class= ?None, expression= ?Just (Feature "verse" "chapter")}
		, {title="Verse",   css_class= ?None, expression= ?Just (Feature "verse" "verse")}
		, {title="Form",    css_class=Hebrew, expression= ?Just (Feature "verb" "g_word_utf8")}
		, {title="Root",    css_class=Hebrew, expression= ?Just (Feature "lex" "voc_lex_utf8")}
		, {title="Tense",   css_class= ?None, expression= ?Just (Feature "verb" "vt")}
		, {title="P",       css_class= ?None, expression= ?Just (Feature "verb" "ps")}
		, {title="G",       css_class= ?None, expression= ?Just (Feature "verb" "gn")}
		, {title="N",       css_class= ?None, expression= ?Just (Feature "verb" "nu")}
		, {title="Gloss",   css_class= ?None, expression= ?Just (Feature "verb" "gloss")}
		, {title="Notes",   css_class= ?None, expression= ?None}
		]

yhwh_as_subject =
	{ name = "YHWH as subject"
	, description = "All clauses with YHWH as the subject."
	, example =
		{ SheetSettingsInput
		| nodes     = nodes
		, columns   = columns
		, text_node = "verse"
		}
	}
where
	nodes =
		[ {name="yhwh",    type="word",   optional=False, filterDescription=FeatureOp "lex" (Eq "JHWH/"), color= ?None}
		, {name="subject", type="phrase", optional=False, filterDescription=AND (FeatureOp "function" (Eq "Subj")) (ParentOf "yhwh"), color= ?Just "#c80000"}
		, {name="verb",    type="word",   optional=False, filterDescription=AND (FeatureOp "sp" (Eq "verb")) (ChildOf "clause"), color= ?Just "#eebf09"}
		, {name="clause",  type="clause", optional=False, filterDescription=ParentOf "subject",           color= ?None}
		, {name="verse",   type="verse",  optional=False, filterDescription=ParentOf "yhwh",              color= ?None}
		]
	columns =
		[ {title="Book",    css_class= ?None, expression= ?Just (Feature "verse" "book")}
		, {title="Chapter", css_class= ?None, expression= ?Just (Feature "verse" "chapter")}
		, {title="Verse",   css_class= ?None, expression= ?Just (Feature "verse" "verse")}
		, {title="Verb",    css_class=Hebrew, expression= ?Just (Feature "verb" "g_word_utf8")}
		, {title="Root",    css_class=Hebrew, expression= ?Just (Feature "verb" "lex_utf8")}
		, {title="Notes",   css_class= ?None, expression= ?None}
		]

lamed_in_genesis =
	{ name = "Lamed in Genesis"
	, description = "All instances of the preposition lamed followed by a substantive in Genesis."
	, example =
		{ SheetSettingsInput
		| nodes     = nodes
		, columns   = columns
		, text_node = "verse"
		}
	}
where
	nodes =
		[ {name="lamed", type="word",  optional=False, filterDescription=FeatureOp "lex" (Eq "L"), color= ?Just "#eebf09"}
		, {name="noun",  type="word",  optional=False, filterDescription=AND (FeatureOp "pdp" (Eq "subs")) (DistanceInterval "lamed" 0 2), color= ?Just "#c80000"}
		, {name="verse", type="verse", optional=False, filterDescription=AND (ParentOf "lamed") (FeatureOp "book" (Eq "Genesis")), color= ?None}
		]
	columns =
		[ {title="Book",    css_class= ?None, expression= ?Just (Feature "verse" "book")}
		, {title="Chapter", css_class= ?None, expression= ?Just (Feature "verse" "chapter")}
		, {title="Verse",   css_class= ?None, expression= ?Just (Feature "verse" "verse")}
		, {title="Noun",    css_class=Hebrew, expression= ?Just (Feature "noun" "g_word_utf8")}
		, {title="Lexeme",  css_class=Hebrew, expression= ?Just (Feature "noun" "g_lex_utf8")}
		, {title="Notes",   css_class= ?None, expression= ?None}
		]

israel =
	{ name = "Israel"
	, description = "All cases of the word \"Israel\"."
	, example =
		{ SheetSettingsInput
		| nodes     = nodes
		, columns   = columns
		, text_node = "verse"
		}
	}
where
	nodes =
		[ {name="israel", type="word",   optional=False, filterDescription=FeatureOp "lex" (Eq "JFR>L/"), color= ?Just "#c80000"}
		, {name="clause", type="clause", optional=False, filterDescription=ParentOf "israel",             color= ?Just "#eebf09"}
		, {name="phrase", type="phrase", optional=False, filterDescription=ParentOf "israel",             color= ?Just "#ea9804"}
		, {name="verse",  type="verse",  optional=False, filterDescription=ParentOf "israel",             color= ?None}
		]
	columns =
		[ {title="Book",     css_class= ?None, expression= ?Just (Feature "verse" "book")}
		, {title="Chapter",  css_class= ?None, expression= ?Just (Feature "verse" "chapter")}
		, {title="Verse",    css_class= ?None, expression= ?Just (Feature "verse" "verse")}
		, {title="Function", css_class=Hebrew, expression= ?Just (Feature "phrase" "function")}
		, {title="Notes",    css_class= ?None, expression= ?None}
		]

pnh_with_prep =
	{ name = "Pānim with prepositions"
	, description = "All cases of the word pānim \"face\", with preceding preposition when available."
	, example =
		{ SheetSettingsInput
		| nodes     = nodes
		, columns   = columns
		, text_node = "verse"
		}
	}
where
	nodes =
		[ {name="pnh",   type="word",  optional=False, filterDescription=FeatureOp "lex" (Eq "PNH/"), color= ?Just "#c80000"}
		, {name="prep",  type="word",  optional=True,  filterDescription=AND (DistanceInterval "pnh" -2 0) (FeatureOp "pdp" (Eq "prep")), color= ?Just "#eebf09"}
		, {name="verse", type="verse", optional=False, filterDescription=ParentOf "pnh", color= ?None}
		]
	columns =
		[ {title="Book",        css_class= ?None, expression= ?Just (Feature "verse" "book")}
		, {title="Chapter",     css_class= ?None, expression= ?Just (Feature "verse" "chapter")}
		, {title="Verse",       css_class= ?None, expression= ?Just (Feature "verse" "verse")}
		, {title="Form",        css_class=Hebrew, expression= ?Just (Feature "pnh" "g_word_utf8")}
		, {title="Preposition", css_class=Hebrew, expression= ?Just (Feature "prep" "g_lex_utf8")}
		, {title="Notes",    css_class= ?None, expression= ?None}
		]

ntn_with_waw =
	{ name = "ntn with clause starting with waw (not wayyiqṭol)"
	, description = "The verb ntn \"give\" with a clause starting with the conjunction waw, to find cases with an object clause as in Ruth 1:9."
	, example =
		{ SheetSettingsInput
		| nodes     = nodes
		, columns   = columns
		, text_node = "v2"
		}
	}
where
	nodes =
		[ { name = "ntn"
		  , type = "word"
		  , optional = False
		  , filterDescription = FeatureOp "lex" (Eq "NTN[")
		  , color = ?Just "red"
		  }
		, { name = "w"
		  , type = "word"
		  , optional = False
		  , filterDescription = AND (DistanceInterval "c1" 0 2) (AND (ChildOf "c2") (FeatureOp "lex" (Eq "W")))
		  , color = ?Just "red"
		  }
		, { name = "obj"
		  , type = "phrase"
		  , optional = True
		  , filterDescription = AND (ChildOf "c1") (FeatureOp "function" (Eq "Objc"))
		  , color = ?Just "brown"
		  }
		, { name = "c1"
		  , type = "clause_atom"
		  , optional = False
		  , filterDescription = ParentOf "ntn"
		  , color = ?Just "orange"
		  }
		, { name = "c2"
		  , type = "clause_atom"
		  , optional = False
		  , filterDescription = DistanceInterval "c1" 0 2
		  , color = ?Just "orange"
		  }
		, { name = "notwayq"
		  , type = "word"
		  , optional = False
		  , filterDescription = AND (DistanceInterval "w" 0 2) (FeatureOp "vt" (Ne "wayq"))
		  , color = ?None
		  }
		, { name = "v2"
		  , type = "verse"
		  , optional = False
		  , filterDescription = ParentOf "w"
		  , color = ?None
		  }
		]
	columns =
		[ { title = "Book"
		  , css_class = ?None
		  , expression = ?Just (Feature "v2" "book")
		  }
		, { title = "Chapter"
		  , css_class = ?None
		  , expression = ?Just (Feature "v2" "chapter")
		  }
		, { title = "Verse"
		  , css_class = ?None
		  , expression = ?Just (Feature "v2" "verse")
		  }
		, { title = "Stem"
		  , css_class = ?None
		  , expression = ?Just (Feature "ntn" "vs")
		  }
		, { title = "Tense"
		  , css_class = ?None
		  , expression = ?Just (Feature "ntn" "vt")
		  }
		, { title = "Suffix"
		  , css_class = ?Just "hebrew"
		  , expression = ?Just (Feature "ntn" "g_prs_utf8")
		  }
		, { title = "HasObject"
		  , css_class = ?None
		  , expression = ?Just (Feature "obj" "function")
		  }
		, { title = "Notes"
		  , css_class = ?None
		  , expression = ?None
		  }
		]
