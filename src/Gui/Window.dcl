definition module Gui.Window

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.GenEq import generic gEq
from Data.Map import :: Map
from System.FilePath import :: FilePath
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.SDS.Definition import :: SimpleSDSLens, :: SDSLens
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import :: Task, :: TaskId, class iTask

from iTasks.Extensions.SearchField import :: Query

from Electron.App import :: ElectronWindow

from Gui.Main import :: ApplicationMenu
from Gui.Sheet import :: Sheet

:: MBQSWindow =
	{ file_path    :: !?FilePath
	, menu         :: !ApplicationMenu
	, sheet        :: !?Sheet
	, sheet_task   :: !TaskId
	, search_field :: !Query
	}

derive class iTask MBQSWindow

windows :: SDSLens ElectronWindow (?MBQSWindow) (?MBQSWindow)

openInNewWindow :: !FilePath -> Task ElectronWindow

addColumn :: !ElectronWindow -> Task ()
addDisambiguatingColumn :: !ElectronWindow -> Task ()
renameColumn :: !ElectronWindow -> Task ()
deleteColumn :: !ElectronWindow -> Task ()

addRows :: !ElectronWindow -> Task ()
deleteRow :: !ElectronWindow -> Task ()

save :: !Bool !ElectronWindow -> Task ()

close :: !ElectronWindow -> Task ()

focusSearchBar :: !ElectronWindow -> Task ()
cancelSearch :: !ElectronWindow -> Task ()
trimBySearch :: !ElectronWindow -> Task ()

toggleTag :: !Int !ElectronWindow -> Task ()
