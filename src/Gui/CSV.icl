implementation module Gui.CSV

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Text.CSV

import iTasks

import Electron.App
import Electron.Dialog

from Gui.Sheet import :: Sheet{settings,values}
import Gui.Window
import MBQS.Model

export :: !FilePath !ElectronWindow -> Task ()
export path win =
	get (sdsFocus win windows) >>- \win -> case win of
	?None ->
		throw "Illegal window passed to export"
	?Just {sheet= ?None} ->
		throw "This window contains no sheet"
	?Just {sheet= ?Just {settings,values}} ->
		accWorldError (export` settings values path) id >-|
		showSimpleMessageBox InfoMsg "Sheet exported successfully"

export` :: !SheetSettings !SheetValues !FilePath !*World -> (!MaybeError String (), !*World)
export` {SheetSettings|columns} {node_names,entries} path w
	# (ok,f,w) = fopen path FWriteText w
	| not ok = (Error ("Failed to open " +++ path), w)
	# f = writeCSVFileWith ',' '"' '"' csv f
	# (ok,w) = fclose f w
	| not ok = (Error ("Failed to close " +++ path), w)
	| otherwise = (Ok (), w)
where
	csv =
		[
			[name \\ name <-: node_names] ++
			[title \\ {ColumnSettings|title} <- columns]
		:
			[ [toString n \\ n <-: nodes] ++ [c \\ c <-: columns]
			\\ {SheetEntry|nodes,columns} <-: entries
			]
		]
