definition module Gui.Util

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.GenEq import generic gEq
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.SDS.Definition import :: SimpleSDSLens, :: SDSLens,
	class Identifiable, class Readable, class Registrable, class Modifiable,
	class Writeable, class RWShared
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.UI.Tune import class tune
from iTasks.WF.Combinators.Common import @!
from iTasks.WF.Definition import :: Task, :: TaskId, class iTask

//* For use in open/save dialogs.
FILE_EXTENSIONS :==
	[ ("MBQS sheet (*.mbqs)", ["mbqs"])
	, ("All files", ["*"])
	]

:: WithLoader = WithLoader

instance tune WithLoader (Task a)

share :: !String a -> SimpleSDSLens a | JSONEncode{|*|}, JSONDecode{|*|}, TC a
persistentShare :: !String a -> SimpleSDSLens a | JSONEncode{|*|}, JSONDecode{|*|}, TC a

onChange :: !(sds () r w) !(r -> Task a) -> Task a | iTask r & iTask a & RWShared sds & TC w

closeOnException :: !(Task a) -> Task () | iTask a
exceptionsToErrors :: !(Task a) -> Task (?a) | iTask a
exceptionsToWarnings :: !(Task a) -> Task (?a) | iTask a

waitForEmptyTCPQueue :: Task ()

voidTask t :== t @! ()

withStoredTaskId :: !(SimpleSDSLens TaskId) !(Task a) -> Task a
