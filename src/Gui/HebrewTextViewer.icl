implementation module Gui.HebrewTextViewer

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Func
from Data.List import find
from Text import concat3
import Text.HTML

import iTasks

import TextFabric
import TextFabric.BHSA

import Gui.Derives
import Gui.Shares

viewHebrewWithMorphologyPopups :: (sds () (?(NodeRef, [(NodeRef, String)])) w) -> Task () | RWShared sds & TC w
viewHebrewWithMorphologyPopups node_and_colored_nodes =
	withShared ?None \info_node ->
	(
		forever (
			whileUnchanged node_and_colored_nodes \_ -> // TODO: should not be needed, but without this updateSharedInformation won't update frequently enough
			updateSharedInformation [UpdateSharedUsing id (const id) hebrewTextViewer] (node_and_colored_nodes |*< data_set |*< info_node)) -||
		forever (
			watch info_node >>*
			[ OnValue (ifValue isJust (\_ ->
				Title "Morphology" @>>
				sizeAttr WrapSize WrapSize @>>
				("resizable", JSONBool False) @>>
				ToWindow FloatingWindow AlignMiddle AlignCenter @>>
				viewNodeInfo info_node))
			]
		)
	) @! ()
where
	viewNodeInfo info_node =
		viewSharedInformation [ViewUsing id nodeInfoViewer] (info_node >*< data_set) >>*
		[ OnAction ActionClose (always (set ?None info_node))
		]

:: NodeInfo =
	{ lexeme       :: !HebrewString
	, gloss        :: !String
	, gender       :: !String
	, number       :: !String
	, person       :: !String
	, verbal_stem  :: !String
	, verbal_tense :: !String
	}

:: HebrewString =: HebrewString String

:: HebrewTextEditEvent
	= AddVerseStart
	| AddVerseEnd
	| OpenNodeInfo !NodeRef

derive class iTask NodeInfo, HebrewTextEditEvent
derive class iTask \ gEditor HebrewString

gEditor{|HebrewString|} purpose = classAttr ["hebrew"] @>> gEditor purpose
where
	derive gEditor HebrewString

hebrewTextViewer :: Editor ((?(NodeRef, [(NodeRef, String)]), ?DataSet), ?NodeRef) (EditorReport (?NodeRef))
hebrewTextViewer = mapEditorWithState (0,0) modifyRead modifyWrite htmlViewWithCustomEditEvents
where
	modifyRead ((?Just (node,colored_nodes),?Just data),_) state = (?Just html, state`)
	where
		html = DivTag
			[ClassAttr "hebrew large"]
			(if first_verse [] [ellipsis True, Html "&nbsp;"] ++
				[ SpanTag
					(mbStyleAttr ++
						maybe [] (\ref -> [ClassAttr "hebrew-with-morphology", OnclickAttr (toHtmlEventCall (OpenNodeInfo ref))]) mbRef)
					[Text word]
				\\ n <- [fst state`..snd state`]
				, (mbRef,word) <- if (node == -1) [] (get_text n data)
				, let mbStyleAttr = if (n <> node)
						[StyleAttr "color:gray;"]
						(case mbRef of
							?None -> []
							?Just ref -> case find (\(ancestor,_) -> ref == ancestor || is_ancestor_of ref ancestor data) colored_nodes of
								?None -> []
								?Just (_,color) -> [StyleAttr (concat3 "color:" color ";")])
				] ++
				if last_verse [] [ellipsis False])
		where
			ellipsis start_not_end = SpanTag
				[ OnclickAttr (toHtmlEventCall (if start_not_end AddVerseStart AddVerseEnd))
				, StyleAttr "cursor:pointer;"
				]
				[Html " &#8230;"]

			first_verse = let node = fst state`-1 in node < 0 || get_node_feature 0 data.nodes.[node] <> "verse"
			last_verse = let node = snd state` + 1 in size data.nodes <= node || get_node_feature 0 data.nodes.[node] <> "verse"

		state` = if (fst state == 0) (node-1, node+1) state
	modifyRead _ state = (?Just (Text ""), state)

	modifyWrite ?None state = (ValidEditor ?None, state)
	modifyWrite (?Just AddVerseStart) (start,end) = (ValidEditor ?None, (start-1, end))
	modifyWrite (?Just AddVerseEnd) (start,end) = (ValidEditor ?None, (start, end+1))
	modifyWrite (?Just (OpenNodeInfo node)) state = (ValidEditor (?Just node), state)

nodeInfoViewer :: Editor (?NodeRef, ?DataSet) ()
nodeInfoViewer =
	mapEditorWrite (const ()) $
	mapEditorRead toNodeInfo $
	gEditor{|*|} ViewValue
where
	toNodeInfo (?Just node_ref,?Just data) = ?Just
		{ lexeme = HebrewString (get_node_feature lex_utf8 node)
		, gloss = get_node_feature gloss node
		, gender = get_node_feature gn node
		, number = get_node_feature nu node
		, person = get_node_feature ps node
		, verbal_stem = get_node_feature vs node
		, verbal_tense = get_node_feature vt node
		}
	where
		node = data.DataSet`.nodes.[node_ref]
		(?Just [lex_utf8,gloss,gn,nu,ps,vs,vt:_]) = get_node_feature_ids ["lex_utf8","gloss","gn","nu","ps","vs","vt"] data
	toNodeInfo (_,_) = ?None
