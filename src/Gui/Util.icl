implementation module Gui.Util

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import Data.Func

import iTasks
import iTasks.Internal.TaskEval
import iTasks.WF.Combinators.Core

import Electron.App
import Electron.Dialog

import Gui.Main

instance tune WithLoader (Task a)
where
	tune WithLoader task = ApplyLayout (setUIType UILoader) @>> task

share :: !String a -> SimpleSDSLens a | JSONEncode{|*|}, JSONDecode{|*|}, TC a
share name default = sdsFocus name (memoryStore "MBQS" (?Just default))

persistentShare :: !String a -> SimpleSDSLens a | JSONEncode{|*|}, JSONDecode{|*|}, TC a
persistentShare name default = sdsFocus (name +++ ".json") (jsonFileStore "MBQS" False False (?Just default))

onChange :: !(sds () r w) !(r -> Task a) -> Task a | iTask r & iTask a & RWShared sds & TC w
onChange sds f = whileUnchanged sds \v -> f v @? toUnstable
where
	toUnstable v = case v of
		Value v _
			-> Value v False
			-> v

closeOnException :: !(Task a) -> Task () | iTask a
closeOnException task =
	catchAll
		(task @! ())
		(\e ->
			WithLoader @>> showMessageBox ErrorMsg "Error" e ["Ok"] 0 >-|
			closeWindow)

exceptionsToErrors :: !(Task a) -> Task (?a) | iTask a
exceptionsToErrors task =
	catchAll
		(task @ ?Just)
		(\e ->
			WithLoader @>> showMessageBox ErrorMsg "Error" e ["Ok"] 0 @!
			?None)

exceptionsToWarnings :: !(Task a) -> Task (?a) | iTask a
exceptionsToWarnings task =
	catchAll
		(task @ ?Just)
		(\w ->
			WithLoader @>> showMessageBox WarningMsg "Warning" w ["Ok"] 0 @!
			?None)

waitForEmptyTCPQueue :: Task ()
waitForEmptyTCPQueue =
	watch tcpQueueEmpty >>*
	[OnValue $ ifValue id \_ -> return ()]

withStoredTaskId :: !(SimpleSDSLens TaskId) !(Task a) -> Task a
withStoredTaskId share task = Task
	\ev evalOpts=:{TaskEvalOpts|taskId} iworld -> case ev of
		ResetEvent
			# (_,iworld) = write taskId share EmptyContext iworld
			-> apTask task ev evalOpts iworld
		ev
			-> apTask task ev evalOpts iworld
