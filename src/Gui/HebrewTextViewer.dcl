definition module Gui.HebrewTextViewer

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from iTasks.SDS.Definition import class Identifiable, class Modifiable,
	class Readable, class Registrable, class RWShared, class Writeable
from iTasks.WF.Definition import :: Task

from TextFabric import :: NodeRef

viewHebrewWithMorphologyPopups :: (sds () (?(NodeRef, [(NodeRef, String)])) w) -> Task () | RWShared sds & TC w
