definition module Gui.Sheet

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.GenEq import generic gEq
from System.FilePath import :: FilePath
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Definition import :: UIAttribute
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import :: Task, class iTask

from TextFabric import :: NodeRef

from MBQS.Model import :: SheetSettings, :: ColumnSettings, :: SheetValues

from Gui.DistinctColumnValues import :: DistinctColumnValues

openSheet :: !FilePath -> Task (SheetSettings, ?SheetValues)

saveSheet :: !FilePath !SheetSettings !SheetValues -> Task ()

/**
 * Note that not all fields of this record are synced in the `onRefresh` of the
 * editor (or modified by `onEdit`). See the documentation on the various
 * fields for details.
 */
:: Sheet =
	{ html_id                   :: !?String
		//* If set, the HTML element will get a class `mbqs-sheet-THE_ID`.
	, settings                  :: !SheetSettings
		//* The settings, e.g. used to display column titles. The settings are
		//* not modifiable in the editor. External updates are ignored.
	, values                    :: !SheetValues
		//* The values, editable in the editor. External updates are ignored.
	, shown_rows                :: !?{#Int}
		//* Set to `?Just` to only show a subset of the rows. This field is
		//* never modified by the editor.
	, distinct_values           :: !DistinctColumnValues
		//* This field is maintained by the editor, setting it externally has
		//* no effect. This value is computed in `onReset`, so it can be
		//* `[!!]` initially.
	, distinct_values_selection :: !DistinctColumnValues
		//* This field is maintained by the editor, setting it externally has
		//* no effect. This value is computed in `onReset`, so it can be
		//* `[!!]` initially.
	, unsaved_changes           :: !?Int
		//* `?Just n` means there were `n` edits since the last save *or auto-save*.
		//* `?None` means there were no edits since the last save.
		//* This field may be modified both by the editor and externally.
	, current_cell              :: !?(Int, Int)
		//* This field is maintained by the editor; setting it has no effect.
		//* The row index refers to the array of entries in `values`.
	, current_selection         :: !?((Int, Int), (Int, Int))
		//* This field is maintained by the editor; setting it has no effect.
		//* It contains the corner cells of the selection, if there is one.
		//* There is no guarantee that the first cell is the top left; rather,
		//* it refers to the same cell as `current_cell`.
		//* Unlike `current_cell`, the row indices refer to visual rows (after
		//* `shown_rows` has been taken into account).
	}

derive class iTask \ gEditor Sheet
derive gEditor Sheet

/**
 * The Sheet editor must be tuned with this attribute for settings (e.g. column
 * widths) to be stored in local storage. The identifier should be persistent
 * w.r.t. changes in settings and data but unique w.r.t. other sheets. For
 * instance, a file path can be used.
 */
tableIdAttr :: !String -> UIAttribute

unsavedChangesView :: Editor Sheet ()
nrSearchResultsView :: Editor Sheet ()
selectionView :: Editor Sheet ()

sheetEditor :: Editor Sheet (EditorReport Sheet)

addColumn :: !Int !ColumnSettings !Sheet -> Task Sheet
addDisambiguatingColumn :: !Int !ColumnSettings ![Int] !Sheet -> Task Sheet
renameColumn :: !Int !String !Sheet -> Task Sheet
deleteColumn :: !Int !Sheet -> Task Sheet

addEntry :: ![NodeRef] !Sheet -> Task Sheet
deleteEntry :: !Int !Sheet -> Task Sheet

trimByShownRows :: !Sheet -> Sheet
