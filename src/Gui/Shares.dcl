definition module Gui.Shares

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError
from System.FilePath import :: FilePath

from iTasks.SDS.Definition import :: SDSSource, :: SimpleSDSLens, :: SDSLens
from iTasks.WF.Definition import :: Task

from TextFabric import :: DataSet, :: DataSet`, :: EdgeSet
from TextFabric.Import import :: FeatureType

from Bible import :: Reference

getTextFabricPath :: Task FilePath

data_set :: SimpleSDSLens (?DataSet)

getDataSet :: ![String] -> Task DataSet
getAllFeatures :: Task [(String,FeatureType)]

setupESword :: Task ()

/**
 * Fetches an English translation for the chapter containing a verse in the
 * Hebrew Bible.
 *
 * @param The reference to look up, using the numbering of the Hebrew Bible.
 * @result The verses in the same chapter preceding the given reference (empty
 *   if the first parameter is false). Each element is a HTML `span` tag, with
 *   the class set to `current` if it matches the reference parameter. Each
 *   `span` begins with a `sup` element with the verse number, and the chapter
 *   number if the verse is 1.
 */
englishChapter :: SDSSource Reference (MaybeError String [String]) ()
