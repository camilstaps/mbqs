implementation module Gui.Sheet.UpgradeDataVersion

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdOverloadedList

import Data.Func
import Data.Functor
import qualified Data.Map
import Data.Tuple
import System.FilePath
from Text import class Text(concat), instance Text String, concat4, concat5
import Text.HTML

import iTasks

import Electron.App
import Electron.Dialog

import TextFabric
import TextFabric.BHSA
import TextFabric.NodeMapping

import MBQS.Model
import MBQS.Model.UpgradeVersion
import Gui.Derives
import Gui.Settings
import Gui.Shares
import Gui.Sheet
import Gui.Util

upgradeSheet :: !ElectronWindow -> Task ()
upgradeSheet _ =
	closeOnException $
	getTextFabricPath >>- \tf_path ->
	let tf_path_parent = takeDirectory tf_path in
	sequenceJust
		[ ("Choose the sheet to upgrade.",
			showOpenDialog "Select the sheet to upgrade"
				{ OpenDialogSettings
				| defaultOpenDialogSettings
				& filters = FILE_EXTENSIONS
				})
		, ("Choose the old data version (select the directory containing *.tf files).",
			showOpenDialog "Choose the old data version"
				{ OpenDialogSettings
				| defaultOpenDialogSettings
				& allowFiles = False
				, allowDirectories = True
				, defaultPath = ?Just tf_path_parent
				})
		, ("Choose the new data version (select the directory containing *.tf files).",
			showOpenDialog "Choose the new data version"
				{ OpenDialogSettings
				| defaultOpenDialogSettings
				& allowFiles = False
				, allowDirectories = True
				, defaultPath = ?Just tf_path
				})
		] >>- \[sheet,oldData,newData:_]
	| newData <> tf_path ->
		// We require the new version to be the main version for getDataSet to
		// use the new version. This is used to show the differences between
		// different candidates when a node can be mapped to multiple
		// candidates.
		throw "This function currently requires the new data version to be set as the default version in Settings. Please change your settings and try again."
	| otherwise ->
		upgrade sheet (dropDirectory oldData) newData >>- \(settings, values) ->
		Hint "Choose the location to save the sheet (it is wise to not overwrite the original file and first check that the new file is correct)." @>>
		WithLoader @>>
		showSaveDialog "Choose a location to save the sheet"
			{ SaveDialogSettings
			| defaultSaveDialogSettings
			& filters = FILE_EXTENSIONS
			, defaultPath =
				let (path,ext) = splitExtension sheet in
				?Just (concat5 path "-" (dropDirectory newData) "." ext)
			} >>- \mbNewSheet ->
		case mbNewSheet of
			?None ->
				throw "Upgrade was cancelled by the user"
			?Just newSheet ->
				saveSheet newSheet settings values >-|
				closeWindow
where
	sequenceJust [] = return []
	sequenceJust [(hint,task):tasks] =
		Hint hint @>> WithLoader @>> task >>*
		[ OnValue $ ifValue isJust \(?Just this) -> sequenceJust tasks @ \rest -> [this:rest]
		, OnValue $ ifValue isNone \_ -> throw "Upgrade was cancelled by the user"
		]

// For the old version we don't need the path, only the name.
upgrade :: !FilePath !String !FilePath -> Task (SheetSettings, SheetValues)
upgrade sheet oldVersionName newVersion =
	openSheet sheet >>- \(settings, mbValues) ->
	case mbValues of
		?None ->
			throw "This sheet contains no values."
		?Just values ->
			accWorldError
				(\w
					# (ok,f,w) = fopen omapFile FReadText w
					| not ok -> (Error ("failed to open " +++ omapFile), w)
					# (mbMapping,f) = parse_node_mapping_file f
					# (_,w) = fclose f w
					-> (mbMapping, w))
				((+++) "Failed to parse node mapping: ") >>- \nodeMapping ->
			upgradeWithChoices nodeMapping 'Data.Map'.newMap settings values @
			tuple settings
where
	newVersionName = dropDirectory newVersion
	omapFile = newVersion </> "omap@" +++ oldVersionName +++ "-" +++ newVersionName +++ ".tf"

upgradeWithChoices :: !NodeMapping !MappingChoices !SheetSettings !SheetValues -> Task SheetValues
upgradeWithChoices mapping choices sheetSettings values =
	case upgradeSheetValues mapping choices values of
		Ok values ->
			return values
		Error unknown ->
			makeChoices sheetSettings values mapping unknown >>- \extraChoices ->
			upgradeWithChoices mapping ('Data.Map'.union extraChoices choices) sheetSettings values

:: TargetChoice =
	{ target        :: !NodeRef
	, type          :: !String
	, dissimilarity :: !?Int
	, content       :: !String
	}

derive class iTask \ gText TargetChoice

gText{|TargetChoice|} AsHeader _ =
	[ "Target"
	, "Type"
	, "Dissimilarity"
	, "Content"
	]
gText{|TargetChoice|} _ (?Just {target,type,dissimilarity,content}) =
	[ toString target
	, type
	, maybe "-" toString dissimilarity
	, content
	]

makeChoices :: !SheetSettings !SheetValues !NodeMapping ![(Int, Int, NodeRef)] -> Task MappingChoices
makeChoices sheetSettings {node_names,entries} mapping unknown =
	getDataSet ["voc_lex_utf8"] >>- \data ->
	sequence
		[ choose i (length unknown) data entryIndex nodeIndex n >>! return o tuple key
		\\ i <- [1..] & key=:(entryIndex,nodeIndex,n) <- unknown
		] @
	'Data.Map'.fromList
where
	node_names` = [n \\ n <-: node_names]

	choose :: !Int !Int !DataSet !Int !Int !NodeRef -> Task NodeRef
	choose i total data entryIndex nodeIndex ref =
		viewInformation []
			(displayEntry sheetSettings.SheetSettings.columns entries.[entryIndex].SheetEntry.columns)
		||- case options of
			?None ->
				Hint "Enter a value manually:" @>>
				updateInformation [] 0
			?Just options ->
				Hint (if (length options == 1) "The following candidate will be used:" "Pick a candidate:") @>>
				AddCSSClass "choicegrid-fourth-column-hebrew" @>>
				updateChoiceAs
					[ChooseFromGrid id]
					options
					(\opt -> opt.TargetChoice.target)
					(hd options).TargetChoice.target
	where
		nodeType = hd [type \\ {NodeSettings|type,name} <- sheetSettings.SheetSettings.nodes | name == nodeName]
		nodeName = node_names` !! nodeIndex

		displayEntry headers values = DivTag []
			[ H4Tag [] [Text (concat4 "Data disturbance " (toString i) " of " (toString total))]
			, PTag []
				[ Text $ concat
					[ "There "
					, case options of
						?None -> "are no candidates"
						?Just [_] -> "is an imperfect candidate"
						?Just _ -> "are multiple candidates"
					, " to map node ", toString ref
					, " (a ", nodeType, " node called '", nodeName, "')"
					, " in entry ", toString (entryIndex+1)
					, ". The entry looks like this:"
					]
				]
			, TableTag []
				[ TheadTag [] [TrTag [] [ThTag [] [Text c.ColumnSettings.title] \\ c <- headers]]
				, TbodyTag []
					[ TrTag []
						[ TdTag (maybeToList (ClassAttr <$> css_class)) [Text v]
						\\ v <-: values & {css_class} <- headers
						]
					]
				]
			]

		options =
			sortBy ((<) `on` \opt -> opt.TargetChoice.dissimilarity) <$>
			MapM toTargetChoice <$>
			'Data.Map'.get ref mapping

		toTargetChoice {Target|target,dissimilarity} =
			{ target = target
			, type = type
			, dissimilarity = dissimilarity
			, content = content
			}
		where
			type = get_node_feature 0 data.DataSet`.nodes.[target]
			content = case type of
				"lex" -> get_node_feature lex data.DataSet`.nodes.[target]
				_ -> concat $ map snd $ get_text target data
			where
				(?Just lex) = get_node_feature_id "voc_lex_utf8" data
