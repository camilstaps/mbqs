implementation module Gui.SearchHistory

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import Data.Error
import qualified Data.Map

import iTasks

import Gui.Util

MAX_HISTORY_SIZE :== 25

derive class iTask SearchHistoryCommand

searchHistory :: SDSLens String SearchHistory SearchHistoryCommand
searchHistory =: sdsLens "searchHistory"
	(const ())
	(SDSRead \filename history -> Ok (fromMaybe [] ('Data.Map'.get filename history)))
	(SDSWrite \filename history cmd -> Ok (?Just ('Data.Map'.alter (?Just o apply cmd o fromMaybe []) filename history)))
	(SDSNotifyWithoutRead \filename _ _ filename2 -> filename == filename2)
	?None
	(persistentShare "globalSearchHistory" 'Data.Map'.newMap)
where
	apply :: !SearchHistoryCommand !SearchHistory -> SearchHistory
	apply (AddEntry q) hist
		| isMember q hist
			= [q:filter ((<>) q) hist]
		| length hist >= MAX_HISTORY_SIZE
			= [q:take 24 hist]
			= [q:hist]
	apply (RemoveEntry q) hist
		= filter ((<>) q) hist
