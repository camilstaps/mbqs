definition module Gui.DistinctColumnValues

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.GenEq import generic gEq
from Data.Map import :: Map
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import :: Task, class iTask

from MBQS.Model import :: SheetEntry

//* For each column, a `Map`, with for each value the number of occurrences.
:: DistinctColumnValues =: DistinctColumnValues [!Map String Int!]

computeAllDistinctValues :: !{#SheetEntry} -> DistinctColumnValues
updateDistinctValues :: !Int !String !String !DistinctColumnValues -> DistinctColumnValues

:: DistinctColumnValuesView =
	{ values :: !DistinctColumnValues
	, column :: !?Int
	}

derive class iTask \ gEditor DistinctColumnValues, DistinctColumnValuesView
//* This editor is only for viewing values.
derive gEditor DistinctColumnValuesView
