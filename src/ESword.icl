implementation module ESword

/**
 * This file is part of MBQS.
 *
 * MBQS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * MBQS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with MBQS. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdDebug

import Data.Error
import Database.SQL
import Database.SQL.SQLite
import System.FilePath
import System._Pointer
import System._Unsafe
from Text import class Text(concat), instance Text String, concat3, concat4

import Bible

import code from library "-lsqlite3"

toBookID :: !Book -> ?Int
toBookID book = case book of
	// NB: this follows the christian ordering
	Genesis       -> ?Just  1
	Exodus        -> ?Just  2
	Leviticus     -> ?Just  3
	Numeri        -> ?Just  4
	Deuteronomium -> ?Just  5
	Josua         -> ?Just  6
	Judices       -> ?Just  7
	Ruth          -> ?Just  8
	Samuel_I      -> ?Just  9
	Samuel_II     -> ?Just 10
	Reges_I       -> ?Just 11
	Reges_II      -> ?Just 12
	Chronica_I    -> ?Just 13
	Chronica_II   -> ?Just 14
	Esra          -> ?Just 15
	Nehemia       -> ?Just 16
	Esther        -> ?Just 17
	Iob           -> ?Just 18
	Psalmi        -> ?Just 19
	Proverbia     -> ?Just 20
	Ecclesiastes  -> ?Just 21
	Canticum      -> ?Just 22
	Jesaia        -> ?Just 23
	Jeremia       -> ?Just 24
	Threni        -> ?Just 25
	Ezechiel      -> ?Just 26
	Daniel        -> ?Just 27
	Hosea         -> ?Just 28
	Joel          -> ?Just 29
	Amos          -> ?Just 30
	Obadia        -> ?Just 31
	Jona          -> ?Just 32
	Micha         -> ?Just 33
	Nahum         -> ?Just 34
	Habakuk       -> ?Just 35
	Zephania      -> ?Just 36
	Haggai        -> ?Just 37
	Sacharia      -> ?Just 38
	Maleachi      -> ?Just 39
	UnknownBook _ -> ?None

openModule :: !FilePath !*World -> (!MaybeError String ESwordModule, !*World)
openModule file w
	# (mbErr,mbCtx,w) = openContext w
	| isJust mbErr
		= (Error (toString (fromJust mbErr)), w)
	# (mbErr,mbCon,ctx) = openConnection db (is_SQLiteContext (fromJust mbCtx))
	| isJust mbErr
		= (Error (toString (fromJust mbErr)), w)
	# (mbErr,mbCur,con) = openCursor (is_SQLiteConnection (fromJust mbCon))
	| isJust mbErr
		= (Error (toString (fromJust mbErr)), w)
		= (Ok (toESwordModule (fromJust mbCur)), w)
where
	db =
		{ database = file
		, host     = ?None
		, username = ?None
		, password = ?None
		}

	is_SQLiteContext :: !u:SQLiteContext -> u:SQLiteContext
	is_SQLiteContext c = c
	is_SQLiteConnection :: !u:SQLiteConnection -> u:SQLiteConnection
	is_SQLiteConnection c = c

getMaxVerse :: !Book !Int !ESwordModule -> (!MaybeError String Int, !ESwordModule)
getMaxVerse book chapter mod
	# bookID = toBookID book
	| isNone bookID
		= (Error (concat3 "Unknown book '" (toString book) "'"), mod)
	# cur = toSQLiteCursor mod
	# (mbErr,cur) = execute
		"SELECT MAX(Verse) FROM Bible WHERE Book=? AND Chapter=?;"
		[SQLVInteger (fromJust bookID), SQLVInteger chapter]
		cur
	| isJust mbErr
		= (Error (toString (fromJust mbErr)), toESwordModule cur)
	# (mbErr,mbRow,cur) = fetchOne cur
	| isJust mbErr
		= (Error (toString (fromJust mbErr)), toESwordModule cur)
	| isNone mbRow
		= (Error err, toESwordModule cur)
	= case fromJust mbRow of
		[SQLVInteger verse]
			-> (Ok verse, toESwordModule cur)
			-> (Error err, toESwordModule cur)
where
	err = concat4 (englishName book) " " (toString chapter) " could not be found."

getVerse :: !Reference !ESwordModule -> (!MaybeError String String, !ESwordModule)
getVerse {book,chapter,verse} mod
	# bookID = toBookID book
	| isNone bookID
		= (Error (concat ["Unknown book '",toString book,"'"]), mod)
	# cur = toSQLiteCursor mod
	# (mbErr,cur) = execute
		"SELECT Scripture FROM Bible WHERE Book=? AND Chapter=? AND Verse=?;"
		[SQLVInteger (fromJust bookID), SQLVInteger chapter, SQLVInteger verse]
		cur
	| isJust mbErr
		= (Error (toString (fromJust mbErr)), toESwordModule cur)
	# (mbErr,mbRow,cur) = fetchOne cur
	| isJust mbErr
		= (Error (toString (fromJust mbErr)), toESwordModule cur)
	| isNone mbRow
		= (Error err, toESwordModule cur)
	= case fromJust mbRow of
		[SQLVText text]
			-> (Ok text, toESwordModule cur)
			-> (Error err, toESwordModule cur)
where
	err = concat [englishName book," ",toString chapter,":",toString verse," could not be found."]

hebrew_letters :: {![Char]}
hebrew_letters =:
	{ ['א'], ['ב'], ['ג'], ['ד'], ['ה'], ['ו'], ['ז'], ['ח']
	, ['ט'], ['י'], ['ך'], ['כ'], ['ל'], ['ם'], ['מ'], ['ן']
	, ['נ'], ['ס'], ['ע'], ['ף'], ['פ'], ['ץ'], ['צ'], ['ק']
	, ['ר'], ['ש'], ['ת']
	}

eSwordMarkupToHTML :: !Bool !String -> String
eSwordMarkupToHTML allow_footnotes markup =
	let (converted,rest) = convert [c \\ c <-: markup] in
	concat
		[ toString (dropWhile isSpace converted)
		: if (isEmpty rest) []
			[ "<span style='color:red;'>"
			, toString rest
			, "</span>"
			]
		]
where
	convert :: ![Char] -> (![Char], ![Char])
	convert [] = ([], [])
	convert [c:cs] = case c of
		'{'
			# (group,rest) = convert_group cs
			-> add group (convert rest)
		'\\'
			-> case cs of
				['emdash':cs] | noAlphaNum cs
					-> add ['&mdash;'] (convert (dropSpace cs))
				['par':cs] | noAlphaNum cs
					-> add ['<br/>'] (convert (dropSpace cs))
				['tab':cs] | noAlphaNum cs
					-> add ['&emsp;'] (convert (dropSpace cs))
				['scaps':cs] | noAlphaNum cs
					-> add ['<span class="small-caps">'] (convert (dropSpace cs))
				['scaps0':cs] | noAlphaNum cs
					-> add ['</span>'] (convert (dropSpace cs))
				// Hebrew letters: unknown encoding. First capture sin with dot:
				['\'f9\\\'d1':cs] | noAlphaNum cs
					-> add ['שׁ'] (convert cs)
				['\'f9\\\'d2':cs] | noAlphaNum cs
					-> add ['שׂ'] (convert cs)
				// Then capture remaining letters:
				['\'':[c1,c2:cs]] | noAlphaNum cs &&
						(c1=='e' && (isDigit c2 || 'a'<=c2 && c2<='f') ||
							c1=='f' && (isDigit c2 || c2=='a'))
					-> add hebrew_letters.[(toInt (c1-'e')<<4)+toInt (c2 - if (isDigit c2) '0' 'W')] (convert cs)
				cs
					-> add ['\\'] (convert cs)
		'}'
			-> ([], cs)
		c
			-> add [c] (convert cs)
	where
		add c (seen,rest) = (c++seen, rest)
		dropSpace cs = if (isEmpty cs) cs (if (isSpace (hd cs)) (tl cs) cs)

	convert_group :: ![Char] -> (![Char], ![Char])
	convert_group cs
		# (add_markup,cs) = read_attrs cs
		# (before,after) = convert cs
		= (add_markup before, after)
	where
		scan_for_close level seen ['}':cs]
			| level==0  = (reverse seen, cs)
			| otherwise = scan_for_close (level-1) seen

	read_attrs :: ![Char] -> (![Char] -> [Char], ![Char])
	read_attrs cs = case cs of
		['\\b':cs] // bold font
			# (f,cs) = read_attrs cs
			-> (encapsulate ['<b>'] ['</b>'] o f, cs)
		['\\cf':cs] // ???
			-> read_attrs (dropWhile isDigit cs)
		['\\fs':cs] // font size?
			-> read_attrs (dropWhile isDigit cs)
		['\\f2':cs] | noAlphaNum cs // Hebrew font; encapsulate with LTR/RTL marks
			# (f,cs) = read_attrs cs
			-> (encapsulate ['<span class="hebrew">'] ['</span>'] o f,cs)
		['\\f':[c:cs]] | isDigit c // font?
			-> read_attrs (dropWhile isDigit cs)
		['\\i':cs] // italic font
			# (f,cs) = read_attrs cs
			-> (encapsulate ['<i>'] ['</i>'] o f, cs)
		['\\scaps':cs] // small-caps
			# (f,cs) = read_attrs cs
			-> (encapsulate ['<span class="small-caps">'] ['</span>'] o f, cs)
		['\\super':cs] //superscript
			# (f,cs) = read_attrs cs
			| allow_footnotes
				-> (encapsulate ['<sup>'] ['</sup>'] o f, cs)
				-> (const [], cs)
		['\\':cs]
			# (mod,cs) = span (\c -> c<>' ' && c<>'\\') cs
			-> trace_n ("Unknown e-Sword markup modifier \\"+++toString mod) read_attrs cs
		[' ':cs]
			-> (id, cs)
		cs
			-> trace_n "internal error in eSwordMarkupToHTML" (id, cs)
	where
		encapsulate before after cs = before ++ cs ++ after

	noAlphaNum cs = isEmpty cs || not (isAlphanum (hd cs))

toSQLiteCursor :: (ESwordModule -> .SQLiteCursor)
toSQLiteCursor = unsafeCoerce

toESwordModule :: (.SQLiteCursor -> ESwordModule)
toESwordModule = unsafeCoerce
