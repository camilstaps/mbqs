# *Məḇaqqēš*

*Məḇaqqēš* is a spreadsheet application for scholars of the Hebrew Bible and
Biblical Hebrew. It looks like this:

![Screenshot](/doc/screenshot.png)

With *Məḇaqqēš*, you can:

1. Programmatically find words, phrases, clauses, etc. of interest
2. Create a spreadsheet with information about these text elements
3. Annotate the data manually

*Məḇaqqēš* knows which verse corresponds to each row of your spreadsheet and
displays the Hebrew text, as well as an English translation, next to it.

*Məḇaqqēš* is currently supported on Linux and 64-bit Windows.

## Installation

1. Install dependencies. On Windows, all dependencies are included, so you
   don't need to do anything. On Linux, you need to install the 32-bit version
   of libsqlite3:

   ```bash
   sudo dpkg --add-architecture i386
   sudo apt install libsqlite3-0:i386
   ```

2. Get the data: this application expects Text-Fabric files (`*.tf`) in the
   directory `C:\Users\{username}\text-fabric-data\github\etcbc\bhsa\tf\2021`
   (Windows) or `~/text-fabric-data/github/etcbc/bhsa/tf/2021` (Linux). You can
   download the data from https://github.com/ETCBC/bhsa/releases (choose
   `tf-2021.zip`).

   If you prefer a different location for your data files, you can change it in
   the Settings dialog after you have installed *Məḇaqqēš*.

   If you have the [Text-Fabric][] app for the ETCBC data installed, you may
   have this data already; in that case you do not need to download it again.

3. Download and unpack the application:

   - Windows: download
     [the package](https://gitlab.com/camilstaps/mbqs/-/jobs/artifacts/main/raw/dist/mbqs-windows-x64.zip?job=windows)
     and unpack it to your location of choice.

   - Linux: download
     [the package](https://gitlab.com/camilstaps/mbqs/-/jobs/artifacts/main/raw/dist/mbqs-linux-x64.tar.gz?job=linux)
     and unpack it to your location of choice.

4. You can now run `StartMBQS` from the unpacked package.

## Acknowledgements

- We rely on the annotated version of the Hebrew Bible maintained by the
  [Eep Talstra Centre for Bible and Computer][ETCBC].
- The English translation currently used is the [New English Translation][NET].

## Copyright &amp; License

*Məḇaqqēš* is written and maintained by [Camil Staps][].
Copyright &copy; 2019&ndash;2023 Camil Staps.
Licensed under AGPLv3 (see the LICENSE file for details).

[Camil Staps]: https://camilstaps.nl
[ETCBC]: http://etcbc.nl/
[NET]: https://netbible.com/
[Text-Fabric]: https://github.com/annotation/text-fabric
